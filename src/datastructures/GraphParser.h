#ifndef NEAR_DELAUNAY_GRAPHPARSER_H
#define NEAR_DELAUNAY_GRAPHPARSER_H


#include "graph/Graph.h"
#include "dcel/DCEL.h"

/**
 * Static class which provides some functionality associated to handling graphs.
 */
class GraphParser {
private:
    static std::string get_file_extension(const std::string& file_name);
    static void next_line(std::ifstream& in_file, std::stringstream& ss);

public:
    static std::shared_ptr<DCEL> parse_graph_to_DCEL(std::shared_ptr<Graph> graph);

    static std::shared_ptr<Graph> parse_DCEL_to_graph(std::shared_ptr<DCEL> dcel);

    static std::shared_ptr<Graph> read_in_file(const std::string& file_name);

    static void write_to_file(std::shared_ptr<Graph> graph, const std::string& file_name);
};


#endif //NEAR_DELAUNAY_GRAPHPARSER_H
