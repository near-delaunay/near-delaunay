#include <unordered_map>
#include <queue>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

#include <GeometryHelper.h>
#include "GraphParser.h"

std::shared_ptr<DCEL> GraphParser::parse_graph_to_DCEL(std::shared_ptr<Graph> graph) {
    std::shared_ptr<DCEL> dcel = std::make_shared<DCEL>();

    std::unordered_map<std::shared_ptr<GraphVertex>, std::shared_ptr<DCELVertex>> graph_to_DCEL_vertex;

    // Create all DCELVertices and add them to DCEL. Keep track of them in map for later use.
    for (const std::shared_ptr<GraphVertex>& vertex : graph->get_vertices()) {
        std::shared_ptr<DCELVertex> current_dcel_vertex = std::make_shared<DCELVertex>(
                DCELVertex(vertex->get_x(), vertex->get_y()));

        graph_to_DCEL_vertex[vertex] = current_dcel_vertex;

        dcel->manual_add_vertex(current_dcel_vertex);
    }

    // Loop over all edges and add 2 halfedges for each edge.
    for (const std::shared_ptr<GraphVertex>& vertex : graph->get_vertices()) {
        for (auto edge : vertex->get_edges()) {

            // As edges are undirected, they are stored in both the source and target.
            // Make sure that each edge is only done once.
            if ((*graph_to_DCEL_vertex[vertex].get()) < (*graph_to_DCEL_vertex[edge.lock()].get())) {
                continue;
            }

            // create edges
            std::shared_ptr<Halfedge> h1 = std::make_shared<Halfedge>();
            std::shared_ptr<Halfedge> h2 = std::make_shared<Halfedge>();

            // set properties
            h1->set_target(graph_to_DCEL_vertex[vertex]);
            h2->set_target(graph_to_DCEL_vertex[edge.lock()]);

            h1->set_twin(h2);
            h2->set_twin(h1);

            // add edges
            dcel->manual_add_edge(h1);
            dcel->manual_add_edge(h2);

            graph_to_DCEL_vertex[edge.lock()]->add_edge(h1);
            graph_to_DCEL_vertex[vertex]->add_edge(h2);
        }
    }

    // Set next edge for the half edges by looping over vertices and sorting the associated edges on angle.
    for (const std::shared_ptr<GraphVertex>& vertex : graph->get_vertices()) {
        std::vector<std::pair<std::shared_ptr<Halfedge>, double>> edge_angles;

        for (const std::shared_ptr<Halfedge>& edge : graph_to_DCEL_vertex[vertex]->get_edges()) {
            int v2x = edge->get_target()->get_x() - vertex->get_x();
            int v2y = edge->get_target()->get_y() - vertex->get_y();

            double angle = GeometryHelper::get_inclination(v2x, v2y);
            edge_angles.emplace_back(edge, angle);
        }

        // sort on angle
        std::sort(edge_angles.begin(), edge_angles.end(), [](const auto& lhs, const auto& rhs) {
            return lhs.second < rhs.second;
        });

        // add next edge
        for (int i = 0; i < edge_angles.size(); i++) {
            edge_angles[i].first->get_twin()->set_next(edge_angles[(i+1) % edge_angles.size()].first);
            edge_angles[(i+1) % edge_angles.size()].first->set_previous(edge_angles[i].first->get_twin());
        }
    }

    // create and assign the faces
    for (std::shared_ptr<Halfedge>& edge : dcel->get_edges()) {
        if (edge->get_face() != nullptr) {
            continue;
        }

        std::shared_ptr<Face> f = std::make_shared<Face>();

        f->set_edge(edge);

        dcel->manual_add_face(f);

        std::shared_ptr<Halfedge>& current_edge = edge;

        do {
            current_edge->set_face(f);
            current_edge = current_edge->get_next();
        } while (current_edge->get_face() == nullptr);
    }

    return dcel;
}

std::shared_ptr<Graph> GraphParser::parse_DCEL_to_graph(std::shared_ptr<DCEL> dcel) {
    std::shared_ptr<Graph> graph = std::make_shared<Graph>();

    // Unordered map to convert DCELVertices as found in the parameter dcel to the GraphVertices as added to graph.
    std::unordered_map<std::shared_ptr<DCELVertex>, std::shared_ptr<GraphVertex>> dcel_to_graph_vertices;

    // Loop over all the vertices in the DCEL and add them to the graph.
    for (const std::shared_ptr<DCELVertex>& current_vertex : dcel->get_vertices()) {
        std::shared_ptr<GraphVertex> current_graph_vertex = std::make_shared<GraphVertex>(
                GraphVertex(current_vertex->get_x(), current_vertex->get_y()));

        dcel_to_graph_vertices[current_vertex] = current_graph_vertex;
        graph->add_vertex(current_graph_vertex);
    }

    // Loop over all the halfedges and add an edge to graph if the target vertex is bigger than the source
    // (thus each edge is only added once)
    for (const std::shared_ptr<Halfedge>& current_edge : dcel->get_edges()) {
        if ((*current_edge->get_target()) < (*current_edge->get_twin()->get_target())) {
            graph->add_edge(dcel_to_graph_vertices[current_edge->get_target()],
                           dcel_to_graph_vertices[current_edge->get_twin()->get_target()]);
        }
    }

    return graph;
}

std::shared_ptr<Graph> GraphParser::read_in_file(const std::string& file_name) {
    // Evaluate the file extension of the file we are reading in.
    // Basic sanity check
    std::string ext = get_file_extension(file_name);
    if (ext != "txt") {
        std::string message = "Wrong file extension found: " + ext + " for file: " + file_name + "\n";
        std::cerr << message;
        throw std::runtime_error(message);
    }

    // Try opening the file
    std::ifstream in_file;
    in_file.open(file_name);

    // Check if file could be opened
    if (!in_file) {
        std::string message = "Unable to find or open file: " + file_name + " \n";
        std::cerr << message;
        throw std::runtime_error(message);
    }

    // Create the graph object
    std::shared_ptr<Graph> graph = std::make_shared<Graph>();

    int vertex_count;
    int edge_count;

    std::stringstream linestream;

    // Read in all vertices
    next_line(in_file, linestream);
    linestream >> vertex_count;
    int id;
    int x, y;
    std::unordered_map<int, std::shared_ptr<GraphVertex>> vertex_pointer;

    for (int i = 0; i < vertex_count; i++) {
        next_line(in_file, linestream);
        linestream >> id >> x >> y;

        std::shared_ptr<GraphVertex> vertex = std::make_shared<GraphVertex>(GraphVertex(x, y));

        graph->add_vertex(vertex);
        vertex_pointer[id] = vertex;
    }

    // Read in all edges
    next_line(in_file, linestream);
    linestream >> edge_count;

    for (int i = 0; i < edge_count; i++) {
        int u, v;

        next_line(in_file, linestream);
        std::string string_significance;
        linestream >> u >> v;

        graph->add_edge(vertex_pointer[u], vertex_pointer[v]);
    }

    return graph;
}

std::string GraphParser::get_file_extension(const std::string& file_name) {
    std::size_t found = file_name.find_last_of(".");
    return file_name.substr(found + 1);
}

void GraphParser::next_line(std::ifstream &in_file, std::stringstream& ss) {
    std::string line;
    getline(in_file, line);
    ss.str(line); // Insert the string
    ss.clear(); // Clears stringstream flags, including the one indicating stream is empty.
}

void GraphParser::write_to_file(std::shared_ptr<Graph> graph, const std::string& file_name) {
    std::ofstream out_file;

    out_file.open(file_name);

    if (!out_file.is_open()) {
        std::cout << "file not open\n";
        std::cerr << "Cannot create ..., error was: " << errno << "\n";
        throw std::invalid_argument("Cannot create ..., error was: " + std::to_string(errno));
    }

    std::vector<std::shared_ptr<GraphVertex>> vertices = graph->get_vertices();
    std::unordered_map<std::shared_ptr<GraphVertex>, int> vertex_ids;

    // add all vertices to the file
    // print no of vertices
    out_file << vertices.size() << "\n";
    int edge_count = 0;

    for (int i = 0; i < vertices.size(); i++) {
        std::shared_ptr<GraphVertex> current_vertex = vertices.at(i);

        out_file << i << " " << current_vertex->get_x() << " " << current_vertex->get_y() << "\n";

        vertex_ids[current_vertex] = i;
        edge_count += current_vertex->get_edges().size();
    }

    // add all edges to the file
    // print no of edges (is divided by two as the edge_count counts each edge twice)
    out_file << edge_count / 2 << "\n";

    for (int i = 0; i < vertices.size(); i++) {
        std::shared_ptr<GraphVertex> current_vertex = vertices.at(i);
        for (auto adj_vertex : current_vertex->get_edges()) {
            if (vertex_ids[current_vertex] > vertex_ids[adj_vertex.lock()]) {
                out_file << vertex_ids[current_vertex] << " " << vertex_ids[adj_vertex.lock()] << "\n";
            }
        }
    }

    out_file.close();
}
