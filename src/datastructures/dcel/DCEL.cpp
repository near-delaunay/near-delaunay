#include "DCEL.h"

void DCEL::add_edge_at(std::shared_ptr<DCELVertex> v, std::shared_ptr<Halfedge> h) {
    std::shared_ptr<Face> f = h->get_face();
    std::shared_ptr<Face> f1 = std::make_shared<Face>();
    std::shared_ptr<Face> f2 = std::make_shared<Face>();
    std::shared_ptr<Halfedge> h1 = std::make_shared<Halfedge>();
    std::shared_ptr<Halfedge> h2 = std::make_shared<Halfedge>();

    h->get_target()->add_edge(h1);
    v->add_edge(h2);

    f1->set_edge(h1);
    f2->set_edge(h2);

    h1->set_twin(h2);
    h2->set_twin(h1);

    h1->set_target(v);
    h2->set_target(h->get_target());

    h2->set_next(h->get_next());
    h2->get_next()->set_previous(h2);
    h1->set_previous(h);
    h->set_next(h1);

    std::shared_ptr<Halfedge> iterator = h2;

    h2->set_face(f2);
    while (iterator->get_target() != v) {
        iterator = iterator->get_next();
        iterator->set_face(f2);
    }

    h1->set_next(iterator->get_next());
    h1->get_next()->set_previous(h1);

    iterator->set_next(h2);
    h2->set_previous(iterator);

    iterator = h1;

    h1->set_face(f1);
    while (iterator->get_target() != h->get_target()) {
        iterator = iterator->get_next();
        iterator->set_face(f1);
    }

    this->edges.push_back(h1);
    this->edges.push_back(h2);
    this->faces.push_back(f1);
    this->faces.push_back(f2);

    this->faces.remove(f);

    // reset outer face
    this->outer_face = nullptr;
}

void DCEL::initial_vertices(std::shared_ptr<DCELVertex> v1, std::shared_ptr<DCELVertex> v2) {
    if (!this->faces.empty()) {
        throw "Non-empty DCEL";
    }

    std::shared_ptr<Face> outer = std::make_shared<Face>();
    std::shared_ptr<Halfedge> h1 = std::make_shared<Halfedge>();
    std::shared_ptr<Halfedge> h2 = std::make_shared<Halfedge>();

    h1->set_face(outer);
    h2->set_face(outer);

    h1->set_target(v1);
    h2->set_target(v2);

    h1->set_next(h2);
    h1->set_previous(h2);
    h2->set_next(h1);
    h2->set_previous(h1);

    h1->set_twin(h2);
    h2->set_twin(h1);

    v1->add_edge(h2);
    v2->add_edge(h1);

    outer->set_edge(h1);

    this->vertices.push_back(v1);
    this->vertices.push_back(v2);
    this->faces.push_back(outer);
    this->edges.push_back(h1);
    this->edges.push_back(h2);

    // reset outer face
    this->outer_face = nullptr;
}

void DCEL::add_vertex_at(std::shared_ptr<DCELVertex> v, std::shared_ptr<Halfedge> h) {
    std::shared_ptr<Halfedge> h1 = std::make_shared<Halfedge>();
    std::shared_ptr<Halfedge> h2 = std::make_shared<Halfedge>();

    v->add_edge(h2);
    h->get_target()->add_edge(h1);

    h1->set_twin(h2);
    h2->set_twin(h1);

    h1->set_target(v);
    h2->set_target(h->get_target());

    h1->set_face(h->get_face());
    h2->set_face(h->get_face());

    h1->set_next(h2);
    h2->set_next(h->get_next());

    h1->set_previous(h);
    h2->set_previous(h1);

    h->set_next(h1);
    h2->get_next()->set_previous(h2);

    this->edges.push_back(h1);
    this->edges.push_back(h2);
    this->vertices.push_back(v);

    // reset outer face
    this->outer_face = nullptr;
}

void DCEL::flip_edge(std::shared_ptr<Halfedge> h) {
    std::shared_ptr<Face> f1 = h->get_face();
    std::shared_ptr<Face> f2 = h->get_twin()->get_face();

    if (!is_edge_flippable(h)) return;

    // Update faces
    f1->set_edge(h);
    f2->set_edge(h->get_twin());

    // Correctly update vertices
    h->get_previous()->get_target()->remove_edge(h);
    h->get_twin()->get_previous()->get_target()->remove_edge(h->get_twin());
    h->get_twin()->get_next()->get_target()->add_edge(h);
    h->get_next()->get_target()->add_edge(h->get_twin());

    // Correctly set h.next
    h->get_next()->set_face(f2);
    h->get_next()->set_next(h->get_twin());
    h->get_next()->set_previous(h->get_twin()->get_previous());

    // Correctly set h.previous
    h->get_previous()->set_next(h->get_twin()->get_next());
    h->get_previous()->set_previous(h);

    // Correctly set h.twin.next
    h->get_twin()->get_next()->set_face(f1);
    h->get_twin()->get_next()->set_next(h);
    h->get_twin()->get_next()->set_previous(h->get_previous());

    // Correctly set h.twin.previous
    h->get_twin()->get_previous()->set_next(h->get_next());
    h->get_twin()->get_previous()->set_previous(h->get_twin());

    // Correctly set h
    h->set_target(h->get_next()->get_target());
    h->set_next(h->get_previous());
    h->set_previous(h->get_twin()->get_next());

    // Correctly set h.twin
    h->get_twin()->set_target(h->get_twin()->get_next()->get_target());
    h->get_twin()->set_next(h->get_twin()->get_previous());
    h->get_twin()->set_previous(h->get_twin()->get_previous()->get_next());
}

std::shared_ptr<Face> DCEL::calculate_outer_face() {
    if (faces.size() == 1) {
        return faces.front();
    }

    for (std::shared_ptr<Face> face : faces) {
        if (face->get_adjacent_edges().size() > 3) {
            return face;
        }
    }

    // All faces have exactly 3 edges associated to them (fewer than three is not possible by our assumptions).
    // We thus have to find the one that has angles bigger than PI associated to it.
    for (std::shared_ptr<Halfedge> edge : edges) {
        if (edge->get_angle_to_next() > M_PI) {
            return edge->get_face();
        }
    }

    throw std::runtime_error("The state of the DCEL is not correct.");
}

std::vector<std::shared_ptr<Halfedge>> DCEL::get_flippable_edges() {
    std::vector<std::shared_ptr<Halfedge>> flippable_edges;

    for (const auto& edge : this->get_edges()) {
        if (!is_edge_flippable(edge)) continue;

        flippable_edges.push_back(edge);
    }

    return flippable_edges;
}