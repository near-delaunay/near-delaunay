#include <utility>
#include <memory>
#include "DCELVertex.h"
#include <cmath>

#ifndef NEAR_DELAUNAY_HALFEDGE_H
#define NEAR_DELAUNAY_HALFEDGE_H

class Face;

/**
 * Special 'halfedge' used in the DCEL data structure.
 */
class Halfedge {
private:
    std::shared_ptr<DCELVertex> target;
    std::shared_ptr<Face> face;
    std::shared_ptr<Halfedge> twin;
    std::shared_ptr<Halfedge> next;
    std::shared_ptr<Halfedge> previous;

public:
    // Empty constructor
    Halfedge() {}

    // Getters
    std::shared_ptr<DCELVertex> get_target() const {
        return target;
    }

    std::shared_ptr<Face> get_face() const {
        return face;
    }

    std::shared_ptr<Halfedge> get_twin() const {
        return twin;
    }

    std::shared_ptr<Halfedge> get_next() const {
        return next;
    }

    std::shared_ptr<Halfedge> get_previous() const {
        return previous;
    }

    // Setters
    void set_target(std::shared_ptr<DCELVertex> target) {
        this->target = std::move(target);
    }

    void set_face(std::shared_ptr<Face> f) {
        this->face = std::move(f);
    }

    void set_twin(std::shared_ptr<Halfedge> twin) {
        this->twin = std::move(twin);
    }

    void set_next(std::shared_ptr<Halfedge> next) {
        this->next = std::move(next);
    }

    void set_previous(std::shared_ptr<Halfedge> prev) {
        this->previous = std::move(prev);
    }

    bool operator== (const Halfedge &h) const {
        return (this == &h);
    }

    bool operator!= (const Halfedge &h) const {
        return (this != &h);
    }

    /**
     * Calculates the counter clockwise degree between vec(v1, v2) and vec(v3, v2) with
     *
     * v1 = get_twin()->get_target()
     * v2 = get_target()
     * v3 = get_next()->get_target()
     *
     * Thus is should be leq to PI for angles associated to inner faces.
     */
    double get_angle_to_next() const;

    double get_length() {
        double x_dist = this->get_target()->get_x() - this->get_twin()->get_target()->get_x();
        double y_dist = this->get_target()->get_y() - this->get_twin()->get_target()->get_y();
        return std::sqrt(x_dist * x_dist + y_dist * y_dist);
    }
};

#endif //NEAR_DELAUNAY_HALFEDGE_H
