#include "Face.h"

std::vector<std::shared_ptr<Halfedge>> Face::get_adjacent_edges() const {
    std::vector<std::shared_ptr<Halfedge>> adjacent_edges;
    std::shared_ptr<Halfedge> pointer = this->edge;

    do {
        adjacent_edges.push_back(pointer);
        pointer = pointer->get_next();
    } while (pointer != this->edge);

    return adjacent_edges;
}

std::unordered_set<std::shared_ptr<Face>> Face::get_adjacent_faces() const {
    std::unordered_set<std::shared_ptr<Face>> adjacent_faces = {};

    for (const std::shared_ptr<Halfedge>& e : this->get_adjacent_edges()) {
        adjacent_faces.insert(e->get_twin()->get_face());
    }

    return adjacent_faces;
}