#include <cmath>
#include <GeometryHelper.h>
#include <vector>
#include "Halfedge.h"


double Halfedge::get_angle_to_next() const {

    std::shared_ptr<DCELVertex> vertex1 = get_twin()->get_target();
    std::shared_ptr<DCELVertex> vertex2 = get_target();
    std::shared_ptr<DCELVertex> vertex3 = get_next()->get_target();
    double v1x = vertex1->get_x() - vertex2->get_x();
    double v1y = vertex1->get_y() - vertex2->get_y();
    std::vector<double> v1{v1x, v1y};

    double v2x = vertex3->get_x() - vertex2->get_x();
    double v2y = vertex3->get_y() - vertex2->get_y();
    std::vector<double> v2{v2x, v2y};

    double angle = fmod(GeometryHelper::signed_angle(v1, v2) + 2 * M_PI, 2 * M_PI);

    return angle;
}
