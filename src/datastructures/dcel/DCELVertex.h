#ifndef NEAR_DELAUNAY_DCEL_VERTEX_H
#define NEAR_DELAUNAY_DCEL_VERTEX_H


#include <list>
#include <memory>

class Halfedge;

/**
 * Vertex used in the DCEL data structure.
 */
class DCELVertex {
private:
    int x;
    int y;
    std::list<std::shared_ptr<Halfedge>> edges;

public:
    // constructor for coordinates x,y
    DCELVertex(int x, int y) : x(x), y(y) {}

    // Getters
    std::list<std::shared_ptr<Halfedge>> get_edges() {
        return this->edges;
    };

    int get_x() const {
        return x;
    }

    int get_y() const {
        return y;
    }

    void add_edge(const std::shared_ptr<Halfedge>& h) {
        this->edges.push_back(h);
    }

    void remove_edge(const std::shared_ptr<Halfedge>& h) {
        this->edges.remove(h);
    }

    /**
     * Gives two vertices a comparator for an arbitrary ordering which can for example be used to avoid
     * computing edges twice.
     *
     * @param rhs   DCELVertex to compare against.
     * @return      Boolean whether this is bigger than rhs.
     */
    inline bool operator< (const DCELVertex& rhs) const {
        return (this->get_x() > rhs.get_x()) || (this->get_x() == rhs.get_x() && this->get_y() > rhs.get_y());
    }

    inline std::string get_string() const {
        return std::to_string(this->get_x()) + "," + std::to_string(this->get_y());
    }
};

#endif //NEAR_DELAUNAY_DCEL_VERTEX_H
