#ifndef NEAR_DELAUNAY_HASHABLEDCEL_H
#define NEAR_DELAUNAY_HASHABLEDCEL_H


#include <unordered_map>
#include <utility>
#include "DCEL.h"
#include "../../algorithms/EdgeBasedAlgorithm.h"
#include <iostream>
#include <bitset>

/**
 * A subclass of the DCEL, which keeps a running hash of itself, which can be easily accessed as well as a list of
 * scores each of its edges archive for specified algorithms.
 */
class HashableDCEL : public DCEL, public std::enable_shared_from_this<HashableDCEL> {
private:
    // The current hash of the DCEL. The hash is a 400 bit variable where each bit represents one edge that the triangulation
    // could possibly have. Each vertex is associated to a number and edge (v_1, v_2) is associated to bit
    // (std::min(v_1, v_2) * this->vertices.size() + std::max(v_1, v_2)) where that bit is 1 iff the triangulation contains that particular edge.
    std::bitset<400> hash;

    // A map which associates each vertex {v_1, ..., v_n} to a unique number in {0, ..., n-1}.
    std::unordered_map<std::shared_ptr<DCELVertex>, int> vertex_numbering;

    // The sum of the length of all the edges in the triangulation which is dynamically updated.
    double edge_length_sum;

    // A vector of all the algorithms for which the scores should be tracked in this DCEL.
    std::vector<std::shared_ptr<EdgeBasedAlgorithm>> algorithms;

    // For each algorithm there is a map from each edge to the score which that edge gets.
    // Currently both a halfedge and it's twin are contained in this map leading to duplicate data but easier access.
    // This could be changed without much loss in performance to safe 50% of the space. (Same goes for the algorithm_scores.)
    std::vector<std::unordered_map<std::shared_ptr<Halfedge>, double>> edge_scores;

    // For each algorithm, there is a sorted multiset of all the edge scores.
    // Currently both a halfedge and it's twin are contained in this map leading to duplicate data but easier access.
    // This could be changed without much loss in performance to safe 50% of the space. (Same goes for the edge_scores.)
    std::vector<std::multiset<double>> algorithm_scores;

    /**
     * Update the hash to contain/not contain the edge anymore.
     *
     * @param edge  The edge which should be toggled.
     */
    void toggle_edge_to_hash(const std::shared_ptr<Halfedge>& edge) {
        toggle_edge_to_hash(vertex_numbering[edge->get_target()], vertex_numbering[edge->get_twin()->get_target()]);
    }

    /**
     * Update the hash to contain/not contain the edge from the vertex1 to vertex2 anymore.
     *
     * @param vertex1  The first vertex.
     * @param vertex2  The second vertex.
     */
    void toggle_edge_to_hash(std::shared_ptr<DCELVertex> vertex1, std::shared_ptr<DCELVertex> vertex2) {
        toggle_edge_to_hash(vertex_numbering[vertex1], vertex_numbering[vertex2]);
    }

    /**
     * Update the hash to contain/not contain the edge from the vertex with number no_1 to vertex with number no_2 anymore.
     *
     * @param no_1  Number of the first vertex of the edge.
     * @param no_2  Number of the second vertex of the edge.
     */
    void toggle_edge_to_hash(int no_1, int no_2) {
        int index = (std::min(no_1, no_2) * this->vertices.size() + std::max(no_1, no_2));
        hash.flip(index);
    }

    /**
     * Recalculate the edge_scores and algorithm_scores based for the given edge and all associated edges.
     *
     * @param edge  The edge which should be recalculated.
     */
    void recalculate_scores(std::shared_ptr<Halfedge> edge);

    /**
     * Recalculate the edge_score and algortihm_score for a specific algorithm and edge.
     * None of the surrounding edges are being recalculated.
     *
     * @param edge              The specific edge which should be recalculated.
     * @param algorithm_index   The algorithm for which the edge should be recalculated.
     */
    void recalculate_scores_single_edge(std::shared_ptr<Halfedge> edge, int algorithm_index);

public:
    /**
     * Create a Hashable DCEL from the parts of an existing DCEL.
     *
     * @param v The DCEL vertices of the existing DCEL.
     * @param h The halfedges of the existing DCEL.
     * @param f The faces of the existing DCEL.
     * @param a The algorithms which should be kept track of in the HashableDCEL.
     */
    HashableDCEL(const std::list<std::shared_ptr<DCELVertex>> v, const std::list<std::shared_ptr<Halfedge>> h,
                 const std::list<std::shared_ptr<Face>> f, std::vector<std::shared_ptr<EdgeBasedAlgorithm>> a);

    // -------------------------- override functions that are currently not supported ----------------------------
    void manual_add_vertex(std::shared_ptr<DCELVertex> v) override {
        throw std::runtime_error("HashableDCEL does not support manual_add_vertex()");
    }

    void manual_add_edge(std::shared_ptr<Halfedge> e) override {
        throw std::runtime_error("HashableDCEL does not support manual_add_edge()");
    }

    void manual_add_face(std::shared_ptr<Face> f) override {
        throw std::runtime_error("HashableDCEL does not support manual_add_face()");
    }

    void initial_vertices(std::shared_ptr<DCELVertex> v1, std::shared_ptr<DCELVertex> v2) override {
        throw std::runtime_error("HashableDCEL does not support initial_vertices()");
    };

    void add_edge_at(std::shared_ptr<DCELVertex> v, std::shared_ptr<Halfedge> h) override {
        throw std::runtime_error("HashableDCEL does not support add_edge_at()");
    };

    void add_vertex_at(std::shared_ptr<DCELVertex> v, std::shared_ptr<Halfedge> h) override {
        throw std::runtime_error("HashableDCEL does not support add_vertex_at()");
    };

    // ---------------------------------- end of override -------------------------------------------------------

    /**
     * Return the current hash of the DCEL.
     *
     * @return  The hash.
     */
    std::bitset<400> get_hash() {
        return hash;
    }

    /**
     * Calculate and return the hash that the DCEL would have if h were flipped.
     *
     * @param h     The halfedge that would be flipped.
     * @return      The hash the DCEL would have if h would be flipped.
     */
    std::bitset<400> get_flipped_hash(std::shared_ptr<Halfedge> h);

    void flip_edge(std::shared_ptr<Halfedge> h) override {
        toggle_edge_to_hash(h);
        edge_length_sum -= h->get_length();

        DCEL::flip_edge(h);

        edge_length_sum += h->get_length();
        toggle_edge_to_hash(h);
        recalculate_scores(h);
    }

    /**
     * Get the multiset of edge scores for the algorithm with the specified index.
     *
     * @param       algorithm_index
     * @return      The multiset of scores for the requested algorithm.
     */
    std::multiset<double> get_multiset(int algorithm_index) {
        return algorithm_scores[algorithm_index];
    }

    /**
     * Get the edge length sum.
     *
     * @return  The edge length sum.
     */
    double get_edge_length_sum() {
        return edge_length_sum;
    }
};


#endif //NEAR_DELAUNAY_HASHABLEDCEL_H
