#ifndef NEAR_DELAUNAY_FACE_H
#define NEAR_DELAUNAY_FACE_H

#include <vector>
#include <set>
#include <unordered_set>
#include <memory>

#include "Halfedge.h"

/**
 * Face used in the DCEL data structure.
 */
class Face {
private:
    std::shared_ptr<Halfedge> edge;

public:
    // empty constructor
    Face(){}

    // Getter
    std::shared_ptr<Halfedge> get_edge_pointer() const {
        return edge;
    }

    // Setter
    void set_edge(std::shared_ptr<Halfedge> edge) {
        this->edge = std::move(edge);
    }

    // Return all halfedges adjacent to face
    std::vector<std::shared_ptr<Halfedge>> get_adjacent_edges() const;

    // Return all adjacent faces
    std::unordered_set<std::shared_ptr<Face>> get_adjacent_faces() const;

    bool operator== (const Face f) const {
        return (this == &f);
    }
};

#endif //NEAR_DELAUNAY_FACE_H
