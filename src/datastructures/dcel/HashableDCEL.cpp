#include <iostream>
#include "HashableDCEL.h"
#include <memory>

HashableDCEL::HashableDCEL(const std::list<std::shared_ptr<DCELVertex>> v, const std::list<std::shared_ptr<Halfedge>> h,
                           const std::list<std::shared_ptr<Face>> f,
                           std::vector<std::shared_ptr<EdgeBasedAlgorithm>> a) : DCEL(v, h, f) , algorithms(std::move(a)) {

    hash = 0;
    edge_length_sum = 0;

    // 1. Assign numbers to the vertices.
    int counter = 0;
    for (const auto& vertex : v) {
        vertex_numbering[vertex] = counter;
        counter++;
    }

    // 2. Create the hash.
    for (const auto& edge : h) {
        if (vertex_numbering[edge->get_target()] > vertex_numbering[edge->get_twin()->get_target()]) {
            toggle_edge_to_hash(edge);
            edge_length_sum += edge->get_length();
        }
    }

    // workaround for being able to create smart pointers from the constructor as found here: https://forum.libcinder.org/topic/solution-calling-shared-from-this-in-the-constructor
    auto wptr = std::shared_ptr<HashableDCEL>( this, [](HashableDCEL*){} );

    // 3. Calculate the algorithm scores for the first time. These will be dynamically updated on edge flips.
    for (int i = 0; i < algorithms.size(); i++) {
        edge_scores.push_back(algorithms[i]->calculate_metric(shared_from_this()));

        std::multiset<double> new_scores;
        for (const auto &edge_result : edge_scores[i]) {
            new_scores.insert(edge_result.second);
        }

        algorithm_scores.push_back(new_scores);
    }
}

void HashableDCEL::recalculate_scores(std::shared_ptr<Halfedge> edge) {
    for (int i = 0; i < algorithms.size(); i++) {
        recalculate_scores_single_edge(edge, i);
        recalculate_scores_single_edge(edge->get_next(), i);
        recalculate_scores_single_edge(edge->get_next()->get_next(), i);
        recalculate_scores_single_edge(edge->get_twin()->get_next(), i);
        recalculate_scores_single_edge(edge->get_twin()->get_next()->get_next(), i);
    }
}

void HashableDCEL::recalculate_scores_single_edge(std::shared_ptr<Halfedge> edge, int algorithm_index) {
    if (edge->get_face() == get_outer_face() || edge->get_twin()->get_face() == get_outer_face()) {
        return;
    }

    // remove entries for both sides of halfedge
    for (int i = 0; i < 2; i++) {
        auto itr = algorithm_scores[algorithm_index].find(edge_scores[algorithm_index][edge]);
        if (itr != algorithm_scores[algorithm_index].end()) {
            algorithm_scores[algorithm_index].erase(itr);
        } else {
            throw std::runtime_error("The score of the edge trying to be flipped in the HashableDCEL does not seem "
                                            "to exist.");
        }
    }

    // add the new entries.
    double new_score = algorithms[algorithm_index]->calculate_edge(edge, shared_from_this());
    edge_scores[algorithm_index][edge] = new_score;
    edge_scores[algorithm_index][edge->get_twin()] = new_score;

    algorithm_scores[algorithm_index].insert(new_score);
    algorithm_scores[algorithm_index].insert(new_score);

}

std::bitset<400> HashableDCEL::get_flipped_hash(std::shared_ptr<Halfedge> h) {
    if (!is_edge_flippable(h)) {
        return hash;
    }

    toggle_edge_to_hash(h);
    toggle_edge_to_hash(h->get_next()->get_target(), h->get_twin()->get_next()->get_target());

    std::bitset<400> flipped_hash = hash;

    toggle_edge_to_hash(h->get_next()->get_target(), h->get_twin()->get_next()->get_target());
    toggle_edge_to_hash(h);

    return flipped_hash;
}

