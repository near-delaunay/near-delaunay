#ifndef NEAR_DELAUNAY_DCEL_H
#define NEAR_DELAUNAY_DCEL_H

#include <cmath>
#include <list>

#include "DCELVertex.h"
#include "Halfedge.h"
#include "Face.h"

/**
 * Doubly-Connected Edge List data structure.
 */
class DCEL {
protected:
    std::list<std::shared_ptr<DCELVertex>> vertices;
    std::list<std::shared_ptr<Halfedge>> edges;
    std::list<std::shared_ptr<Face>> faces;

    std::shared_ptr<Face> outer_face;

    /**
     * Calculate the outer face of a DCEL which is assumed to be a triangulation
     * (though it doesn't have to fill the entire convex hull).
     * Sole assumptions:
     *  1. All inner faces have exactly 3 vertices associated to them.
     *
     * @return The outer face.
     */
    std::shared_ptr<Face> calculate_outer_face();

public:
    // empty constructor
    DCEL(){}

    // constructor from previous structure
    DCEL(const std::list<std::shared_ptr<DCELVertex>> v, const std::list<std::shared_ptr<Halfedge>> h, const std::list<std::shared_ptr<Face>> f)
        : vertices(v), edges(h), faces(f) {}

    // Getters
    std::list<std::shared_ptr<DCELVertex>> get_vertices() const {
        return this->vertices;
    }

    std::list<std::shared_ptr<Halfedge>> get_edges() const {
        return this->edges;
    }

    std::list<std::shared_ptr<Face>> get_faces() const {
        return this->faces;
    }

    virtual // Manual operations - tread carefully
    void manual_add_vertex(std::shared_ptr<DCELVertex> v) {
        vertices.push_back(v);
        this->outer_face = nullptr;
    }

    virtual void manual_add_edge(std::shared_ptr<Halfedge> e) {
        edges.push_back(e);
        this->outer_face = nullptr;
    }

    virtual void manual_add_face(std::shared_ptr<Face> f) {
        faces.push_back(f);
        this->outer_face = nullptr;
    }

    // Add initial 2 vertices, constructing outer face
    virtual void initial_vertices(std::shared_ptr<DCELVertex> v1, std::shared_ptr<DCELVertex> v2);

    // Add edge between vertex v and h.target, where h.face is split
    virtual void add_edge_at(std::shared_ptr<DCELVertex> v, std::shared_ptr<Halfedge> h);

    // Insert new vertex connected with a new edge to h.target - ensure correct edge chosen in regard to face!
    virtual void add_vertex_at(std::shared_ptr<DCELVertex> v, std::shared_ptr<Halfedge> h);

    // Flip edge h
    virtual void flip_edge(std::shared_ptr<Halfedge> h);

    std::shared_ptr<Face> get_outer_face() {
        if (this->outer_face == nullptr) {
            this->outer_face = calculate_outer_face();
        }

        return this->outer_face;
    }

    /**
     * Check if a given edge is flippable.
     *
     * @param edge
     * @return  True if edge is flippable.
     */
    bool is_edge_flippable(std::shared_ptr<Halfedge> edge) {
        // Flipping on outer edges not possible
        if (edge->get_face().get() == this->get_outer_face().get() ||
                edge->get_twin()->get_face().get() == this->get_outer_face().get()) return false;

        // Check if the resulting triangulation is still valid.
        // This check prevents overlapping edges if the two vertices that the edge should be flipped to cannot see
        // each other.
        if (edge->get_angle_to_next() + edge->get_twin()->get_previous()->get_angle_to_next() + 0.0001 >= M_PI ||
                edge->get_twin()->get_angle_to_next() + edge->get_previous()->get_angle_to_next() + 0.0001 >= M_PI) return false;

        return true;
    }

    /**
     * Get all the edges of the DCEL that can currently be flipped.
     *
     * @return  A vector of all edges that can be flipped.
     */
    std::vector<std::shared_ptr<Halfedge>> get_flippable_edges();
};

#endif //NEAR_DELAUNAY_DCEL_H
