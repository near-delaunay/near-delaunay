#ifndef NEAR_DELAUNAY_GRAPHVERTEX_H
#define NEAR_DELAUNAY_GRAPHVERTEX_H

#include <vector>
#include <memory>
#include <algorithm>
#include <unordered_set>

/**
 * Vertex used in a graph. Contains (x,y) coordinates and a list associated vertices in the form of edges.
 */
class GraphVertex {
private:
    int x;
    int y;
    std::vector<std::weak_ptr<GraphVertex>> edges;

public:
    GraphVertex(int x, int y) : x(x), y(y) {}

    int get_x() const {
        return this->x;
    }

    int get_y() const {
        return this->y;
    }

    std::vector<std::weak_ptr<GraphVertex>> get_edges() {
        return this->edges;
    }

    void add_edge(const std::weak_ptr<GraphVertex>& edge) {
        this->edges.push_back(edge);
    }

    /**
     * Check if other vertex has equal position.
     * @param vertex Vertex to check against.
     * @return       Boolean whether pos is equal.
     */
    bool is_equal_pos(const std::shared_ptr<GraphVertex>& vertex) {
        return this->get_x() == vertex->get_x() && this->get_y() == vertex->get_y();
    }

    void remove_all_edges() {
        this->edges = {};
    }

    inline bool operator< (const GraphVertex& rhs){
        return (this->get_x() > rhs.get_x()) || (this->get_x() == rhs.get_x() && this->get_y() > rhs.get_y());
    }
};

#endif //NEAR_DELAUNAY_GRAPHVERTEX_H
