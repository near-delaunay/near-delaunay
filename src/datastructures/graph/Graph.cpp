#include "Graph.h"

bool Graph::is_isomorphic(std::shared_ptr<Graph> g) {
    if (this->vertices.size() != g->get_vertices().size()) {
        return false;
    }

    std::vector<std::shared_ptr<GraphVertex>> all_vertices = g->get_vertices();

    for (const std::shared_ptr<GraphVertex>& vertex : this->get_vertices()) {
        std::shared_ptr<GraphVertex> other_vertex = nullptr;

        // find equivalent vertex in other graph
        for (const std::shared_ptr<GraphVertex>& check_vertex : all_vertices) {
            if (vertex->is_equal_pos(check_vertex)) {
                other_vertex = check_vertex;
                break;
            }
        }

        // check if equivalent vertex found
        if (other_vertex == nullptr || other_vertex->get_edges().size() != vertex->get_edges().size()) {
            return false;
        }

        // get all edges of vertex
        std::unordered_set<std::pair<double, double>, pair_hash> found_edges;
        for (auto edge : vertex->get_edges()) {
            found_edges.insert({edge.lock()->get_x(), edge.lock()->get_y()});
        }

        // check if edges are also in equivalent vertex
        for (auto edge : other_vertex->get_edges()) {
            if (found_edges.find({edge.lock()->get_x(), edge.lock()->get_y()}) == found_edges.end()) {
                return false;
            }
        }
    }
    return true;
}
