#ifndef NEAR_DELAUNAY_GRAPH_H
#define NEAR_DELAUNAY_GRAPH_H

#include <HashHelper.h>
#include "GraphVertex.h"

#include <vector>
#include <memory>
#include <set>
#include <unordered_set>

/**
 * Graph used for representing triangulations. Stores a list of vertices.
 */
class Graph {
private:
    std::vector<std::shared_ptr<GraphVertex>> vertices;

public:
    Graph(){}

    std::vector<std::shared_ptr<GraphVertex>> get_vertices() {
        return this->vertices;
    }

    void add_vertex(const std::shared_ptr<GraphVertex> v) {
        this->vertices.push_back(v);
    }

    void add_edge(const std::shared_ptr<GraphVertex> v1, const std::shared_ptr<GraphVertex> v2) {
        v1->add_edge(v2);
        v2->add_edge(v1);
    }

    /**
     * Check if the other graph is isomorphic to this. (Vertex pos have to be identical!)
     * @param g     Graph to check against
     * @return      Boolean whether g is isomorphic to this.
     */
    bool is_isomorphic(std::shared_ptr<Graph> g);
};
#endif //NEAR_DELAUNAY_GRAPH_H
