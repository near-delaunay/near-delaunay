#ifndef NEAR_DELAUNAY_RANGENODE_CPP
#define NEAR_DELAUNAY_RANGENODE_CPP

#include <iostream>
#include "RangeNode.h"

template<typename T>
std::shared_ptr<RangeNode<T>> RangeNode<T>::find_extremum(std::vector<std::pair<double, double>> ranges, bool find_min) {
    // Check if it is the 1D case.
    if (ranges.size() == 1) {
        return find_extremum_in_1D(ranges.at(0), find_min);
    }

    std::pair<double, double> current_range = ranges.at(0);

    std::shared_ptr<RangeNode<T>> split_node = find_split_node(current_range);

    std::vector<std::pair<double, double>> remaining_ranges =
            std::vector<std::pair<double, double>> (ranges.cbegin() + 1, ranges.cend());

    if (split_node->get_associated_vertex()) {
        return split_node;
    }


    std::shared_ptr<RangeNode<T>> extremum_left = split_node->get_left_subtree()->report_right_trees(current_range.first, remaining_ranges, find_min);
    std::shared_ptr<RangeNode<T>> extremum_right = split_node->get_right_subtree()->report_left_trees(current_range.second, remaining_ranges, find_min);

    if (!extremum_left) return extremum_right;
    if (!extremum_right) return extremum_left;

    return (extremum_left->get_lowest_level_score() > extremum_right->get_lowest_level_score()) xor find_min ? extremum_left : extremum_right;
}

template<typename T>
std::shared_ptr<RangeNode<T>> RangeNode<T>::find_extremum_in_1D(std::pair<double, double> range, bool find_min) {
    // if it is a leaf, return associated vertex if appropriate.
    if (this->associated_vertex) {
        // check if point itself is in range (this->min_right_sub_tree for leaf is simply the value)
        if (this->min_right_sub_tree <= range.first || this->min_right_sub_tree >= range.second) {
            return nullptr;
        }

        return std::make_shared<RangeNode<T>>((*this));
    }

    // for non-leaves traverse down to the leaf level.
    if (find_min) {
        if (range.first < this->max_left_sub_tree) {
            return this->get_left_subtree()->find_extremum_in_1D(range, find_min);
        } else {
            return this->get_right_subtree()->find_extremum_in_1D(range, find_min);
        }
    } else {
        if (range.second > this->min_right_sub_tree) {
            return this->get_right_subtree()->find_extremum_in_1D(range, find_min);
        } else {
            return this->get_left_subtree()->find_extremum_in_1D(range, find_min);
        }
    }
}

template<typename T>
std::shared_ptr<RangeNode<T>> RangeNode<T>::find_split_node(std::pair<double, double> range) {
    RangeNode<T> *split_node = this;

    while (!split_node->associated_vertex && (range.second <= split_node->max_left_sub_tree || range.first > split_node->max_left_sub_tree)) {
        if (range.second <= split_node->max_left_sub_tree) {
            split_node = split_node->get_left_subtree().get();
        } else {
            split_node = split_node->get_right_subtree().get();
        }
    }

    return std::make_shared<RangeNode<T>>((*split_node));
}

template<typename T>
std::shared_ptr<RangeNode<T>> RangeNode<T>::report_right_trees(double min_value, std::vector<std::pair<double, double>> remaining_ranges,
        bool find_min) {
    std::shared_ptr<RangeNode<T>> min_node;

    RangeNode<T> *boundary = this;

    // traverse down to where min_value would be and always return the right subtree if traversing to the left.
    while (!boundary->get_associated_vertex()) {
        if (boundary->get_left_subtree()->get_min_right_sub_tree() >= min_value) {
            std::shared_ptr<RangeNode<T>> new_node = boundary->get_right_subtree()->get_associated_structure()->find_extremum(remaining_ranges, find_min);

            if (new_node) {
                if (!min_node || ((new_node->get_lowest_level_score() > min_node->get_lowest_level_score()) xor find_min)) {
                    min_node = new_node;
                }
            }

            boundary = boundary->get_left_subtree().get();
        } else {
            boundary = boundary->get_right_subtree().get();
        }
    }

    return min_node;
}

template<typename T>
std::shared_ptr<RangeNode<T>> RangeNode<T>::report_left_trees(double max_value, std::vector<std::pair<double, double>> remaining_ranges,
        bool find_min) {
    std::shared_ptr<RangeNode<T>> min_node;

    RangeNode<T> *boundary = this;

    // traverse down to where max_value would be and always return the left subtree if traversing to the right.
    while (!boundary->get_associated_vertex()) {
        if (boundary->get_right_subtree()->get_max_left_sub_tree() <= max_value) {
            std::shared_ptr<RangeNode<T>> new_node = boundary->get_left_subtree()->get_associated_structure()->find_extremum(remaining_ranges, find_min);

            if (new_node) {
                if (!min_node || ((new_node->get_lowest_level_score() > min_node->get_lowest_level_score()) xor find_min)) {
                    min_node = new_node;
                }
            }

            boundary = boundary->get_right_subtree().get();
        } else {
            boundary = boundary->get_left_subtree().get();
        }
    }

    return min_node;
}

#endif //NEAR_DELAUNAY_RANGENODE_H