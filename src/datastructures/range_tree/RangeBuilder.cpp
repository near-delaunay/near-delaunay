#ifndef NEAR_DELAUNAY_RANGEBUILDER_CPP
#define NEAR_DELAUNAY_RANGEBUILDER_CPP

#include <algorithm>
#include <cmath>
#include "RangeBuilder.h"
#include "../graph/GraphVertex.h"


template<typename T>
std::shared_ptr<RangeNode<T>> RangeBuilder<T>::build_tree(std::vector<std::shared_ptr<T>> vertices, std::vector<double (*)(const std::shared_ptr<T>)> functions) {
    double (*value)(std::shared_ptr<T>) = functions.at(0);

    std::sort(vertices.begin(), vertices.end(), [value]( const auto& lhs, const auto& rhs )
    {
        return (*value)(lhs) < (*value)(rhs);
    });

    int power = (int) std::log2(vertices.size());
    int remainder = (vertices.size() - std::pow(2, power));

    // the tree is build from bottom to top. The build_rank vector keeps track of the nodes that should be joined next.
    std::vector<std::pair<std::shared_ptr<RangeNode<T>>, std::pair<double, double>>> build_rank;

    // as the number of vertices is not necessarily a power of two, some nodes have to be joined one level deeper.
    // This method guarantees that all leaves on the left side of the tree have at most +1 height to nodes on the right side of the tree.
    for (int i = 0; i < remainder * 2; i += 2) {
        std::shared_ptr<RangeNode<T>> left
                = std::make_shared<RangeNode<T>>(RangeNode(vertices.at(i), (*value)(vertices.at(i)), (*functions.at(functions.size() - 1))(vertices.at(i))));
        std::shared_ptr<RangeNode<T>> right
                = std::make_shared<RangeNode<T>>(RangeNode<T>(vertices.at(i + 1), (*value)(vertices.at(i + 1)), (*functions.at(functions.size() - 1))(vertices.at(i + 1))));

        std::shared_ptr<RangeNode<T>> node
                = std::make_shared<RangeNode<T>>(left, right, (*value)(vertices.at(i)), (*value)(vertices.at(i + 1)));

        if (functions.size() > 1) {

            left->set_associated_structure(RangeBuilder<T>::build_tree({vertices.at(i)},std::vector<double (*)(const std::shared_ptr<T>)>(functions.cbegin() + 1, functions.cend())));
            right->set_associated_structure(RangeBuilder<T>::build_tree({vertices.at(i + 1)},std::vector<double (*)(const std::shared_ptr<T>)>(functions.cbegin() + 1, functions.cend())));
            node->set_associated_structure(RangeBuilder<T>::build_tree({vertices.at(i), vertices.at(i + 1)},std::vector<double (*)(const std::shared_ptr<T>)>(functions.cbegin() + 1, functions.cend())));
        }

        build_rank.push_back({node, {right->get_max_left_sub_tree(), left->get_min_right_sub_tree()}});
    }

    // add remaining leaf nodes that are not yet joined up.
    for (int i = remainder * 2; i < vertices.size(); i++) {
        std::shared_ptr<RangeNode<T>> node
                = std::make_shared<RangeNode<T>>(vertices.at(i), (*value)(vertices.at(i)), (*functions.at(functions.size() - 1))(vertices.at(i)));

        if (functions.size() > 1) {
            node->set_associated_structure(RangeBuilder<T>::build_tree({vertices.at(i)},
                                                                       std::vector<double (*)(const std::shared_ptr<T>)>(functions.cbegin() + 1, functions.cend())));
        }

        build_rank.push_back({node,
                              {(*value)(vertices.at(i)), (*value)(vertices.at(i))}});
    }

    // build the tree from bottom layer up.
    while (build_rank.size() > 1) {
        std::vector<std::pair<std::shared_ptr<RangeNode<T>>, std::pair<double, double>>> next_rank;

        for (int i = 0; i < build_rank.size(); i += 2) {
            std::shared_ptr<RangeNode<T>> node
                    = std::make_shared<RangeNode<T>>(build_rank.at(i).first, build_rank.at(i + 1).first, build_rank.at(i).second.first, build_rank.at(i + 1).second.second);

            next_rank.push_back({node, {build_rank.at(i + 1).second.first, build_rank.at(i).second.second}});

            if (functions.size() > 1) {
                //TODO: the querying of the subnodes can definitely be done faster by using the fact that the nodes
                // are already sorted in vertices
                node->set_associated_structure(RangeBuilder<T>::build_tree(node->get_sub_nodes(),
                                                                           std::vector<double (*)(const std::shared_ptr<T>)>(functions.cbegin() + 1, functions.cend())));
            }
        }

        build_rank = next_rank;
    }

    return build_rank.at(0).first;
}

#endif //NEAR_DELAUNAY_RANGEBUILDER_CPP