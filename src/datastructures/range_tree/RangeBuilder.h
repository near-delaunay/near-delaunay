#ifndef NEAR_DELAUNAY_RANGEBUILDER_H
#define NEAR_DELAUNAY_RANGEBUILDER_H

#include <memory>
#include <vector>
#include <iostream>
#include "RangeNode.h"
#include "../graph/GraphVertex.h"

/**
 * Helper class that builds a range tree based on the given input.
 *
 * @tparam T    The class for which a range tree should be build.
 */
template<class T>
class RangeBuilder {
public:
    /**
     * Build a range tree with balanced depth based on the vertices (which will be used as leaf nodes) and the functions used to score each leaf.
     * If multiple functions are supplied a multi-dimensional tree is build.
     *
     * @param vertices      The vertices for which the tree should be build.
     * @param functions     The functions which score the vertices per dimension.
     * @return              The root node of the first dimension.
     */
    static std::shared_ptr<RangeNode<T>> build_tree(std::vector<std::shared_ptr<T>> vertices,
            std::vector<double (*)(const std::shared_ptr<T>)> functions);

};

#include "RangeBuilder.cpp"

#endif //NEAR_DELAUNAY_RANGEBUILDER_H
