#ifndef NEAR_DELAUNAY_RANGENODE_H
#define NEAR_DELAUNAY_RANGENODE_H

#include <vector>
#include <langinfo.h>
#include "../dcel/DCELVertex.h"

/**
 * Node which can be used as building blocks for range trees over one or multiple dimensions.
 * It is slightly adapted from normal range trees to support search for both minima and maxima over multiple dimensions
 * by not only storing the score for which holds that all leaves in the left subtree are smaller than or equal to this score but storing:
 *  1. max_left_sub_tree: all scores of leaves in the left subtree are smaller or equal to this value and this value exists in the left subtree.
 *  2. min_right_sub_tree: all scores of leaves in the right subtree are bigger or equal to this value and this value exists in the right subtree.
 *
 * @tparam T    The class for which the range tree should be build.
 */
template <typename T>
class RangeNode {
private:
    // Min leaf value stored in the right sub tree. If the node is a leaf, this is equivalent to the score of the leaf.
    double min_right_sub_tree;

    // Max leaf value stored in the left sub tree. If the node is a leaf, this is equivalent to the score of the leaf.
    double max_left_sub_tree;

    // For leaf nodes; the vertex associated to the node. For non-leaf-nodes this value is a nullptr.
    std::shared_ptr<T> associated_vertex;

    // For leaf nodes: the score the associated vertex has in the final dimension.
    // So if we have a 2D tree and this node represents the first dimension, the score stored in this field will be the score of the second dimension.
    // For non-leaf-nodes this value is null.
    double lowest_level_score;

    // For higher levels of multi-dimensional trees: the structure associated to the current node one dimension lower.
    std::shared_ptr<RangeNode<T>> associated_structure;

    // For non-leaf-nodes: the right subtrees stored at the current node. For leaf nodes: nullptr.
    std::shared_ptr<RangeNode<T>> right_subtree;

    // For non-leaf-nodes: the left subtrees stored at the current node. For leaf nodes: nullptr.
    std::shared_ptr<RangeNode<T>> left_subtree;

    /**
     * Return the extremum for 1-dimensional trees.
     *
     * @param range     Range in which the extremum should lie.
     * @param find_min  Boolean whether the minimum or maximum should be found.
     * @return          The requested extremum in the given range as a RangeNode.
     */
    std::shared_ptr<RangeNode<T>> find_extremum_in_1D(std::pair<double, double> range, bool find_min);

    /**
     * Find the split node in the current dimension based on the range. The split node for a range [x, y] is defined
     * as the node where the paths from the root to x and the root to y split.
     *
     * @param range The range for which the split node should be found.
     * @return      The specified split node.
     */
    std::shared_ptr<RangeNode<T>> find_split_node(std::pair<double, double> range);

    /**
     * Report all right sub trees that are bigger than the min_value and search them for the extremum. Of all the found values return the min/max depending on find_min.
     *
     * @param min_value         Min value that the right subtrees should be bigger than.
     * @param remaining_ranges  The remaining ranges that have to be taken into account when reporting the subtree. These are the ranges that have not yet been considered in the range search.
     * @param find_min          Boolean whether the minimum or maximum should be found.
     *
     * @return                  The extremum of all the subtrees.
     */
    std::shared_ptr<RangeNode<T>> report_right_trees(double min_value, std::vector<std::pair<double, double>> remaining_ranges, bool find_min);

    /**
     * Report all left sub trees that are smaller than the max_value and search them for the extremum. Of all the found values return the min/max depending on find_min.
     *
     * @param max_value         Max value that the left subtrees should be smaller than.
     * @param remaining_ranges  The remaining ranges that have to be taken into account when reporting the subtree. These are the ranges that have not yet been considered in the range search.
     * @param find_min          Boolean whether the minimum or maximum should be found.
     *
     * @return                  The extremum of all the subtrees.
     */
    std::shared_ptr<RangeNode<T>> report_left_trees(double max_value, std::vector<std::pair<double, double>> remaining_ranges, bool find_min);

public:
    /**
     * Initializer for leaves.
     *
     * @param associated_vertex     Vertex associated to the leaf.
     * @param score                 The score of the leaf.
     * @param lowest_level          The score of the leaf with the lowest level evaluation function.
     */
    RangeNode(std::shared_ptr<T> associated_vertex, double score, double lowest_level)
            : associated_vertex(associated_vertex), max_left_sub_tree(score), min_right_sub_tree(score), lowest_level_score(lowest_level),
            associated_structure(nullptr), left_subtree(nullptr), right_subtree(nullptr) {};

    /**
     * Initializer for non-leaf-nodes.
     *
     * @param left          Left subtree.
     * @param right         Right subtree.
     * @param max_left      Max leaf score in the left subtree.
     * @param min_right     Min leaf score in the right subtree.
     */
    RangeNode(std::shared_ptr<RangeNode<T>> left, std::shared_ptr<RangeNode<T>> right, double max_left, double min_right)
            : left_subtree(left), right_subtree(right), max_left_sub_tree(max_left), min_right_sub_tree(min_right),
            associated_structure(nullptr), associated_vertex(nullptr) {};

    // Getters and Setters
    void set_associated_structure(std::shared_ptr<RangeNode<T>> associated_structure) {
        this->associated_structure = associated_structure;
    }

    std::shared_ptr<RangeNode<T>> get_associated_structure() {
        return this->associated_structure;
    }

    double get_max_left_sub_tree() {
        return this->max_left_sub_tree;
    }

    double get_min_right_sub_tree() {
        return this->min_right_sub_tree;
    }

    std::shared_ptr<T> get_associated_vertex() {
        return this->associated_vertex;
    }

    std::shared_ptr<RangeNode<T>> get_left_subtree() {
        return this->left_subtree;
    }

    std::shared_ptr<RangeNode<T>> get_right_subtree() {
        return this->right_subtree;
    }

    double get_lowest_level_score() {
        return this->lowest_level_score;
    }

    //TODO: can this be done faster? There might be faster cpp operations to achieve the same goal.
    /**
     * Return all associated vertices stored in the leaves of the tree rooted at the current node.
     *
     * @return  A vector of shared pointers to all the associated vertices.
     */
    std::vector<std::shared_ptr<T>> get_sub_nodes() {
        if (this->associated_vertex != nullptr) {
            return {this->associated_vertex};
        }

        std::vector<std::shared_ptr<T>> left_sub_nodes = this->left_subtree->get_sub_nodes();
        std::vector<std::shared_ptr<T>> right_sub_nodes = this->right_subtree->get_sub_nodes();

        left_sub_nodes.insert(left_sub_nodes.end(), right_sub_nodes.begin(), right_sub_nodes.end());

        return left_sub_nodes;
    }

    /**
     * Find extremum in multiple dimensions based on the given ranges.
     *
     * @param ranges    Vector of pairs of the ranges for which the extremum should be found.
     *                  So for a 2d query over the range [00, 10] in the first dimension and [20, 30] in the second,
     *                  the ranges should be {{0, 10}, {20, 30}}.
     * @param find_min  Boolean whether the minimum or maximum should be found.
     * @return          A shared pointer to the RangeNode which contains the extremum in the specified range.
     */
    std::shared_ptr<RangeNode<T>> find_extremum(std::vector<std::pair<double, double>> ranges, bool find_min);
};

#include "RangeNode.cpp"

#endif //NEAR_DELAUNAY_RANGENODE_H
