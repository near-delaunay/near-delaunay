#ifndef NEAR_DELAUNAY_IPECIRCLE_H
#define NEAR_DELAUNAY_IPECIRCLE_H

#include <cmath>
#include "IpeEllipse.h"

class IpeCircle : public IpeEllipse {
private:
    static std::pair<double, double> center(double ax, double ay,
                                            double bx, double by,
                                            double cx, double cy);

public:
    IpeCircle(double center_x, double center_y, double radius, std::string color) :
            IpeEllipse(center_x, center_y, radius, 0, 0, radius, color){}

    IpeCircle(const double ax, const double ay,
                const double bx, const double by,
                const double cx, const double cy,
                std::string color);

};

#endif //NEAR_DELAUNAY_IPECIRCLE_H
