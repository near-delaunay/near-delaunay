#ifndef NEAR_DELAUNAY_IPEGRAPH_H
#define NEAR_DELAUNAY_IPEGRAPH_H


#include "IpeDrawable.h"
#include "IpeVertex.h"
#include "IpePath.h"
#include <graph/Graph.h>
#include <climits>

/**
 * Drawable object that represents a graph on a layer.
 * A graph is defined via an instance of the Graph.h object.
 */
class IpeGraph : public IpeDrawable {
private:
    std::shared_ptr<Graph> graph;

    std::vector<std::shared_ptr<IpeVertex>> vertices;
    std::vector<std::shared_ptr<IpePath>> edges;


public:
    IpeGraph(std::shared_ptr<Graph> graph, std::string color)
            : IpeDrawable(color), graph(graph) {
        int min_x, min_y, max_x, max_y;
        min_x = INT_MAX;
        min_y = INT_MAX;
        max_x = INT_MIN;
        max_y = INT_MIN;

        for (auto vertex : graph->get_vertices()) {
            if (vertex->get_x() > max_x) {
                max_x = vertex->get_x();
            } else if (vertex->get_x() < min_x) {
                min_x = vertex->get_x();
            }

            if (vertex->get_y() > max_y) {
                max_y = vertex->get_y();
            } else if (vertex->get_y() < max_y) {
                min_y = vertex->get_y();
            }

            vertices.push_back(std::make_shared<IpeVertex>(IpeVertex(vertex->get_x(), vertex->get_y(), "2", color)));

            for (auto edge : vertex->get_edges()) {
                edges.push_back(std::make_shared<IpePath>(IpePath(std::vector<int>{vertex->get_x(), vertex->get_y(), edge.lock()->get_x(), edge.lock()->get_y()}, color)));
            }
        }
        this->bounding_box = {(double) min_x, (double) min_y, (double) max_x, (double) max_y};
    }

    std::string as_XML(std::string layer_name) override;
};


#endif //NEAR_DELAUNAY_IPEGRAPH_H
