#include "IpeCanvas.h"

bool IpeCanvas::save(std::string const file_path) {
    std::ofstream out_file;
    out_file.open(file_path, std::fstream::out);

    if (!out_file) {
        std::string output;
        output = "[ERROR] Unable to find or open file: ";
        output += file_path;
        output += "\n";

        fprintf(stderr,  "%s", output.c_str());
        throw std::invalid_argument(output.c_str());
    }

    out_file << this->as_XML();

    return true;
}

std::string IpeCanvas::as_XML() {
    std::string xml;
    // TODO: move some ipestyle to our (currently default) ipestyle (future)
    xml = "<?xml version=\"1.0\"?>\n"
           "<!DOCTYPE ipe SYSTEM \"ipe.dtd\">\n"
           "<ipe version=\"70212\" creator=\"Ipe 7.2.13\">\n"
           "<info created=\"D:20191121141228\" modified=\"D:20191121141228\"/>\n"
           "<ipestyle name=\"basic\">\n"
           "<symbol name=\"mark/disk(sx)\" transformations=\"translations\">\n"
           "<path fill=\"sym-stroke\">\n"
           "0.6 0 0 0.6 0 0 e\n"
           "</path>\n"
           "</symbol>\n"
           "<symbolsize name=\"tiny\" value=\"1.1\"/>\n"
           "</ipestyle>\n";

    for (IpeStyle& ipe_style : this->ipe_styles) {
        // TODO: indent?
        // TODO: warning. Assumes a single ipe style for the entire file to make the boundingbox work
        xml += ipe_style.as_XML(calculate_bounding_box());
    }

    xml += "<page>\n";

    // Adding the layer names
    for (auto& it : this->layers) {
        xml += "<layer name=\"" + it.first + "\"/>\n";
    }

    // Setting the view layers
    xml += "<view layers=\"";
    for (auto& it : this->layers) {
        if (it.second->get_active()) {
            xml += it.second->get_name() + " ";
        }
    }
    xml += "\"/>";

    // Adding the layer contents
    for (auto& it : this->layers) {
        xml += it.second->as_XML();
    }

    xml += "</page>\n"
           "</ipe>\n";
    return xml;
}

std::shared_ptr<IpeLayer> IpeCanvas::create_layer(std::string name, bool view) {
    std::shared_ptr<IpeLayer> layer = std::make_shared<IpeLayer>(IpeLayer(name, view));
    this->layers.emplace_back(name, layer);
    return layer;
}

std::vector<double> IpeCanvas::calculate_bounding_box() {
    std::vector<double> bounding_box = {-1, -1, -1, -1};

    // Adding the layer contents
    for (auto& it : this->layers) {
        std::vector<double> layer_bounding_box = it.second->get_bounding_box();
        bounding_box[0] = std::max(std::min(bounding_box[0], layer_bounding_box[0]), 0.0);
        bounding_box[1] = std::max(std::min(bounding_box[1], layer_bounding_box[1]), 0.0);
        bounding_box[2] = std::max(0.0, std::max(bounding_box[2], layer_bounding_box[2]));
        bounding_box[3] = std::max(0.0, std::max(bounding_box[3], layer_bounding_box[3]));
    }

    return bounding_box;
}