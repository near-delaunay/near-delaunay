#ifndef NEAR_DELAUNAY_IPECANVAS_H
#define NEAR_DELAUNAY_IPECANVAS_H

#include <string>
#include <vector>
#include <unordered_map>
#include <stdexcept>
#include <fstream>
#include "IpeStyle.h"
#include "IpeLayer.h"

class IpeCanvas {
private:
    std::vector<IpeStyle> ipe_styles = {IpeStyle()}; // TODO: future
    std::vector<std::pair<std::string, std::shared_ptr<IpeLayer>>> layers;

    /**
     * Return a string containing the contents of the current object in XML in a IPE readable format.
     *
     * @return A string containing the XML.
     */
    std::string as_XML();

public:
    IpeCanvas() = default;

    /**
     * Create new IpeLayer on the canvas. A reference to the layer object will then be returned.
     * @param name  Name of the layer to be created.
     * @param view  Whether the layer should be displayed on creation
     *              (alternatively it can be toggled on/off in IPE after opening the file).
     * @return      A reference to the new IpeLayer object.
     */
    std::shared_ptr<IpeLayer> create_layer(std::string name, bool view);

    /**
     * Calculates the maximal bounding box that surrounds all the bounding boxes contained in this layer.
     *
     * @return  A vector<double> with four entries being the min_x, min_y, max_x and max_y of the bounding box.
     *          (Values are currently assumed to be non-negative.)
     */
    std::vector<double> calculate_bounding_box();

    /**
     * Save current canvas to an xml file.
     * @param file_path The path to which the file should be saved.
     * @return          Whether saving succeeded.
     */
    bool save(std::string file_path);
};


#endif //NEAR_DELAUNAY_IPECANVAS_H
