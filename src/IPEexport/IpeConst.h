#ifndef NEAR_DELAUNAY_IPECONST_H
#define NEAR_DELAUNAY_IPECONST_H

/**
 * Class constaining the static constant used for drawing to Ipe.
 */
class IpeConst {
public:
    // Offset in the x-direction when drawing to the canvas.
    const static int x_offset = 0;

    // Offset in the y-direction when drawing to the canvas.
    const static int y_offset = 0;
};
#endif //NEAR_DELAUNAY_IPECONST_H
