#include "IpeEllipse.h"

IpeEllipse::IpeEllipse(
            double center_x, double center_y, double axis1_x, double axis1_y, double axis2_x, double axis2_y,
            std::string color) :
                IpeDrawable(color),
                center_x(center_x), center_y(center_y), axis1_x(axis1_x),
                axis1_y(axis1_y), axis2_x(axis2_x), axis2_y(axis2_y) {
    this->update_bounding_box();
};


void IpeEllipse::update_bounding_box() {
    double max_x = std::max(std::abs(this->axis1_x), std::abs(this->axis2_x));
    double max_y = std::max(std::abs(this->axis1_y), std::abs(this->axis2_y));

    this->bounding_box = {center_x - max_x, center_y - max_y, center_x + max_x, center_y + max_y};
}

std::string IpeEllipse::as_XML(std::string layer_name) {
    std::string xml;

    xml += "<path ";
    xml += "layer=\"" + layer_name + "\" ";
    xml += "stroke=\"" + this->color + "\">";
    xml += std::to_string(axis1_x) + " " + std::to_string(axis1_y)
                + " " + std::to_string(axis2_x) + " " + std::to_string(axis2_y)
                + " " + std::to_string(center_x) + " " + std::to_string(center_y);
    xml += " e </path>\n";

    return xml;
}