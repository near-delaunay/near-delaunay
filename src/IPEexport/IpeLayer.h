#ifndef NEAR_DELAUNAY_IPELAYER_H
#define NEAR_DELAUNAY_IPELAYER_H

#include <string>
#include <vector>
#include <memory>
#include "IpePath.h"
#include "IpeDrawable.h"
#include "IpeVertex.h"

/**
 * A layer on an IpeCanvas on which IpeDrawable objects can be drawn.
 */
class IpeLayer {
private:
    std::string name;
    std::vector<std::shared_ptr<IpeDrawable>> drawables;
    bool active;
    std::vector<double> bounding_box = {-1, -1, -1, -1};

public:
    IpeLayer(std::string name, bool active)
            : name(name), active(active) {}

    void add_drawable(std::shared_ptr<IpeDrawable> drawable);

    std::string as_XML();

    // Getters
    std::string get_name() {
        return this->name;
    }

    bool get_active() {
        return this->active;
    }

    std::vector<double> get_bounding_box() {
        return this->bounding_box;
    }
};

#endif //NEAR_DELAUNAY_IPELAYER_H