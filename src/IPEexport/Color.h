#ifndef NEAR_DELAUNAY_COLOR_H
#define NEAR_DELAUNAY_COLOR_H

#include <string>

/**
 * Class to define colors which can then later be used in Drawable objects.
 * It is important that the name used for the color corresponds to the string name specified in the drawables.
 * TODO: some sort of error checking might be nice here. Not sure how IPE handles unspecified colors.
 */
class Color {
private:
    std::string name;

    /**
     * RGB and alpha value of the current color.
     */
    float r_value, g_value, b_value, a_value;

public:
    Color(std::string name, float r, float g, float b)
            : Color(name, r, g, b, 1) {}

    Color(std::string name, float r, float g, float b, float a)
            : name(name), r_value(r), g_value(g), b_value(b), a_value(a) {}

    /**
    * Return a string containing the contents of the current object in XML in a IPE readable format.
    *
    * @return A string containing the XML.
    */
    std::string as_XML();
};


#endif //NEAR_DELAUNAY_COLOR_H
