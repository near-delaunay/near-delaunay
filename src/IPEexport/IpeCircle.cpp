#include "IpeCircle.h"

IpeCircle::IpeCircle(const double ax, const double ay, const double bx, const double by, const double cx,
                     const double cy, std::string color) :
                        IpeEllipse(std::move(color)) {
    std::pair<double, double> circumcenter = this->center(ax, ay, bx, by, cx, cy);
    this->set_center(circumcenter.first, circumcenter.second);

    double radius = std::sqrt((circumcenter.first - ax) * (circumcenter.first - ax)
                + (circumcenter.second - ay) * (circumcenter.second - ay));

    this->set_axis1(radius, 0);
    this->set_axis2(0, radius);

    this->update_bounding_box();
}

std::pair<double, double>
IpeCircle::center(double ax, double ay, double bx, double by, double cx, double cy) {
    const double dx = bx - ax;
    const double dy = by - ay;
    const double ex = cx - ax;
    const double ey = cy - ay;

    const double bl = dx * dx + dy * dy;
    const double cl = ex * ex + ey * ey;
    const double d = dx * ey - dy * ex;

    const double x = ax + (ey * bl - dy * cl) * 0.5 / d;
    const double y = ay + (dx * cl - ex * bl) * 0.5 / d;

    return std::make_pair(x, y);
}
