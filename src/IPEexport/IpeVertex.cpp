#include "IpeVertex.h"

IpeVertex::IpeVertex(double x, double y, std::string size, std::string color)
        : IpeDrawable(color), x(x), y(y), size(size) {
    this->bounding_box = {x + IpeConst::x_offset, y + IpeConst::y_offset, x + IpeConst::x_offset, y + IpeConst::y_offset};
}

std::string IpeVertex::as_XML(std::string layer_name) {
    std::string xml;

    xml = "<use layer=\"" + layer_name + "\" ";
    xml += "name=\"mark/disk(sx)\" ";
    xml += "pos=\"" + std::to_string(this->x + IpeConst::x_offset) + " " + std::to_string(this->y + IpeConst::y_offset) + "\" ";
    xml += "size=\"" + this->size + "\" ";
    xml += "stroke=\"" + this->color + "\"/>\n";

    return xml;
}