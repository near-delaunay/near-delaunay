#ifndef NEAR_DELAUNAY_IPESTYLE_H
#define NEAR_DELAUNAY_IPESTYLE_H

#include <string>
#include <vector>

#include "Color.h"

/**
 * Style object which defines the colors and possibly bounding box used in the IPE drawing.
 */
class IpeStyle {
private:
    std::string name = "near_delauny";
    std::vector<Color> colors = {
            Color("red", 1, 0, 0),
            Color("green", 0, 1, 0),
            Color("blue", 0, 0, 1)
    };

public:
    /**
     * Return a string containing the contents of the style object in XML in a IPE readable format.
     *
     * @return A string containing the XML.
     */
    std::string as_XML();

    /**
     * Return a string containing the contents of the style object in XML in a IPE readable format.
     *
     * @param bounding_box  The bounding box of the background.
     *                      The vector should contain 4 doubles in form {minX, minY, maxX, maxY}.
     * @return              A string containing the XML including a bounding box.
     */
    std::string as_XML(std::vector<double> bounding_box);
};


#endif //NEAR_DELAUNAY_IPESTYLE_H
