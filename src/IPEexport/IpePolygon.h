#ifndef NEAR_DELAUNAY_IPEPOLYGON_H
#define NEAR_DELAUNAY_IPEPOLYGON_H

#include <utility>
#include "IpePath.h"

class IpePolygon : public IpePath {

public:
    IpePolygon(std::vector<double> coordinates, std::string color, std::string fill_color)
            : IpePath(std::move(coordinates), std::move(color), std::move(fill_color)) {};

    std::string as_XML(std::string layer_name) override;

};


#endif //NEAR_DELAUNAY_IPEPOLYGON_H
