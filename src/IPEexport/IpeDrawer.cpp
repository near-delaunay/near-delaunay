#include <GeometryHelper.h>
#include "IpeDrawer.h"

void IpeDrawer::add_graph(std::shared_ptr<Graph> graph, const std::string& seed, bool add_delaunay) {
    if (this->drawn_graphs.find(seed) != this->drawn_graphs.end()) {
        // graph already drawn
        return;
    }

    std::shared_ptr<IpeLayer> graph_layer = this->canvas.create_layer(seed, true);

    graph_layer->add_drawable(std::make_shared<IpeGraph>(IpeGraph(graph, "black")));

    if (add_delaunay) {
        graph = DelaunayCreator().make_triangulation(graph, "", ' ');

        std::shared_ptr<IpeLayer> delaunay_graph_layer = this->canvas.create_layer(seed + "Delaunay", false);

        delaunay_graph_layer->add_drawable(std::make_shared<IpeGraph>(IpeGraph(graph, "green")));
    }

    this->drawn_graphs.insert(seed);
}

void IpeDrawer::add_edge(std::vector<int> vertices) {
    std::shared_ptr<IpeLayer> edge_layer = canvas.create_layer("Edge:(" + std::to_string(vertices.at(0)) + "," + std::to_string(vertices.at(1)) + "),("+ std::to_string(vertices.at(2)) + "," + std::to_string(vertices.at(3)) + ")", false);

    edge_layer->add_drawable(std::make_shared<IpeVertex>(IpeVertex(vertices.at(0), vertices.at(1), "2", "red")));
    edge_layer->add_drawable(std::make_shared<IpeVertex>(IpeVertex(vertices.at(2), vertices.at(3), "2", "red")));

    edge_layer->add_drawable(std::make_shared<IpePath>(IpePath(std::vector<int>{vertices.at(0), vertices.at(1), vertices.at(2), vertices.at(3)}, "red")));
}

void IpeDrawer::add_csv_line(std::string seed, char csv_delimiter, char seed_delimiter) {
    std::vector<std::string> split_seed = StringParseHelper::split_string(seed, csv_delimiter);

    std::shared_ptr<PointGenerator> generator = IpeDrawer::get_point_generator(split_seed[0]);
    std::shared_ptr<Triangulation> triangulation = IpeDrawer::get_triangulation(split_seed[2]);

    std::shared_ptr<Graph> graph = generator->get_points(split_seed[1], seed_delimiter);
    graph = triangulation->make_triangulation(graph, split_seed[3], seed_delimiter);

    if (split_seed.size() > 4) {
        std::vector<std::string> left_coord = StringParseHelper::split_string(split_seed[4], ',');
        std::vector<std::string> right_coord = StringParseHelper::split_string(split_seed[5], ',');

        this->add_edge({std::stoi(left_coord[0]), std::stoi(left_coord[1]), std::stoi(right_coord[0]), std::stoi(right_coord[1])});
        this->add_edge_surroundings(graph, {std::stoi(left_coord[0]), std::stoi(left_coord[1])}, {std::stoi(right_coord[0]), std::stoi(right_coord[1])});
    }

    this->add_graph(graph, split_seed[0] + csv_delimiter + split_seed[1] + csv_delimiter + split_seed[2] + csv_delimiter + split_seed[3], true);
}

std::shared_ptr<PointGenerator> IpeDrawer::get_point_generator(const std::string& id) {
    std::shared_ptr<PointGenerator> generator;

    if (id == "UniformPointGenerator") {
        generator = std::make_shared<UniformPointGenerator>(UniformPointGenerator());
    } else if (id == "SimplestPointGenerator") {
        generator = std::make_shared<SimplestPointGenerator>(SimplestPointGenerator());
    }

    return generator;
}

std::shared_ptr<Triangulation> IpeDrawer::get_triangulation(const std::string& id) {
    std::shared_ptr<Triangulation> triangulation;

    if (id == "DelaunayTriangulation"){
        triangulation = std::make_shared<DelaunayCreator>(DelaunayCreator());
    } else if (id == "DelaunayTriangulationWithRepeatedFlipping") {
        triangulation = std::make_shared<DelaunayRepeatedFlipping>(DelaunayRepeatedFlipping());
    }

    return triangulation;
}

void IpeDrawer::add_edge_surroundings(std::shared_ptr<Graph> graph, std::pair<int, int> left_coord, std::pair<int, int> right_coord) {
    std::shared_ptr<DCEL> dcel = GraphParser::parse_graph_to_DCEL(graph);

    std::shared_ptr<IpeLayer> edge_layer = canvas.create_layer("Additional info:(" + std::to_string(left_coord.first) + "," + std::to_string(left_coord.second) + "),("+ std::to_string(right_coord.first) + "," + std::to_string(right_coord.second) + ")", false);
    std::shared_ptr<IpeLayer> additional_points = canvas.create_layer("Additional points:(" + std::to_string(left_coord.first) + "," + std::to_string(left_coord.second) + "),("+ std::to_string(right_coord.first) + "," + std::to_string(right_coord.second) + ")", false);
    std::shared_ptr<IpeLayer> algorithm_layer = canvas.create_layer("Algorithm info:(" + std::to_string(left_coord.first) + "," + std::to_string(left_coord.second) + "),("+ std::to_string(right_coord.first) + "," + std::to_string(right_coord.second) + ")", false);

    for (auto edge : dcel->get_edges()) {
        if (edge->get_target()->get_x() == left_coord.first && edge->get_target()->get_y() == left_coord.second &&
                edge->get_twin()->get_target()->get_x() == right_coord.first && edge->get_twin()->get_target()->get_y() == right_coord.second) {
            std::pair<int, int> p1 = {edge->get_target()->get_x(), edge->get_target()->get_y()};
            std::pair<int, int> p2 = {edge->get_twin()->get_target()->get_x(), edge->get_twin()->get_target()->get_y()};
            std::pair<int, int> p3 = {edge->get_next()->get_target()->get_x(), edge->get_next()->get_target()->get_y()};
            std::pair<int, int> p4 = {edge->get_twin()->get_next()->get_target()->get_x(), edge->get_twin()->get_next()->get_target()->get_y()};


            edge_layer->add_drawable(std::make_shared<IpeVertex>(p3.first, p3.second, "2", "blue"));
            edge_layer->add_drawable(std::make_shared<IpeVertex>(p4.first, p4.second, "2", "blue"));

            edge_layer->add_drawable(std::make_shared<IpePath>(IpePath(std::vector<int>{p1.first, p1.second, p3.first, p3.second}, "blue")));
            edge_layer->add_drawable(std::make_shared<IpePath>(IpePath(std::vector<int>{p1.first, p1.second, p4.first, p4.second}, "blue")));

            edge_layer->add_drawable(std::make_shared<IpePath>(IpePath(std::vector<int>{p2.first, p2.second, p3.first, p3.second}, "blue")));
            edge_layer->add_drawable(std::make_shared<IpePath>(IpePath(std::vector<int>{p2.first, p2.second, p4.first, p4.second}, "blue")));

            auto circum1 = GeometryHelper::circumcenter(p1, p2, p3);
            auto circum2 = GeometryHelper::circumcenter(p1, p2, p4);

            additional_points->add_drawable(std::make_shared<IpeVertex>(circum1.first, circum1.second, "2", "green"));
            additional_points->add_drawable(std::make_shared<IpeVertex>(circum2.first, circum2.second, "2", "green"));

            auto intersect1 = GeometryHelper::line_intersection(GeometryHelper::perpendicular_bisector(p1, p3), GeometryHelper::perpendicular_bisector(p1, p4));
            auto intersect2 = GeometryHelper::line_intersection(GeometryHelper::perpendicular_bisector(p2, p3), GeometryHelper::perpendicular_bisector(p2, p4));

            additional_points->add_drawable(std::make_shared<IpeVertex>(intersect1.first, intersect1.second, "2", "black"));
            additional_points->add_drawable(std::make_shared<IpeVertex>(intersect2.first, intersect2.second, "2", "black"));

            algorithm_layer->add_drawable(std::make_shared<IpePath>(IpePath(std::vector<double>{circum1.first, circum1.second, intersect1.first, intersect1.second, circum2.first, circum2.second, intersect2.first, intersect2.second}, "black", "black")));
        }
    }

}
