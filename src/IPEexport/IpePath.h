#ifndef NEAR_DELAUNAY_IPEPATH_H
#define NEAR_DELAUNAY_IPEPATH_H

#include <vector>
#include <string>
#include <iostream>
#include "IpeDrawable.h"

/**
 * Simple drawable object that represents a path on a layer. A path is defined by a vector of x and y coordinates.
 */
class IpePath : public IpeDrawable {
protected:
    /**
     * x and y coordinates of the characteristic points of the path.
     * Thus length has to be a multiple of 2.
     */
    std::vector<double> coordinates;

    std::string fill_color;

    std::string get_path_beginning(std::string layer_name);

public:
    IpePath(std::vector<double> coordinates, std::string color)
            : IpePath(std::move(coordinates), std::move(color), "none") {};

    IpePath(std::vector<int> coordinates, std::string color)
            : IpePath(std::move(std::vector<double> (coordinates.begin(), coordinates.end())), std::move(color), "none") {};


    IpePath(std::vector<double> coordinates, std::string color, std::string fill_color);

    IpePath(std::vector<int> coordinates, std::string color, std::string fill_color)
            : IpePath(std::move(std::vector<double> (coordinates.begin(), coordinates.end())), std::move(color), std::move(fill_color)) {};;

    std::string as_XML(std::string layer_name) override;
};


#endif //NEAR_DELAUNAY_IPEPATH_H
