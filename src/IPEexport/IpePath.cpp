#include "IpePath.h"
#include "IpeConst.h"
#include <utility>

IpePath::IpePath(std::vector<double> coordinates, std::string color, std::string fill_color)
        : IpeDrawable(std::move(color)), coordinates(coordinates), fill_color(std::move(fill_color)) {
    if (coordinates.size() % 2 != 0) {
        //TODO: should we throw an error or something?
        std::cout << "[WARNING] Path has odd number of doubles. Last coordinate is not complete, missing 'y'.\n";
    }

    this->bounding_box = {-1, -1, -1, -1};
    for (int i = 0; i < coordinates.size(); i+= 2) {
        this->bounding_box[0] = std::max(std::min(this->bounding_box[0], coordinates.at(i)), 0.0) + IpeConst::x_offset;
        this->bounding_box[1] = std::max(std::min(this->bounding_box[1], coordinates.at(i + 1)), 0.0) + IpeConst::y_offset;
        this->bounding_box[2] = std::max(0.0, std::max(this->bounding_box[2], coordinates.at(i))) + IpeConst::x_offset;
        this->bounding_box[3] = std::max(0.0, std::max(this->bounding_box[3], coordinates.at(i + 1))) + IpeConst::y_offset;
    }
}


std::string IpePath::as_XML(std::string layer_name) {
    std::string xml;

    xml = this->get_path_beginning(layer_name);

    xml += "</path>\n";

    return xml;
}

std::string IpePath::get_path_beginning(std::string layer_name) {
    std::string xml;

    xml += "<path ";
    xml += "layer=\"" + layer_name + "\"";
    if (this->color.compare("none") != 0) {
        xml += " stroke=\"" + this->color + "\"";
    }

    if (this->fill_color.compare("none") != 0) {
        xml += " fill=\"" + this->fill_color + "\"";
    }
    xml += ">";

    for (int i = 0; i < this->coordinates.size(); i += 2) {
        xml += std::to_string(this->coordinates[i] + IpeConst::x_offset) + " " + std::to_string(this->coordinates[i + 1] + IpeConst::y_offset);
        if (i == 0) {
            xml += " m\n"; // start of path
        } else {
            xml += " l\n"; // subsequent points
        }
    }

    return xml;
}
