#ifndef NEAR_DELAUNAY_IPEDRAWER_H
#define NEAR_DELAUNAY_IPEDRAWER_H


#include <unordered_set>
#include <sys/stat.h>
#include "IpeCanvas.h"
#include <graph/Graph.h>
#include <StringParseHelper.h>
#include <algorithm>
#include <dcel/DCEL.h>
#include <GraphParser.h>
#include "../triangulations/DelaunayCreator.h"
#include "../triangulations/DelaunayRepeatedFlipping.h"
#include "../point_generators/UniformPointGenerator.h"
#include "../point_generators/SimplestPointGenerator.h"
#include "IpeGraph.h"

/**
 * Graph and edge drawer to simplify the process of visualizing triangulations.
 *
 */
class IpeDrawer {
private:
    // Canvas to which is being drawn.
    IpeCanvas canvas;

    // A hashset of all the graphs that have already been drawn to ensure that they are only drawn once.
    std::unordered_set<std::string> drawn_graphs;

    static std::shared_ptr<PointGenerator> get_point_generator(const std::string& id);
    static std::shared_ptr<Triangulation> get_triangulation(const std::string& id);



public:
    IpeDrawer() {
        canvas = IpeCanvas();
    }

    /**
     * Add a graph to the IPE file.
     *
     * @param graph         The graph to be drawn.
     * @param seed          The seed of the graph to be drawn (used for layer name).
     * @param add_delaunay  Whether the same point set should be used to also draw a Delaunay graph.
     */
    void add_graph(std::shared_ptr<Graph> graph, const std::string& seed, bool add_delaunay);

    /**
     * Add a single edge to the IPE file.
     *
     * @param vertices Four ints defining the coordinates of the end points of the edge in form {x_1, y_1, x_2, y_2}.
     */
    void add_edge(std::vector<int> vertices);

    /**
     * Add an entire line of the csv to the IPE file (so the graph and the corresponding edge).
     *
     * @param seed              The seed identifying the csv line.
     * @param csv_delimiter     The delimiter of csv cells.
     * @param seed_delimiter    The delimiter of seeds for PointGenerators and TriangulationCreators.
     */
    void add_csv_line(std::string seed, char csv_delimiter, char seed_delimiter);

    /**
     * Add additional information to an edge.
     *
     * @param graph         The graph for which the additional edge info should be drawn.
     * @param left_coord    The coordinates of on of the end points of the edge.
     * @param right_coord   The coordinates of the second end point of the edge.
     */
    void add_edge_surroundings(std::shared_ptr<Graph> graph, std::pair<int, int> left_coord, std::pair<int, int> right_coord);

    /**
     * Save the IPE file. This is done in the output folder.
     *
     * @param name Name that the file should be saved to.
     */
    void save(std::string name) {
        mkdir("../../output", 0777);
        canvas.save("../../output/" + name);
    }
};


#endif //NEAR_DELAUNAY_IPEDRAWER_H
