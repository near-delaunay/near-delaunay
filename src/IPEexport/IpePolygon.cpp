#include "IpePolygon.h"

std::string IpePolygon::as_XML(std::string layer_name) {
    std::string xml;

    xml = this->get_path_beginning(layer_name);

    xml += "h";

    xml += "</path>\n";

    return xml;
}