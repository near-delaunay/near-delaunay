#ifndef NEAR_DELAUNAY_IPEDRAWABLE_H
#define NEAR_DELAUNAY_IPEDRAWABLE_H

#include <string>
#include <utility>
#include <iostream>
#include <vector>

/**
 * General drawable class that specifies abstract function for getting xml.
 * This class is used in the layer for adding new objects.
 */
class IpeDrawable {
protected:
    /**
     * Bounding Box of the IpeDrawable object. Should be correctly set on startup.
     */
    std::vector<double> bounding_box = {};

    /**
     * The color of the drawable object.
     */
    std::string color;

public:
    IpeDrawable(std::string color) : color(color) {
    };

    /**
     * Return a string containing the contents of the current object in XML in a IPE readable format.
     *
     * @return A string containing the XML.
     */
    virtual std::string as_XML(std::string layer_name) = 0;

    /**
     * Return a vector of four doubles containing the bounding box of the current drawable.
     *
     * @return      The bounding box of the object.
     *              The vector should contain 4 doubles in form {min_x, min_y, max_x, max_y}.
     */
    std::vector<double> get_bounding_box() {
        if (bounding_box.size() != 4) {
            std::cout << "[Warning] The bounding_box of the current object has not been set correctly. \n";
        }

        return bounding_box;
    };
};

#endif //NEAR_DELAUNAY_IPEDRAWABLE_H
