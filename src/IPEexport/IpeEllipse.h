#ifndef NEAR_DELAUNAY_IPEELLIPSE_H
#define NEAR_DELAUNAY_IPEELLIPSE_H

#include "IpeDrawable.h"

/**
 * Drawable object that represents an ellipse on a layer.
 * An ellipse is defined by a center point and then two vectors from that point which define the outside.
 */
class IpeEllipse : public IpeDrawable {
private:
    double center_x{}, center_y{};
    double axis1_x{}, axis1_y{};
    double axis2_x{}, axis2_y{};

protected:
    void set_center(double x, double y) {
        this->center_x = x;
        this->center_y = y;
    }

    void set_axis1(double x, double y) {
        this->axis1_x = x;
        this->axis1_y = y;
    }

    void set_axis2(double x, double y) {
        this->axis2_x = x;
        this->axis2_y = y;
    }

    void update_bounding_box();

public:
    explicit IpeEllipse(std::string color) : IpeDrawable(color) {};

    IpeEllipse(double center_x, double center_y, double axis1_x, double axis1_y, double axis2_x, double axis2_y,
            std::string color);

    std::string as_XML(std::string layer_name) override;
};

#endif //NEAR_DELAUNAY_IPEELLIPSE_H
