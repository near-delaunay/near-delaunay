#include "IpeLayer.h"

void IpeLayer::add_drawable(std::shared_ptr<IpeDrawable> drawable) {
    this->drawables.emplace_back(drawable);

    // update bounding_box of current layer
    std::vector<double> drawableBox = drawable->get_bounding_box();
    if (drawableBox.size() == 4) {
        bounding_box[0] = std::max(std::min(bounding_box[0], drawableBox[0]), 0.0);
        bounding_box[1] = std::max(std::min(bounding_box[1], drawableBox[1]), 0.0);
        bounding_box[2] = std::max(0.0, std::max(bounding_box[2], drawableBox[2]));
        bounding_box[3] = std::max(0.0, std::max(bounding_box[3], drawableBox[3]));
    }
}

std::string IpeLayer::as_XML() {
    std::string xml;

    for (const std::shared_ptr<IpeDrawable>& p : this->drawables) {
        xml += p->as_XML(this->name);
    }

    return xml;
}