#include "IpeGraph.h"

std::string IpeGraph::as_XML(std::string layer_name) {
    std::string xml;

    for (auto vertex : this->vertices) {
        xml += vertex->as_XML(layer_name);
    }

    for (auto edge : this->edges) {
        xml += edge->as_XML(layer_name);
    }
    return xml;
}
