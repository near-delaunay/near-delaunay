#include "IpeStyle.h"

std::string IpeStyle::as_XML() {
    std::string xml;
    xml += "<ipestyle name=\"" + this->name + "\">\n";

    for (Color& c : this->colors) {
        xml += c.as_XML();
    }

    xml += "</ipestyle>\n";
    return xml;
}

std::string IpeStyle::as_XML(std::vector<double> bounding_box) {
    // TODO: how to deal with elements drawn in negative coordinates? Seems bounding_box has to be strictly positive.
    std::string xml;
    xml += "<ipestyle name=\"" + this->name + "\">\n";

    for (Color& c : this->colors) {
        xml += c.as_XML();
    }

    // apply the bounding box
    int padding = 10;
    xml += "<layout paper=\"";
    xml += std::to_string(bounding_box[2] + 2 * padding) + " " + std::to_string(bounding_box[3] + 2 * padding) + "\" ";
    xml += "origin=\"";
    xml += std::to_string(padding) + " " + std::to_string(padding) + "\" ";
    xml += "frame=\"";
    xml += std::to_string(bounding_box[2]) + " " + std::to_string(bounding_box[3]) + "\"/>\n";
    xml += "</ipestyle>\n";

    return xml;
}