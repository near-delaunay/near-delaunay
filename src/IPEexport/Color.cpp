#include "Color.h"

std::string Color::as_XML() {
    std::string xml;
    // TODO: alpha not supported on objects it seems, only on layers
    xml = "<color name=\"" + this->name + "\" value=\"" + std::to_string(this->r_value) + " "
                + std::to_string(this->g_value) + " " + std::to_string(this->b_value) + " "
                + std::to_string(this->a_value) + "\"/>\n";
    return xml;
}