#ifndef NEAR_DELAUNAY_IPEVERTEX_H
#define NEAR_DELAUNAY_IPEVERTEX_H

#include <string>
#include "IpeDrawable.h"
#include "IpeConst.h"

/**
 * Simple drawable object that represents a single vertex on a layer.
 */
class IpeVertex : public IpeDrawable {
private:
    double x, y;
    std::string size;

public:
    IpeVertex(double x, double y, std::string size, std::string color);

    std::string as_XML(std::string layer_name) override;
};


#endif //NEAR_DELAUNAY_IPEVERTEX_H
