#include "UniformPointGenerator.h"
#include <StringParseHelper.h>

#include <random>
#include <unordered_set>

std::shared_ptr<Graph> UniformPointGenerator::get_points(std::string seed, char delimiter) {
    std::shared_ptr<Graph> graph = std::make_shared<Graph>();

    std::vector<std::string> split_seed = StringParseHelper::split_string(seed, delimiter);

    int n = std::stoi(split_seed[0]);

    int min_x = std::stoi(split_seed[1]);
    int max_x = std::stoi(split_seed[2]);
    int min_y = std::stoi(split_seed[3]);
    int max_y = std::stoi(split_seed[4]);

    std::seed_seq random_seed(split_seed[5].begin(), split_seed[5].end());
    std::mt19937 generator(random_seed);

    std::uniform_int_distribution<int> distribution_x(min_x, max_x);
    std::uniform_int_distribution<int> distribution_y(min_y, max_y);

    std::unordered_set<std::pair<double, double>, pair_hash> vertices;

    while (vertices.size() < n) {
        std::pair<double, double> coordinates = std::make_pair(distribution_x(generator), distribution_y(generator));
        vertices.insert(coordinates);
    }

    for (auto& vertex : vertices) {
        graph->add_vertex(std::make_shared<GraphVertex>(GraphVertex(vertex.first, vertex.second)));
    }

    return graph;
}
