#include "SimplestPointGenerator.h"

std::shared_ptr<Graph> SimplestPointGenerator::get_points(std::string seed, char delimiter) {
    std::shared_ptr<Graph> graph = std::make_shared<Graph>();

    graph->add_vertex(std::make_shared<GraphVertex>(GraphVertex(0, 0)));
    graph->add_vertex(std::make_shared<GraphVertex>(GraphVertex(10, 0)));
    graph->add_vertex(std::make_shared<GraphVertex>(GraphVertex(-5, 10)));
    graph->add_vertex(std::make_shared<GraphVertex>(GraphVertex(15, 10)));
    graph->add_vertex(std::make_shared<GraphVertex>(GraphVertex(5, 20)));

    return graph;
}
