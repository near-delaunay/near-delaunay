#ifndef NEAR_DELAUNAY_SIMPLESTPOINTGENERATOR_H
#define NEAR_DELAUNAY_SIMPLESTPOINTGENERATOR_H


#include "PointGenerator.h"

class SimplestPointGenerator : public PointGenerator {
public:
    explicit SimplestPointGenerator(std::string name) : PointGenerator(std::move(name)) {};

    SimplestPointGenerator() : SimplestPointGenerator("SimplestPointGenerator") {};

    std::shared_ptr<Graph> get_points(std::string seed, char delimiter) override;
};


#endif //NEAR_DELAUNAY_SIMPLESTPOINTGENERATOR_H
