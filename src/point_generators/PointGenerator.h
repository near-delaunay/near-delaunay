#ifndef NEAR_DELAUNAY_POINTGENERATOR_H
#define NEAR_DELAUNAY_POINTGENERATOR_H

#include <graph/Graph.h>

class PointGenerator {
private:
    std::string name;

public:
    explicit PointGenerator(std::string name) : name(std::move(name)) {};

    virtual std::shared_ptr<Graph> get_points(std::string seed, char delimiter) = 0;

    std::string get_name() {
        return name;
    }
};


#endif //NEAR_DELAUNAY_POINTGENERATOR_H
