#ifndef NEAR_DELAUNAY_UNIFORMPOINTGENERATOR_H
#define NEAR_DELAUNAY_UNIFORMPOINTGENERATOR_H


#include "PointGenerator.h"

class UniformPointGenerator : public PointGenerator {
public:
    explicit UniformPointGenerator(std::string name) : PointGenerator(std::move(name)) {};

    UniformPointGenerator() : UniformPointGenerator("UniformPointGenerator") {};

    /**
     * Returns randomly generated points based on a provided seed.
     * @param seed      Seed to be used for generating the points. It contains the following elements, separated by the delimiter:
     *                  -   number of points to be generated
     *                  -   minimum x-value
     *                  -   maximum x-value
     *                  -   minimum y-value
     *                  -   maximum y-value
     *                  -   seed for random generator
     * @param delimiter Delimiter used in the seed.
     * @return          Graph containing the randomly generated points.
     */
    std::shared_ptr<Graph> get_points(std::string seed, char delimiter) override;
};


#endif //NEAR_DELAUNAY_UNIFORMPOINTGENERATOR_H
