#include <fstream>
#include "Pipeline.h"
#include <GraphParser.h>
#include <iostream>

const std::string Pipeline::delimiter = ";";

void Pipeline::run_algorithms_for_DCEL(std::shared_ptr<DCEL> dcel, const std::vector<std::shared_ptr<EdgeBasedAlgorithm>>& algorithms,
                                       std::ofstream& output_file, const std::string& generator_name,
                                       const std::string& generator_seed, const std::string& triangulation_name,
                                       const std::string& triangulation_seed) {
    std::vector<std::unordered_map<std::shared_ptr<Halfedge>, double>> results;

    for (const std::shared_ptr<EdgeBasedAlgorithm>& algorithm : algorithms) {
        std::cout << "-------- Calculating algorithm: " << algorithm->get_name() << std::endl;
        results.push_back(algorithm->calculate_metric(dcel));
    }

    std::cout << "-------- Writing edge based results to file" << std::endl;
    for (const std::shared_ptr<Halfedge>& edge : dcel->get_edges()) {

        output_file << generator_name << delimiter << generator_seed << delimiter
                    << triangulation_name << delimiter << triangulation_seed << delimiter;

        output_file << edge->get_target()->get_string() << delimiter << edge->get_twin()->get_target()->get_string();

        for (std::unordered_map<std::shared_ptr<Halfedge>, double> result : results) {
            if (result.find(edge) != result.end()) {
                output_file << delimiter << result[edge];
            } else {
                output_file << delimiter << "N.A.";
            }
        }

        output_file << "\n";
    }
}

void Pipeline::execute(const std::string& output_file_name, char seed_delimiter) {

    std::cout << "Executing pipeline" << std::endl;

    std::cout << "Opening file with name: " << output_file_name << std::endl;
    std::ofstream output_file;
    output_file.open(output_file_name);

    // create header of file
    output_file << "DCEL generator" << delimiter << "DCEL generation seed" << delimiter << "Triangulation generator"
                << delimiter << "Triangulation seed" << delimiter << "Edge source" << delimiter << "Edge target";

    for (const std::shared_ptr<EdgeBasedAlgorithm>& algorithm : Pipeline::algorithms) {
        output_file << delimiter << algorithm->get_name();
    }

    output_file << "\n";


    for (const std::pair<std::shared_ptr<Triangulation>, std::vector<std::string>>& triangulation : triangulations) {

        std::cout << "Handling triangulation: " << triangulation.first->get_name() << std::endl;
        std::vector<std::string> triangulation_seeds = triangulation.second;
        if (triangulation_seeds.empty()) {
            triangulation_seeds.emplace_back("N.A.");
        }
        for (const std::string& triangulation_seed : triangulation_seeds) {
            std:: cout << "-- Handling triangulation seed: " << triangulation_seed << std::endl;
            for (const std::pair<std::shared_ptr<PointGenerator>, std::vector<std::string>>& generator : generators) {
                std::cout << "---- Handling generator: " << generator.first->get_name() << std::endl;
                std::vector<std::string> generator_seeds = generator.second;
                if (generator_seeds.empty()) {
                    generator_seeds.emplace_back("N.A.");
                }
                for (const std::string& generator_seed : generator_seeds) {
                    std::cout << "------ Handling generator seed: " << generator_seed << std::endl;
                    std::shared_ptr<Graph> graph = generator.first->get_points(generator_seed, seed_delimiter);

                    graph = triangulation.first->make_triangulation(graph, triangulation_seed, seed_delimiter);

                    std::shared_ptr<DCEL> dcel = GraphParser::parse_graph_to_DCEL(graph);

                    run_algorithms_for_DCEL(dcel, algorithms, output_file, generator.first->get_name(), generator_seed,
                                            triangulation.first->get_name(), triangulation_seed);
                }
            }
        }
    }

    output_file.close();
}
