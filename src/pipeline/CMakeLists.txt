# Collect sources
set(pipeline_sources
        Pipeline.cpp
        )

# Collect headers
set(pipeline_headers
        Pipeline.h
        )

# Define libary target
add_library(pipeline ${pipeline_sources} ${pipeline_headers})

# Link libraries
target_link_libraries(pipeline PRIVATE algorithms)
target_link_libraries(pipeline PRIVATE point_generators)
target_link_libraries(pipeline PRIVATE triangulations)
target_link_libraries(pipeline PRIVATE datastructures)

# Include directories
target_include_directories(pipeline PUBLIC ${PROJECT_SOURCE_DIR}/src/pipeline)