#ifndef NEAR_DELAUNAY_PIPELINE_H
#define NEAR_DELAUNAY_PIPELINE_H

#include <EdgeBasedAlgorithm.h>
#include <Triangulation.h>
#include <PointGenerator.h>

#include <string>
#include <memory>
#include <utility>
#include <iostream>

class Pipeline {
private:
    const static std::string delimiter;

    static void run_algorithms_for_DCEL(std::shared_ptr<DCEL> dcel, const std::vector<std::shared_ptr<EdgeBasedAlgorithm>>& algorithms,
                                 std::ofstream& output_file, const std::string& generator_name, const std::string& generator_seed,
                                 const std::string& triangulation_name, const std::string& triangulation_seed);

    std::vector<std::pair<std::shared_ptr<PointGenerator>, std::vector<std::string>>> generators;
    std::vector<std::pair<std::shared_ptr<Triangulation>, std::vector<std::string>>> triangulations;
    std::vector<std::shared_ptr<EdgeBasedAlgorithm>> algorithms;

public:
    void execute(const std::string& output_file_name, char seed_delimiter);

    void register_algorithm(const std::shared_ptr<EdgeBasedAlgorithm>& algorithm) {
        Pipeline::algorithms.push_back(algorithm);
        std::cout << "Registered algorithm: " << algorithm->get_name() << std::endl;
    }

    void register_generator(const std::shared_ptr<PointGenerator>& generator, std::vector<std::string> seeds) {
        if (seeds.empty()) {
            seeds = {"N.A."};
        }

        Pipeline::generators.emplace_back(generator, seeds);
        std::cout << "Registered generator: " << generator->get_name() << std::endl;
    }

    void register_generator(const std::shared_ptr<PointGenerator>& generator) {
        Pipeline::register_generator(generator, {});
        std::cout << "Registered generator: " << generator->get_name() << std::endl;
    }

    void register_triangulation(const std::shared_ptr<Triangulation>& triangulation, std::vector<std::string> seeds) {
        if (seeds.empty()) {
            seeds = {"N.A."};
        }

        Pipeline::triangulations.emplace_back(triangulation, seeds);
        std::cout << "Registered triangulation: " << triangulation->get_name() << std::endl;
    }

    void register_triangulation(const std::shared_ptr<Triangulation>& triangulation) {
        Pipeline::register_triangulation(triangulation, {});
        std::cout << "Registered triangulation: " << triangulation->get_name() << std::endl;
    }

};

#endif //NEAR_DELAUNAY_PIPELINE_H
