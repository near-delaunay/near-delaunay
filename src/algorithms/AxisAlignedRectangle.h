#ifndef NEAR_DELAUNAY_AXISALIGNEDRECTANGLE_H
#define NEAR_DELAUNAY_AXISALIGNEDRECTANGLE_H


#include <dcel/DCEL.h>
#include <range_tree/RangeNode.h>
#include <range_tree/RangeBuilder.h>
#include "EdgeBasedAlgorithm.h"

/**
 * Implementation of the axis aligned rectangle metric making use of a 2-d range tree.
 */
class AxisAlignedRectangle : public EdgeBasedAlgorithm {
private:
    // 2D range tree which first sorts by y-value and then by x-value.
    std::shared_ptr<RangeNode<DCELVertex>> y_x_tree;

    // 2D range tree which first sorts by x-value and then by y-value.
    std::shared_ptr<RangeNode<DCELVertex>> x_y_tree;

protected:
    double calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) override;

    void precomputations(std::shared_ptr<DCEL> vertex) override;

public:
    AxisAlignedRectangle() : AxisAlignedRectangle("Axis-aligned Rectangles") {};
    explicit AxisAlignedRectangle(std::string name) : EdgeBasedAlgorithm(std::move(name)) {};
};


#endif //NEAR_DELAUNAY_AXISALIGNEDRECTANGLE_H
