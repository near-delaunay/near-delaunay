#ifndef NEAR_DELAUNAY_OPPOSINGANGLES_H
#define NEAR_DELAUNAY_OPPOSINGANGLES_H


#include <dcel/Halfedge.h>
#include <dcel/DCEL.h>
#include "EdgeBasedAlgorithm.h"

#include <unordered_map>
#include <utility>

class OpposingAngles : public EdgeBasedAlgorithm {
protected:
    double calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) override;

public:
    OpposingAngles() : OpposingAngles("OpposingAngles") {};
    explicit OpposingAngles(std::string name) : EdgeBasedAlgorithm(std::move(name)) {};
};


#endif //NEAR_DELAUNAY_OPPOSINGANGLES_H
