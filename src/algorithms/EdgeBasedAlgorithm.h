#ifndef NEAR_DELAUNAY_EDGEBASEDALGORITHM_H
#define NEAR_DELAUNAY_EDGEBASEDALGORITHM_H

#include <unordered_map>
#include "../datastructures/dcel/DCEL.h"

class EdgeBasedAlgorithm {
private:
    std::string name;

protected:
    /**
     * Function that can be overridden if precomputations are necessary.
     * @param dcel
     */
    virtual void precomputations(std::shared_ptr<DCEL> dcel) {};

public:
    EdgeBasedAlgorithm(std::string name) : name(name) {};

    std::unordered_map<std::shared_ptr<Halfedge>, double> calculate_metric(std::shared_ptr<DCEL> dcel) {
        this->precomputations(dcel);

        std::unordered_map<std::shared_ptr<Halfedge>, double> result;
        std::shared_ptr<Face> outer = dcel->get_outer_face();

        for (std::shared_ptr<Halfedge> edge : dcel->get_edges()) {

            // only calculate each score once.
            if (edge->get_target() > edge->get_twin()->get_target()) {
                continue;
            }

            // score can not be calculated for outer edges.
            if (edge->get_face() == outer || edge->get_twin()->get_face() == outer) {
                continue;
            }

            double score = this->calculate_edge(edge, dcel);
            result[edge] = score;
            result[edge->get_twin()] = score;
        }

        return result;
    }

    /**
     * Function that has to be overridden, which returns the score of the edge for the specific metric. By default this
     * is called for each inner edge once (only on the halfedges where the source is bigger than the target). If it
     * should be called on all half edges, the calculate_metric function has to be overridden.
     *
     * @param edge  The Halfedge for which the score should be caclulated.
     * @param dcel  The DCEL in question.
     * @return      The score of the edge.
     */
    virtual double calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) = 0;

    std::string get_name() {
        return name;
    }
};

#endif //NEAR_DELAUNAY_EDGEBASEDALGORITHM_H
