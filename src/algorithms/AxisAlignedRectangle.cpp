#include "AxisAlignedRectangle.h"

void AxisAlignedRectangle::precomputations(std::shared_ptr<DCEL> dcel) {
    std::vector<std::shared_ptr<DCELVertex>> vertices;
    for (const auto& vertex : dcel->get_vertices()) {
        vertices.push_back(vertex);
    }

    x_y_tree = RangeBuilder<DCELVertex>::build_tree(vertices, {
            [] (const std::shared_ptr<DCELVertex> vertex) -> double {
                return vertex->get_x();
            },
            [] (const std::shared_ptr<DCELVertex> vertex) -> double {
                return vertex->get_y();
            }
    });

    y_x_tree = RangeBuilder<DCELVertex>::build_tree(vertices, {
            [] (const std::shared_ptr<DCELVertex> vertex) -> double {
                return vertex->get_y();
            },
            [] (const std::shared_ptr<DCELVertex> vertex) -> double {
                return vertex->get_x();
            }
    });
}

double AxisAlignedRectangle::calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) {
    double x_dist = std::abs(edge->get_target()->get_x() - edge->get_twin()->get_target()->get_x());
    double y_dist = std::abs(edge->get_target()->get_y() - edge->get_twin()->get_target()->get_y());

    if (x_dist > y_dist) {
        std::pair<double, double> x_range;

        if (edge->get_target()->get_x() < edge->get_twin()->get_target()->get_x()) {
            x_range = {edge->get_target()->get_x(), edge->get_twin()->get_target()->get_x()};
        } else {
            x_range = {edge->get_twin()->get_target()->get_x(), edge->get_target()->get_x()};
        }

        double min_y = std::min(edge->get_target()->get_y(), edge->get_twin()->get_target()->get_y());
        std::pair<double, double> y_range = {min_y, min_y + x_dist};

        auto upper_bound = x_y_tree->find_extremum({x_range, y_range}, true);

        if (!upper_bound) {
            return 1.0;
        }

        if (upper_bound->get_associated_vertex()->get_y() < std::max(edge->get_target()->get_y(), edge->get_twin()->get_target()->get_y())) {
            // no rectangle possible.
            return -1.0;
        }

        double max_y = upper_bound->get_associated_vertex()->get_y();
        y_range = {max_y - x_dist, max_y};
        auto lower_bound = x_y_tree->find_extremum({x_range, y_range}, false);

        if (!lower_bound) {
            return 1.0;
        }

        return (upper_bound->get_associated_vertex()->get_y() - lower_bound->get_associated_vertex()->get_y()) / x_dist;
    } else {
        std::pair<double, double> y_range;

        if (edge->get_target()->get_y() < edge->get_twin()->get_target()->get_y()) {
            y_range = {edge->get_target()->get_y(), edge->get_twin()->get_target()->get_y()};
        } else {
            y_range = {edge->get_twin()->get_target()->get_y(), edge->get_target()->get_y()};
        }

        double min_x = std::min(edge->get_target()->get_x(), edge->get_twin()->get_target()->get_x());
        std::pair<double, double> x_range = {min_x, min_x + y_dist};

        auto right_bound = y_x_tree->find_extremum({y_range, x_range}, true);

        if (!right_bound) {
            return 1.0;
        }

        if (right_bound->get_associated_vertex()->get_x() < std::max(edge->get_target()->get_x(), edge->get_twin()->get_target()->get_x())) {
            // no rectangle possible.
            return -1.0;
        }

        double max_x = right_bound->get_associated_vertex()->get_x();
        x_range = {max_x - y_dist, max_x};
        auto left_bound = y_x_tree->find_extremum({y_range, x_range}, false);

        if (!left_bound) {
            return 1.0;
        }

        return (right_bound->get_associated_vertex()->get_x() - left_bound->get_associated_vertex()->get_x()) / y_dist;
    }
}
