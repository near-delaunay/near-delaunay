#include "DualEdgeRatio.h"
#include <GeometryHelper.h>

double DualEdgeRatio::calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) {
    std::vector<double> edge_vector(2);
    edge_vector[0] = edge->get_target()->get_x() - edge->get_previous()->get_target()->get_x();
    edge_vector[1] = edge->get_target()->get_y() - edge->get_previous()->get_target()->get_y();
    double edge_length = std::sqrt(edge_vector[0] * edge_vector[0] + edge_vector[1] * edge_vector[1]);

    if (edge_length == 0) return 0;

    std::pair<double, double> p1 = std::make_pair(edge->get_previous()->get_target()->get_x(), edge->get_previous()->get_target()->get_y());
    std::pair<double, double> p2 = std::make_pair(edge->get_target()->get_x(), edge->get_target()->get_y());
    std::pair<double, double> p3 = std::make_pair(edge->get_next()->get_target()->get_x(), edge->get_next()->get_target()->get_y());
    std::pair<double, double> p4 = std::make_pair(edge->get_twin()->get_next()->get_target()->get_x(), edge->get_twin()->get_next()->get_target()->get_y());

    std::pair<double, double> circumcenter1 = GeometryHelper::circumcenter(p1, p2, p3);
    std::pair<double, double> circumcenter2 = GeometryHelper::circumcenter(p1, p2, p4);

    std::vector<double> dual_vector(2);
    dual_vector[0] = circumcenter2.first - circumcenter1.first;
    dual_vector[1] = circumcenter2.second - circumcenter1.second;
    double dual_length = std::sqrt(dual_vector[0] * dual_vector[0] + dual_vector[1] * dual_vector[1]);

    if (GeometryHelper::signed_angle(edge_vector, dual_vector) > 0) {
        return dual_length / edge_length;
    }

    return -dual_length / edge_length;
}
