#ifndef NEAR_DELAUNAY_DUALEDGERATIO_H
#define NEAR_DELAUNAY_DUALEDGERATIO_H


#include "EdgeBasedAlgorithm.h"

class DualEdgeRatio : public EdgeBasedAlgorithm {
protected:
    double calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) override;

public:
    DualEdgeRatio() : DualEdgeRatio("DualEdgeRatio") {};
    explicit DualEdgeRatio(std::string name) : EdgeBasedAlgorithm(std::move(name)) {};
};


#endif //NEAR_DELAUNAY_DUALEDGERATIO_H
