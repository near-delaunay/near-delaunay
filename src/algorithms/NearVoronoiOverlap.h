#ifndef NEAR_DELAUNAY_NEARVORONOIOVERLAP_H
#define NEAR_DELAUNAY_NEARVORONOIOVERLAP_H


#include "EdgeBasedAlgorithm.h"

class NearVoronoiOverlap : public EdgeBasedAlgorithm {
protected:
    double calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) override;

public:
    NearVoronoiOverlap() : NearVoronoiOverlap("Near-VoronoiOverlap") {};
    explicit NearVoronoiOverlap(std::string name) : EdgeBasedAlgorithm(std::move(name)) {};
};


#endif //NEAR_DELAUNAY_NEARVORONOIOVERLAP_H
