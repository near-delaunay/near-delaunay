#ifndef NEAR_DELAUNAY_LOCALELLIPSE_H
#define NEAR_DELAUNAY_LOCALELLIPSE_H


#include "EdgeBasedAlgorithm.h"

class LocalEllipse : public EdgeBasedAlgorithm {
private:
    // number of discrete angles which should be tried to find the maximal aspect ratio.
    int no_angles;

    /**
     * Calculate the aspect ratio of the ellipse through the four points associated to the given edge,
     * rotated by the given angle.
     *
     * @param edge  Edge for which the ellipse should be constructed.
     * @param dcel  DCEL in which the edge is.
     * @param angle Angle by which the ellipse should be rotated (in radiant).
     * @return      Aspect ratio of the defined ellipse.
     */
    double getAspectRatio(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel, double angle);

protected:
    double calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) override;

public:
    LocalEllipse(int no_angles) : LocalEllipse("LocalEllipse", no_angles) {};
    explicit LocalEllipse(std::string name, int no_angles) : EdgeBasedAlgorithm(std::move(name)), no_angles(no_angles) {};

};


#endif //NEAR_DELAUNAY_LOCALELLIPSE_H
