#include <iostream>
#include <GeometryHelper.h>
#include "LocalEllipse.h"

double LocalEllipse::calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) {
    double max_aspect_ratio = 0;

    for (int i = 0; i < no_angles; i++) {
        max_aspect_ratio = std::max(max_aspect_ratio, getAspectRatio(edge, dcel, i * M_PI / (2 * no_angles)));
    }

    std::vector<double> edge_vector(2);
    edge_vector[0] = edge->get_target()->get_x() - edge->get_previous()->get_target()->get_x();
    edge_vector[1] = edge->get_target()->get_y() - edge->get_previous()->get_target()->get_y();

    std::pair<double, double> p1 = std::make_pair(edge->get_previous()->get_target()->get_x(), edge->get_previous()->get_target()->get_y());
    std::pair<double, double> p2 = std::make_pair(edge->get_target()->get_x(), edge->get_target()->get_y());
    std::pair<double, double> p3 = std::make_pair(edge->get_next()->get_target()->get_x(), edge->get_next()->get_target()->get_y());
    std::pair<double, double> p4 = std::make_pair(edge->get_twin()->get_next()->get_target()->get_x(), edge->get_twin()->get_next()->get_target()->get_y());

    std::pair<double, double> circumcenter1 = GeometryHelper::circumcenter(p1, p2, p3);
    std::pair<double, double> circumcenter2 = GeometryHelper::circumcenter(p1, p2, p4);

    std::vector<double> dual_vector(2);
    dual_vector[0] = circumcenter2.first - circumcenter1.first;
    dual_vector[1] = circumcenter2.second - circumcenter1.second;

    if (GeometryHelper::signed_angle(edge_vector, dual_vector) > 0) return 2 - max_aspect_ratio;

    return max_aspect_ratio;
}
/**
 *  The formulas for this implementation are based on the descriptions of this website:
 *  http://www.robertobigoni.eu/Matematica/Conics/Ellipse4P/Ellipse4P.html
 *
 *  Before making use of the formulas, the points are rotated by alpha after which the axis-aligned ellipse is created.
 *
 *  To make life easier, I have introduced helping variables which are explained when they are first introduced.
 */
double LocalEllipse::getAspectRatio(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel, double angle) {
    std::shared_ptr<DCELVertex> A = edge->get_target();
    std::shared_ptr<DCELVertex> B = edge->get_twin()->get_target();
    std::shared_ptr<DCELVertex> C = edge->get_next()->get_target();
    std::shared_ptr<DCELVertex> D = edge->get_twin()->get_next()->get_target();

    double cosAlpha = cos(angle);
    double sinAlpha = sin(angle);

    /**
     * rotated x and y pos of the points where the local triangulation looks as follows:
     *
     *          C
     *        /    \
     *      A  ---  B
     *        \   /
     *          D
     *
     */
    double x1 = A->get_x() * cosAlpha - A->get_y() * sinAlpha;
    double x2 = B->get_x() * cosAlpha - B->get_y() * sinAlpha;
    double x3 = C->get_x() * cosAlpha - C->get_y() * sinAlpha;
    double x4 = D->get_x() * cosAlpha - D->get_y() * sinAlpha;

    double y1 = A->get_x() * sinAlpha + A->get_y() * cosAlpha;
    double y2 = B->get_x() * sinAlpha + B->get_y() * cosAlpha;
    double y3 = C->get_x() * sinAlpha + C->get_y() * cosAlpha;
    double y4 = D->get_x() * sinAlpha + D->get_y() * cosAlpha;


    /**
     * get helper variables for system of linear equations. The equation (8) of the website can then be simplified to:
     *
     * a * p + b * q + c * u = d * v
     * e * p + f * q + g * u = h * v
     * i * p + j * q + k * u = l * v
     */
    double a = y1 * y1 - y2 * y2;
    double b = x1 * x1 - x2 * x2;
    double c = -2 * (x1 - x2);
    double d = 2 * (y1 - y2);

    double e = y1 * y1 - y3 * y3;
    double f = x1 * x1 - x3 * x3;
    double g = -2 * (x1 - x3);
    double h = 2 * (y1 - y3);

    double i = y1 * y1 - y4 * y4;
    double j = x1 * x1 - x4 * x4;
    double k = -2 * (x1 - x4);
    double l = 2 * (y1 - y4);

    /**
     * Solving the previous system of linear equations, will result in infinitely many solutions,
     * but if we solve p, and q for v we can at least get the factor between the variables.
     * Thus we get p = pf * v and q = qf * v.
     * As the aspect ratio only depend on the ratio of p and q we don't have to find the exact values for p and q and can instead
     * return pf/qf or qf/pf respectively.
     */
    double pf = ((((b * g) * l)) - (((b * h) * k)) - (((c * f) * l)) + (((c * h) * j)) + (((d * f) * k)) - (((d * g) * j)))
                / (((a * f) * k) - ((a * g) * j) - ((b * e) * k) + ((b * g) * i) + ((c * e) * j) - ((c * f) * i));
    double qf = (((((-a) * g) * l)) + (((a * h) * k)) + (((c * e) * l)) - (((c * h) * i)) - (((d * e) * k)) + (((d * g) * i)))
                / (((a * f) * k) - ((a * g) * j) - ((b * e) * k) + ((b * g) * i) + ((c * e) * j) - ((c * f) * i));

    return std::min(pf/qf, qf/pf);
}
