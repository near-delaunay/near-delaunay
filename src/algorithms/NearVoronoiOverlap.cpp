#include <GeometryHelper.h>
#include "NearVoronoiOverlap.h"

double NearVoronoiOverlap::calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) {
    std::vector<double> edge_vector(2);
    edge_vector[0] = edge->get_target()->get_x() - edge->get_previous()->get_target()->get_x();
    edge_vector[1] = edge->get_target()->get_y() - edge->get_previous()->get_target()->get_y();
    double edge_length = std::sqrt(edge_vector[0] * edge_vector[0] + edge_vector[1] * edge_vector[1]);

    if (edge_length == 0) return 0;

    std::pair<double, double> p1 = std::make_pair(edge->get_previous()->get_target()->get_x(), edge->get_previous()->get_target()->get_y());
    std::pair<double, double> p2 = std::make_pair(edge->get_target()->get_x(), edge->get_target()->get_y());
    std::pair<double, double> p3 = std::make_pair(edge->get_next()->get_target()->get_x(), edge->get_next()->get_target()->get_y());
    std::pair<double, double> p4 = std::make_pair(edge->get_twin()->get_next()->get_target()->get_x(), edge->get_twin()->get_next()->get_target()->get_y());

    std::pair<double, double> circumcenter1 = GeometryHelper::circumcenter(p1, p2, p3);
    std::pair<double, double> circumcenter2 = GeometryHelper::circumcenter(p1, p2, p4);

    std::shared_ptr<Line> bisector13 = GeometryHelper::perpendicular_bisector(p1, p3);
    std::shared_ptr<Line> bisector14 = GeometryHelper::perpendicular_bisector(p1, p4);
    std::shared_ptr<Line> bisector23 = GeometryHelper::perpendicular_bisector(p2, p3);
    std::shared_ptr<Line> bisector24 = GeometryHelper::perpendicular_bisector(p2, p4);

    std::pair<double, double> intersection_bisectors1 = GeometryHelper::line_intersection(bisector13, bisector14);
    std::pair<double, double> intersection_bisectors2 = GeometryHelper::line_intersection(bisector23, bisector24);

    // Detect case where no overlap exists
    std::vector<double> dual_vector(2);
    dual_vector[0] = circumcenter2.first - circumcenter1.first;
    dual_vector[1] = circumcenter2.second - circumcenter1.second;

    if (GeometryHelper::signed_angle(edge_vector, dual_vector) < 0) return 0;

    std::vector<double> circumcenter_vector{circumcenter2.first - circumcenter1.first, circumcenter2.second - circumcenter1.second};
    std::vector<double> intersection_bisectors_vector{intersection_bisectors2.first - intersection_bisectors1.first, intersection_bisectors2.second -intersection_bisectors1.second};

    double overlapping_area = 0.5 * std::abs(intersection_bisectors_vector[0] * circumcenter_vector[1] - circumcenter_vector[0] * intersection_bisectors_vector[1]);

    return (overlapping_area / edge_length);
}
