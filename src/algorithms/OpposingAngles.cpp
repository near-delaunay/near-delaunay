#include "OpposingAngles.h"

double OpposingAngles::calculate_edge(std::shared_ptr<Halfedge> edge, std::shared_ptr<DCEL> dcel) {
    return edge->get_next()->get_angle_to_next() + edge->get_twin()->get_next()->get_angle_to_next();
}