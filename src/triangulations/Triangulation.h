#ifndef NEAR_DELAUNAY_TRIANGULATION_H
#define NEAR_DELAUNAY_TRIANGULATION_H

#include <memory>
#include <utility>
#include "../datastructures/graph/GraphVertex.h"
#include "../datastructures/graph/Graph.h"

class Triangulation {
private:
    std::string name;

protected:
    virtual std::shared_ptr<Graph> add_new_edges(std::shared_ptr<Graph> graph, std::string seed, char delimiter) = 0;

public:
    explicit Triangulation(std::string name) : name(std::move(name)) {};

    std::shared_ptr<Graph> make_triangulation(std::shared_ptr<Graph> graph, std::string seed, char delimiter) {
        return add_new_edges(graph, std::move(seed), delimiter);
    }

    std::string get_name() {
        return name;
    }
};


#endif //NEAR_DELAUNAY_TRIANGULATION_H
