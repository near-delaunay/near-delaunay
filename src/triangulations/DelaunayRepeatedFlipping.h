#ifndef NEAR_DELAUNAY_DELAUNAYREPEATEDFLIPPING_H
#define NEAR_DELAUNAY_DELAUNAYREPEATEDFLIPPING_H

#include <graph/Graph.h>
#include "Triangulation.h"

/**
 * Class to create a near Delaunay triangulation for a given Graph by creating a Delaunay triangulation and flipping random edges.
 */
class DelaunayRepeatedFlipping : public Triangulation {
protected:

    /**
     * Format of the seed, separated by the delimiter:
     * -    Number of flips
     * -    Seed for random generator
     */
    std::shared_ptr<Graph> add_new_edges(std::shared_ptr<Graph> graph, std::string seed, char delimiter) override;

public:
    explicit DelaunayRepeatedFlipping(std::string name) : Triangulation(std::move(name)) {};

    explicit DelaunayRepeatedFlipping() : Triangulation("DelaunayTriangulationWithRepeatedFlipping") {};
};

#endif //NEAR_DELAUNAY_DELAUNAYREPEATEDFLIPPING_H
