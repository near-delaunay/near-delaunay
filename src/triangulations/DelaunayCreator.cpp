#include "DelaunayCreator.h"
#include <delaunator.hpp>
#include <HashHelper.h>
#include <unordered_map>

std::shared_ptr<Graph> DelaunayCreator::add_new_edges(std::shared_ptr<Graph> graph, std::string seed, char delimiter) {

    std::vector<double> coordinates;

    std::unordered_map<std::pair<double, double>, std::shared_ptr<GraphVertex>, pair_hash> pos_to_graph;

    std::shared_ptr<Graph> new_graph = std::make_shared<Graph>();

    // create coordinate vector and clean all edges and create vertex map
    for (const std::shared_ptr<GraphVertex>& vertex : graph->get_vertices()) {
        coordinates.push_back(vertex->get_x());
        coordinates.push_back(vertex->get_y());

        auto new_vertex = std::make_shared<GraphVertex>(vertex->get_x(), vertex->get_y());
        new_graph->add_vertex(new_vertex);

        pos_to_graph[{vertex->get_x(), vertex->get_y()}] = new_vertex;
    }

    delaunator::Delaunator delaunay_triangulation(coordinates);

    std::unordered_set<std::pair<std::pair<double, double>, std::pair<double, double>>, pair_of_pairs_hash> added_edges;

    // get edges from the delaunator
    for(std::size_t i = 0; i < delaunay_triangulation.triangles.size(); i+=3) {
        std::pair<double, double> point1 = std::make_pair(delaunay_triangulation.coords[2 * delaunay_triangulation.triangles[i]], delaunay_triangulation.coords[2 * delaunay_triangulation.triangles[i] + 1]);
        std::pair<double, double> point2 = std::make_pair(delaunay_triangulation.coords[2 * delaunay_triangulation.triangles[i + 1]], delaunay_triangulation.coords[2 * delaunay_triangulation.triangles[i + 1] + 1]);
        std::pair<double, double> point3 = std::make_pair(delaunay_triangulation.coords[2 * delaunay_triangulation.triangles[i + 2]], delaunay_triangulation.coords[2 * delaunay_triangulation.triangles[i + 2] + 1]);

        std::vector<std::pair<std::pair<double, double>, std::pair<double, double>>> edges =
                {std::make_pair(point1, point2), std::make_pair(point1, point3), std::make_pair(point2, point3)};

        for (std::pair<std::pair<double, double>, std::pair<double, double>> edge : edges) {
            if (added_edges.find(edge) == added_edges.end()) {
                new_graph->add_edge(pos_to_graph[edge.first], pos_to_graph[edge.second]);
                added_edges.insert(edge);
                added_edges.insert({edge.second, edge.first});
            }
        }
    }

    return new_graph;
}
