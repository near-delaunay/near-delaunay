#include "TriangulationEnumerator.h"

// We treat the triangulations as a graph, where each possible triangulation is a vertex and two vertices are connected
// by an edge (t_1, t_2) if t_2 can be created from t_1 with a single edge flip (and thus vice verca).
// As it is proven that all possible triangulations are connected that way, we can use a BFS to iterate all of them.
void TriangulationEnumerator::run(std::shared_ptr<DCEL> dcel, void func (std::shared_ptr<HashableDCEL>, double), std::vector<std::shared_ptr<EdgeBasedAlgorithm>> algorithms, double factor) {
    // Hash function to hash bitsets.
    std::hash<std::bitset<400>> hash_fn;

    // Hash of all the triangulations which have been seen.
    std::set<size_t> seen;

    // Hash of all the triangulations which have been visited and expanded.
    std::set<size_t> expanded;

    // Stack of edges to be flipped. As we have to reach triangulations by flipping edges. Each edge has to be flipped an even number of times.
    // If we could get from triangulation t_s to t_1 and t_2 by flipping e_1 and e_2 respectively, we would travers that
    // graph by first flipping e_1 to get to t_1, then flipping e_1 a second time to get back to t_s and then flipping e_2.
    //        t_1
    //       /
    //   t_s
    //       \
    //        t_2
    std::stack<std::shared_ptr<Halfedge>> stack;

    // The variable to keep track of the current DCEL.
    std::shared_ptr<HashableDCEL> hashableDcel = std::make_shared<HashableDCEL>(dcel->get_vertices(), dcel->get_edges(), dcel->get_faces(), algorithms);

    for (const auto& edge : hashableDcel->get_flippable_edges()) {
        if ((*edge->get_target()) < (*edge->get_twin()->get_target())) {
            seen.insert(hash_fn(hashableDcel->get_flipped_hash(edge)));
            stack.push(edge);
        }
    }

    while (!stack.empty()) {
        std::shared_ptr<Halfedge> next_edge = stack.top();
        hashableDcel->flip_edge(next_edge);

        // we are returning to the vertex. Thus we don't have to process it further.
        if (expanded.find(hash_fn(hashableDcel->get_hash())) != expanded.end()) {
            stack.pop();
            continue;
        }

        // if we reach this point, this is a vertex that has not yet been expanded.
        // We therefore don't have to remove it from the stack (see comment on the stack variable).
        for (const auto& edge : hashableDcel->get_flippable_edges()) {
            if ((*edge->get_target()) < (*edge->get_twin()->get_target())) {
                if (seen.find(hash_fn(hashableDcel->get_flipped_hash(edge))) == seen.end()) {
                    seen.insert(hash_fn(hashableDcel->get_flipped_hash(edge)));
                    stack.push(edge);
                }
            }
        }

//        if (hashableDcel->get_hash() == start_hash) {
//            std::cout << "We got him! \n";
//            for (auto value : hashableDcel->get_multiset(0)) {
//                std::cout << value << " ";
//            }
//            std::cout << std::endl;
//        }

        expanded.insert(hash_fn(hashableDcel->get_hash()));
        func(hashableDcel, factor);
    }
}
