#ifndef NEAR_DELAUNAY_TRIANGULATIONENUMERATOR_H
#define NEAR_DELAUNAY_TRIANGULATIONENUMERATOR_H


#include "../datastructures/dcel/DCEL.h"
#include "../algorithms/EdgeBasedAlgorithm.h"
#include "../datastructures/dcel/HashableDCEL.h"
#include "../IPEexport/IpeCanvas.h"
#include "../datastructures/GraphParser.h"
#include <stack>

/**
 * A static class which allows to iterate over all possible triangulations of a given pointset (in form of a valid DCEL)
 * and allows running a specified function on these triangulations.
 */
class TriangulationEnumerator {

public:
    /**
     * Run over all the triangulations possible for the given DCEL and for each triangulation execute func.
     * For each DCEL, keep track of the scores in a HashableDCEL with the algorithms as specified by algorithms.
     *
     * @param dcel          The original triangulation based on which all other triangulations should be found.
     * @param func          The function to which all possible triangulations should be passed.
     * @param algorithms    The algorithms which should be kept track of in the HashableDCEL.
     * @param factor        Additional argument used in the call to func.
     */
    static void run(std::shared_ptr<DCEL> dcel, void func (std::shared_ptr<HashableDCEL>, double), std::vector<std::shared_ptr<EdgeBasedAlgorithm>> algorithms, double factor);
};


#endif //NEAR_DELAUNAY_TRIANGULATIONENUMERATOR_H
