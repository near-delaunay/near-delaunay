#include <dcel/DCEL.h>
#include <GraphParser.h>
#include <StringParseHelper.h>
#include <random>

#include "DelaunayRepeatedFlipping.h"
#include "DelaunayCreator.h"

std::shared_ptr<Graph> DelaunayRepeatedFlipping::add_new_edges(std::shared_ptr<Graph> graph, std::string seed, char delimiter) {
    DelaunayCreator delaunay_creator;
    graph = delaunay_creator.make_triangulation(graph, "", delimiter);

    std::shared_ptr<DCEL> dcel = GraphParser::parse_graph_to_DCEL(graph);

    std::vector<std::string> split_seed = StringParseHelper::split_string(seed, delimiter);

    int n = std::stoi(split_seed[0]);

    std::seed_seq random_seed(split_seed[1].begin(), split_seed[1].end());
    std::mt19937 generator(random_seed);

    std::uniform_int_distribution<int> distribution(0, dcel->get_edges().size() - 1);

    std::list<std::shared_ptr<Halfedge>> list_of_edges = dcel->get_edges();
    std::vector<std::shared_ptr<Halfedge>> vector_of_edges;
    vector_of_edges.reserve(list_of_edges.size());
    std::copy(std::begin(list_of_edges), std::end(list_of_edges), std::back_inserter(vector_of_edges));

    dcel->get_outer_face();

    for (int i = 0; i < n; i++) {
        dcel->flip_edge(vector_of_edges[distribution(generator)]);
    }

    return GraphParser::parse_DCEL_to_graph(dcel);
}
