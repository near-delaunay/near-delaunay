#ifndef NEAR_DELAUNAY_DELAUNAYCREATOR_H
#define NEAR_DELAUNAY_DELAUNAYCREATOR_H


#include <graph/Graph.h>
#include "Triangulation.h"

/**
 * Class to create Delaunay triangulation for a given Graph.
 */
class DelaunayCreator : public Triangulation {
protected:
    std::shared_ptr<Graph> add_new_edges(std::shared_ptr<Graph> graph, std::string seed, char delimiter) override;

public:
    explicit DelaunayCreator(std::string name) : Triangulation(std::move(name)) {};

    explicit DelaunayCreator() : Triangulation("DelaunayTriangulation") {};
};

#endif //NEAR_DELAUNAY_DELAUNAYCREATOR_H
