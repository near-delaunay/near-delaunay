# Add subdirectories
add_subdirectory(algorithms)
add_subdirectory(datastructures)
add_subdirectory(helpers)
add_subdirectory(IPEexport)
add_subdirectory(pipeline)
add_subdirectory(point_generators)
add_subdirectory(triangulations)
