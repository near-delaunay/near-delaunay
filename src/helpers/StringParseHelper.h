#ifndef NEAR_DELAUNAY_STRINGPARSEHELPER_H
#define NEAR_DELAUNAY_STRINGPARSEHELPER_H

#include <vector>
#include <string>
#include <sstream>

class StringParseHelper {
public:
    static std::vector<std::string> split_string(const std::string& input_string, char delimiter) {
        std::stringstream string_stream(input_string);
        std::string segment;

        std::vector<std::string> result;

        while (std::getline(string_stream, segment, delimiter)) {
            result.push_back(segment);
        }

        return result;
    }
};

#endif //NEAR_DELAUNAY_STRINGPARSEHELPER_H
