#include <utility>
#include <vector>
#include <stdexcept>
#include <cmath>

#include "GeometryHelper.h"
#include "Line.h"

std::shared_ptr<Line> GeometryHelper::perpendicular_bisector(std::pair<double, double> p1, std::pair<double, double> p2) {
    double midpoint_x = (p1.first + p2.first) / 2.0;
    double midpoint_y = (p1.second + p2.second) / 2.0;

    std::shared_ptr<Line> line = std::make_shared<Line>(p1, p2);

    std::shared_ptr<Line> perpendicular_bisector = nullptr;

    if (line->is_vertical_asymptote()) {
        perpendicular_bisector = std::make_shared<Line>(0, midpoint_y);
    } else {
        double slope = line->get_slope();

        if (slope == 0) {
            perpendicular_bisector = std::make_shared<Line>(midpoint_x);
        } else {
            double perpendicular_slope = - 1.0 / slope;
            double perpendicular_intercept = midpoint_y - perpendicular_slope * midpoint_x;
            perpendicular_bisector = std::make_shared<Line>(perpendicular_slope, perpendicular_intercept);
        }
    }

    return perpendicular_bisector;
}

std::pair<double, double> GeometryHelper::line_intersection(double slope1, double axis_intercept1, double slope2, double axis_intercept2) {
    std::pair<double, double> intercept;

    if (slope1 == slope2) return std::make_pair(nan(""), nan(""));

    intercept.first = (axis_intercept2 - axis_intercept1) / (slope1 - slope2);
    intercept.second = intercept.first * slope1 + axis_intercept1;

    return intercept;
}

std::pair<double, double> GeometryHelper::line_intersection(std::shared_ptr<Line> line1, std::shared_ptr<Line> line2) {
    if (!line1->is_vertical_asymptote() && !line2->is_vertical_asymptote()) {
        return GeometryHelper::line_intersection(line1->get_slope(), line1->get_y_intercept(), line2->get_slope(), line2->get_y_intercept());
    } else if (line1->is_vertical_asymptote() && !line2->is_vertical_asymptote()) {
        double y_coordinate_intersection = line2->get_y_intercept() + line2->get_slope() * line1->get_x_intercept();
        return std::make_pair(line1->get_x_intercept(), y_coordinate_intersection);
    } else if (!line1->is_vertical_asymptote() && line2->is_vertical_asymptote()) {
        double y_coordinate_intersection = line1->get_y_intercept() + line1->get_slope() * line2->get_x_intercept();
        return std::make_pair(line2->get_x_intercept(), y_coordinate_intersection);
    } else {
        // Both asymptotes
        return std::make_pair(nan(""), nan(""));
    }
}

std::pair<double, double> GeometryHelper::circumcenter(std::pair<double, double> p1, std::pair<double, double> p2, std::pair<double, double> p3) {
    std::shared_ptr<Line> bisector1 = perpendicular_bisector(p1, p2);
    std::shared_ptr<Line> bisector2 = perpendicular_bisector(p2, p3);

    return GeometryHelper::line_intersection(bisector1, bisector2);
}

double GeometryHelper::signed_angle(std::vector<double> v1, std::vector<double> v2) {
    if (v1.size() != 2 || v2.size() != 2) throw std::invalid_argument("Only 2D vectors supported");

    double dot_product = v1[0] * v2[0] + v1[1] * v2[1];
    double determinant = v1[0] * v2[1] - v1[1] * v2[0];

    return std::atan2(determinant, dot_product);
}

bool GeometryHelper::line_segment_intersect(std::pair<double, double> p1, std::pair<double, double> p2,
                                            std::pair<double, double> p3, std::pair<double, double> p4) {
    std::shared_ptr<Line> line1 = std::make_shared<Line>(p1, p2);
    std::shared_ptr<Line> line2 = std::make_shared<Line>(p3, p4);

    // Parallel vertical asymptotes
    if (line1->is_vertical_asymptote() && line2->is_vertical_asymptote()) {
        // Parallel non-intersecting vertical asymptotes
        if (line1->get_x_intercept() != line2->get_x_intercept()) return false;

        // Check whether line segments overlap
        return ((std::max(p1.second, p2.second) >= std::min(p3.second, p4.second)) && (std::min(p1.second, p2.second) <= std::max(p3.second, p4.second)));
    }

    // Parallel lines
    if (!line1->is_vertical_asymptote() && !line2->is_vertical_asymptote() && line1->get_slope() == line2->get_slope()) {
        // Parallel non-intersecting lines
        if ((p3.first - p1.first) * line1->get_slope() + p1.second != p3.second) return false;

        // Check whether line segments overlap
        return ((std::max(p1.first, p2.first) >= std::min(p3.first, p4.first)) && (std::min(p1.first, p2.first) <= std::max(p3.first, p4.first)));
    }

    // Non-parallel lines
    std::pair<double, double> intersection_point = GeometryHelper::line_intersection(line1, line2);

    bool on_vector1 = (intersection_point.first >= std::min(p1.first, p2.first)) && (intersection_point.first <= std::max(p1.first, p2.first));
    bool on_vector2 = (intersection_point.first >= std::min(p3.first, p4.first)) && (intersection_point.first <= std::max(p3.first, p4.first));

    return (on_vector1 && on_vector2);
}

std::pair<double, double> GeometryHelper::midpoint(std::pair<double, double> p1, std::pair<double, double> p2) {
    return std::pair<double, double>((p1.first + p2.first) / 2.0, (p1.second + p2.second) / 2.0);
}

double GeometryHelper::get_inclination(const double x, const double y) {
    double angle = fmod((std::atan2(y, x) + 2 * M_PI), (2 * M_PI));
    return angle;
}