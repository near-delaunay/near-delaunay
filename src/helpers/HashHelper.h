#ifndef NEAR_DELAUNAY_HASHHELPER_H
#define NEAR_DELAUNAY_HASHHELPER_H

/**
 * General hash helper.
 */

#include <cstdlib>
#include <memory>

/**
 * Hash of a pair.
 */
struct pair_hash
{
    template <class T1, class T2>
    std::size_t operator() (const std::pair<T1, T2>& pair) const
    {
        return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
    }
};

/**
 * Hash of a pair of pairs.
 */
struct pair_of_pairs_hash
{
    template <class T1, class T2, class T3, class T4>
    std::size_t operator() (const std::pair<std::pair<T1, T2>, std::pair<T3, T4>>& pair) const
    {
        return std::hash<T1>()(pair.first.first) ^ std::hash<T2>()(pair.first.second)
                                                   ^ std::hash<T3>()(pair.second.first)
                                                     ^ std::hash<T4>()(pair.second.second);
    }
};

#endif //NEAR_DELAUNAY_HASHHELPER_H
