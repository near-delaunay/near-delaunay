#ifndef NEAR_DELAUNAY_GEOMETRYHELPER_H
#define NEAR_DELAUNAY_GEOMETRYHELPER_H

#include <vector>
#include <memory>
#include "Line.h"

class GeometryHelper {
public:
    /**
     * Calculates the perpendicular bisector between two points in 2D Cartesian space.
     * @param p1    Coordinates of point 1.
     * @param p2    Coordinates of point 2.
     * @return      Perpendicular bisector for points p1, p2 described by the slope and intercept.
     */
    static std::shared_ptr<Line> perpendicular_bisector(std::pair<double, double> p1, std::pair<double, double> p2);

    /**
     * Calculates the point of intersection between two lines in 2D Cartesian space.
     * @param slope1            Slope of the first line.
     * @param axis_intercept1   Intercept of the first line.
     * @param slope2            Slope of the second line.
     * @param axis_intercept2   Intercept of the second line.
     * @return                  Coordinates of the point of intersection. If no or infinitely many intersections exist a pair of NaN is returned.
     */
    static std::pair<double, double> line_intersection(double slope1, double axis_intercept1, double slope2, double axis_intercept2);

    /**
     * Calculates the point of intersection between two line objects
     * @param line1         First Line object.
     * @param line2         Second Line object.
     * @return              Coordinates of the point of intersection. If no intersection exists, a pair of NaN is returned.
     */
    static std::pair<double, double> line_intersection(std::shared_ptr<Line> line1, std::shared_ptr<Line> line2);

    /**
     * Calculates the circumcenter for three given points in 2D Cartesian space.
     * @param p1    Coordinates of point 1.
     * @param p2    Coordinates of point 2.
     * @param p3    Coordinates of point 3.
     * @return      Coordinates of the circumcenter.
     */
    static std::pair<double, double> circumcenter(std::pair<double, double> p1, std::pair<double, double> p2, std::pair<double, double> p3);

    /**
     * Calculates the signed angle between two given vectors in 2D Cartesian space.
     * @param v1    First 2-dimensional vector.
     * @param v2    Second 2-dimensional vector.
     * @return      Signed angle between the vectors in the range (-PI, PI].
     */
    static double signed_angle(std::vector<double> v1, std::vector<double> v2);

    /**
     * Calculates whether two given line segments in 2D Cartesian space intersect.
     * @param p1    Starting point of first line segment.
     * @param p2    Endpoint of first line segment.
     * @param p3    Starting point of second line segment.
     * @param p4    Endpoint of second line segment.
     * @return      Whether the two given line segments intersect.
     */
    static bool line_segment_intersect(std::pair<double, double> p1, std::pair<double, double> p2, std::pair<double, double> p3, std::pair<double, double> p4);

    /**
     * Calculates the midpoint of a line segment in 2D Cartesian space.
     * @param p1    Starting point of line segment.
     * @param p2    Endpoint of line segment.
     * @return      Midpoint of the given line segment.
     */
    static std::pair<double, double> midpoint(std::pair<double, double> p1, std::pair<double, double> p2);

    /**
     * Returns the angle of a vector to the positive x-axis.
     * @param x     x-component of vector.
     * @param y     y-component of vector.
     * @return      Angle in range [0, 2 * PI).
     */
    static double get_inclination(double x, double y);
};


#endif //NEAR_DELAUNAY_GEOMETRYHELPER_H
