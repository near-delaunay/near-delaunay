#ifndef NEAR_DELAUNAY_LINE_H
#define NEAR_DELAUNAY_LINE_H


#include <cmath>
#include <stdexcept>

class Line {
private:
    bool vertical_asymptote;
    double slope;
    double intercept;

public:
    Line(std::pair<double, double> p1, std::pair<double, double> p2) {
        this->vertical_asymptote = (p1.first == p2.first);

        if (this->vertical_asymptote) {
            this->intercept = p1.first;
        } else {
            this->slope = (p2.second - p1.second) / (p2.first - p1.first);
            this->intercept = p1.second - p1.first * this->slope;
        }
    }

    Line(bool is_vertical_asymptote, double slope, double y_intercept) {
        this->vertical_asymptote = is_vertical_asymptote;
        this->slope = slope;
        this->intercept = y_intercept;
    }

    Line(double slope, double y_intercept) {
        this->vertical_asymptote = false;
        this->slope = slope;
        this->intercept = y_intercept;
    }

    Line(double x_intercept) {
        this->vertical_asymptote = true;
        this->slope = nan("");
        this->intercept = x_intercept;
    }

    bool is_vertical_asymptote() {
        return this->vertical_asymptote;
    }

    double get_slope() {
        if (vertical_asymptote) {
            throw std::logic_error("Vertical asymptote, has no slope");
        }
        return this->slope;
    }

    double get_y_intercept() {
        if (vertical_asymptote) {
            throw std::logic_error("Vertical asymptote, has no y-intercept");
        }
        return this->intercept;
    }

    double get_x_intercept() {
        if (!vertical_asymptote) {
            throw std::logic_error("Not a vertical asymptote, has a y-intercept");
        }
        return this->intercept;
    }
};


#endif //NEAR_DELAUNAY_LINE_H
