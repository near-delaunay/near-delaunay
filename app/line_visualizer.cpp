#include <iostream>
#include <IpeDrawer.h>
#include <filesystem>

char csv_delimiter = ';';
char seed_delimiter = '#';

/**
 * Draw the graph and edge of one specific csv line.
 *
 * @param seed  The specification of the particular edge in the csv file (the entire line in the file).
 */
void draw_line(const std::string& seed) {
    IpeDrawer drawer;

    drawer.add_csv_line(seed, csv_delimiter, seed_delimiter);

    drawer.save(seed + ".ipe");
}

/**
 * Draw all lines specified in a csv file to separate files.
 *
 * @param location The location of the csv file for which the graphs should be drawn.
 */
void draw_csv(const std::string& location) {
    std::ifstream myfile;
    myfile.open (location);

    std::string line;
    std::string previous_seed;

    getline(myfile, line);

    if (myfile.is_open())
    {
        while (getline(myfile, line) )
        {
            draw_line(line);
        }
        myfile.close();
    }
}


int main() {
    // draw one specific line first
    const std::string edge_specification = "UniformPointGenerator;50#0#1000#0#1000#supersede107;DelaunayTriangulationWithRepeatedFlipping;100000#someseed;183,643;668,235;4.10986;-0.960227;111.519;-1";
    draw_line(edge_specification);
    std::cout << "line drawn \n";

    //draw an entire csv file
    // make sure you don't choose csv's that are too big. Otherwise you will end up with a lot of files ^^
    const std::string file_location = "../../output/reduced_experiment.csv";

    // Check whether file exists.
    if (!std::filesystem::exists(file_location)) {
        std::cout << "File not found" << std::endl;
        return 1;
    };


    draw_csv(file_location);
    std::cout << "file drawn \n";
}