#include <iostream>
#include <thread>
#include <Pipeline.h>
#include <OpposingAngles.h>
#include <DualEdgeRatio.h>
#include <SimplestPointGenerator.h>
#include <DelaunayRepeatedFlipping.h>
#include <NearVoronoiOverlap.h>
#include <AxisAlignedRectangle.h>
#include <sys/stat.h>
#include <UniformPointGenerator.h>
#include <LocalEllipse.h>
#include <DelaunayCreator.h>

char csv_delimiter = ';';
char seed_delimiter = '#';


/**
 * Run the pipeline with all specified point generators and metrics and store the results.
 */
void run_pipeline() {
    Pipeline pipeline;

    // Register algorithms.
    pipeline.register_algorithm(std::make_shared<OpposingAngles>(OpposingAngles()));
    pipeline.register_algorithm(std::make_shared<DualEdgeRatio>(DualEdgeRatio()));
    pipeline.register_algorithm(std::make_shared<NearVoronoiOverlap>(NearVoronoiOverlap()));
    pipeline.register_algorithm(std::make_shared<AxisAlignedRectangle>(AxisAlignedRectangle()));
    pipeline.register_algorithm(std::make_shared<LocalEllipse>(LocalEllipse(100)));

    // DCELs are created by running each registered generator and for each of them all registered triangulations are evaluated.
    // Algorithms are then run on all created DCELs.
    pipeline.register_generator(std::make_shared<SimplestPointGenerator>(SimplestPointGenerator()));
    pipeline.register_generator(std::make_shared<UniformPointGenerator>(UniformPointGenerator()), std::vector<std::string>{"100#0#1000#0#1000#supersede"});

    pipeline.register_triangulation(std::make_shared<DelaunayRepeatedFlipping>(DelaunayRepeatedFlipping()), std::vector<std::string>{"10000#someseed"});
    pipeline.register_triangulation(std::make_shared<DelaunayCreator>(DelaunayCreator()));

    mkdir("../../output", 0777);
    pipeline.execute("../../output/experiment1.csv", seed_delimiter);
}

int main() {
    run_pipeline();
}
