#include <iostream>
#include <OpposingAngles.h>
#include <DualEdgeRatio.h>
#include <SimplestPointGenerator.h>
#include <UniformPointGenerator.h>
#include <IpeDrawer.h>
#include <LocalEllipse.h>
#include <TriangulationEnumerator.h>

char csv_delimiter = ';';
char seed_delimiter = '#';

std::vector<std::shared_ptr<Graph>> best_graph = {};
std::vector<std::multiset<double>> best_multiset = {};
std::vector<std::shared_ptr<EdgeBasedAlgorithm>> algorithms;
std::vector<size_t> best_hash = {};
std::hash<std::bitset<400>> hash_fn;

std::bitset<400> delaunay_hash;

double edge_length_restriction = 0;
double best_edge_length = 0;
std::shared_ptr<Graph> best_edge_length_graph;

IpeCanvas canvas;
int number_triangulations_seen = 0;
int number_triangulation_within_range = 0;

bool is_multiset_bigger_than(const std::multiset<double>& set1, const std::multiset<double>& set2, bool start_beginning) {
    if (start_beginning) {
        auto it1 = set1.begin();
        auto it2 = set2.begin();
        while (it1 != set1.end()) {
            if (std::abs((*it1) - (*it2)) < 0.00001) {
                it1++;
                it2++;
                continue;
            }
            return (*it1) > (*it2);
        }
    } else {
        auto it1 = set1.rbegin();
        auto  it2 = set2.rbegin();
        while (it1 != set1.rend()) {
            if (std::abs((*it1) - (*it2)) < 0.00001) {
                it1++;
                it2++;
                continue;
            }
            return (*it1) > (*it2);
        }
    }

    return false;
}

void enumerator_function(std::shared_ptr<HashableDCEL> dcel, double factor) {
    number_triangulations_seen++;
    if (number_triangulations_seen % 5000 == 0) {
        std::cout << number_triangulations_seen << std::endl;
    }

    if (dcel->get_edge_length_sum() < edge_length_restriction * factor) {
        return;
    }

    number_triangulation_within_range++;

    if (dcel->get_edge_length_sum() > best_edge_length) {
        best_edge_length_graph = GraphParser::parse_DCEL_to_graph(dcel);
        best_edge_length = dcel->get_edge_length_sum();
    }

    for (int i = 0; i < algorithms.size(); i++) {
        std::multiset<double> current_multiset = dcel->get_multiset(i);
        for (int j = 0; j < 4; j++) {
            if (best_multiset.size() <= i * 4 + j) {
                best_multiset.push_back(current_multiset);
                best_graph.push_back(GraphParser::parse_DCEL_to_graph(dcel));
                best_hash.push_back(hash_fn(dcel->get_hash()));
            } else if (is_multiset_bigger_than(current_multiset, best_multiset[i * 4 + j], j < 2) xor (j % 2 == 1)) {
                best_multiset[i * 4 + j] = current_multiset;
                best_graph[i * 4 + j] = GraphParser::parse_DCEL_to_graph(dcel);
                best_hash[i * 4 + j] = hash_fn(dcel->get_hash());
            }
        }
    }
}

/**
 * Function for formatting the numeric seed as a constant length string.
 * @param i         Integer seed.
 * @param max       Maximum integer seed.
 * @return          Seed as string with equally many characters as the maximum seed would have.
 */
std::string get_seed_string(int i, int max) {
    // i should be at most the maximum.
    if (i > max) {
        throw std::runtime_error("Unsupported argument");
    }

    // Compute return value
    std::string string = std::to_string(i);
    int desired_length = std::to_string(max).length();

    std::string prepend(desired_length - string.length(), '0');
    return prepend + string;
}

/**
 * Generates an Ipe file with the best solutions for all algorithms over all possible triangulations for the four
 * possible lexicographical orderings.
 * @param numeric_seed      Seed used to generate the example.
 * @param max               Maximum over all seeds.
 * @param n_points          Number of points.
 * @param factor            Weight factor compared to Delaunay.
 */
void create_examples(int numeric_seed, int max, int n_points, double factor) {
    // Reset global variables.
    best_graph = {};
    best_multiset = {};
    best_hash = {};
    edge_length_restriction = 0;
    best_edge_length = 0;
    canvas = IpeCanvas();
    number_triangulations_seen = 0;
    number_triangulation_within_range = 0;

    // Retrieve seed as string object.
    std::string seed = get_seed_string(numeric_seed, max);
    std::cout << "Seed: " << seed << std::endl;

    // Create the graph.
    std::shared_ptr<Graph> graph = UniformPointGenerator().get_points(std::to_string(n_points) + "#0#1000#0#1000#" + seed, seed_delimiter);

    // Create the Delaunay triangulation.
    graph = DelaunayCreator().make_triangulation(graph, "", seed_delimiter);

    // Register the algorithms.
    algorithms = {
            std::make_shared<DualEdgeRatio>(),
            std::make_shared<OpposingAngles>(),
            std::make_shared<LocalEllipse>(180)
    };

    // Create the Doubly Connected Edge List datastructure.
    std::shared_ptr<DCEL> dcel = GraphParser::parse_graph_to_DCEL(graph);
    std::shared_ptr<HashableDCEL> hashableDcel = std::make_shared<HashableDCEL>(HashableDCEL(dcel->get_vertices(), dcel->get_edges(), dcel->get_faces(), algorithms));

    // Print the weight of the Delaunay triangulation.
    std::cout << "Weight of the Delaunay triangulation: " << hashableDcel->get_edge_length_sum() << std::endl;

    // Retrieve the features of the Delaunay triangulation.
    edge_length_restriction = hashableDcel->get_edge_length_sum();
    delaunay_hash = hashableDcel->get_hash();

    // Create the Ipe layer for the Delaunay triangulation.
    std::shared_ptr<IpeLayer> layer = canvas.create_layer("StartingPosition", true);
    layer->add_drawable(std::make_shared<IpeGraph>(IpeGraph(graph, "green")));

    // Iterate over all triangulations.
    TriangulationEnumerator::run(dcel, enumerator_function, algorithms, factor);

    // Return if no valid triangulation found.
    if (best_graph.empty()) {
        std::cout << "No valid triangulation was found for seed: " << seed << ", with factor: " << factor << "." << std::endl;
        return;
    }

    // Names of the different lexicographical orderings.
    std::string layer_names[4] = {"BeginningMax", "BeginningMin", "EndMax", "EndMin"};

    // Add layers to the Ipe files for all combinations of orderings and algorithms.
    for (int i = 0; i < algorithms.size(); i++) {
        for (int j = 0; j < 4; j++) {
            layer = canvas.create_layer(layer_names[j] + algorithms[i]->get_name(), false);
            layer->add_drawable(std::make_shared<IpeGraph>(IpeGraph(best_graph[i * 4 + j], "black")));
        }
    }

    std::cout << "Number of triangulations seen: " << number_triangulations_seen << "\n";
    std::cout << "Number of triangulations within range: " << number_triangulation_within_range << "\n \n";

    // Print for which algorithms and which lexicographical ordering the resulting triangulation is Delaunay.
    for (int i = 0; i < algorithms.size(); i++) {
        for (int j = 0; j < 4; j++) {
            if (best_hash[i * 4 + j] == hash_fn(delaunay_hash)) {
                std::cout << layer_names[j] + algorithms[i]->get_name() << " is Delaunay \n";
            }
        }
    }

    // Check which combinations of orderings and algorithms result in the same graphs.
    for (int i = 0; i < algorithms.size(); i++) {
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < algorithms.size(); k++) {
                for (int l = 0; l < 4; l++) {
                    if (i * 4 + j >= k * 4 + l) {
                        continue;
                    }

                    if (best_hash[i * 4 + j] == best_hash[k * 4 + l] && best_hash[i * 4 + j] != hash_fn(delaunay_hash)) {
                        std::cout << "The following graphs are the same: " << layer_names[j] + algorithms[i]->get_name() << " and " << layer_names[l] + algorithms[k]->get_name() << std::endl;
                    }
                }
            }
        }
    }

    std::cout << std::endl;

    // Create a layer for the triangulation with the maximum weight.
    layer = canvas.create_layer("LongestEdgeLength", false);
    layer->add_drawable(std::make_shared<IpeGraph>(IpeGraph(best_edge_length_graph, "red")));

    // Save the Ipe file.
    canvas.save("../../output/Points:" + std::to_string(n_points) + ",Factor:" + std::to_string(factor) + ",Seed:" + seed + ".ipe");
}

int main() {
    // Determine number of triangulations.
    int const min_seed = 0;
    int const max_seed = 49;

    // Determine number of points in the pointset.
    int const n_points = 14;

    // Determine the minimum weight factor over the weight of the Delaunay triangulation.
    double const factor = 1.5;

    // Create examples with seeds 0 up to max_seed.
    for (int seed = min_seed; seed <= max_seed; seed++) {
        std::cout << "------------ Creating a new example: -------------- \n";
        create_examples(seed, max_seed, n_points, factor);
    }
}
