# Near-Delaunay

This repository contains the code written by Benjamin Rodatz and Hidde Koerts to aid the research into meaningful metrics for measuring the 'Delaunay-ness' of arbitrary triangulations.

## Functionality

The main aim of this code is to evaluate various metrics on generated triangulations. These evaluations can subsequently also be visualized.

The code is currently capable of:
- Generate pointsets uniformly
- Compute the Delaunay triangulation
- Generate pseudo-random triangulations
- Store triangulation in a `.txt` file and read from file (currently not used)
- Evaluate various metrics
- Write results to a `.csv` file
- Visualize results via an `.ipe` file
- Step through all possible triangulation of a point set and optimize for one of the various metrics

The code has been set up with extendability in mind. If you desire any furher functionality, it should not be overly difficult to implement.

## Structure of the repository

The repository has a standard layout for C++ applications based on *CMake*. Here, we will briefly discuss what the main components are.


### `app`
This top level folder contains all executables. 

The `main_pipeline` is the basic application used for experimentation. It allows for defining point generators, triangulation generators, and evaluating the algorithms. Results can be written to `.csv` and to `.ipe`. 

The `line_visualizer` can be used to visualize more data in Ipe from a generated `.csv`. Specifically, it allows for highlighting a single edge in a triangulation.

The `triangulation_enumeration` runs through all possible triangulation for a generated point set. This allows you to brute force all kinds of statistics. Currently it is used to evaluate the best triangulations for all metrics with at least a given weight.

### `libraries`
This top level folder contains all external libraries used.

#### `libraries/delaunator`
A fast library used to compute Delaunay triangulations for given point sets.

### `src`
This top level folder contains all datastructures, algorithms, I/O and pipeline code. 

#### `src/IPEexport`
Our own implementaton of an IPE interface in C++. It supports the following operations:
- Adding colors
- Adding circles
- Adding ellipses
- Adding paths
- Adding polygons
- Drawing a graph
- Managing layers
- Defining the style

#### `src/algorithms`
Our implementation of the metrics. The following metrics are currently supported, all of which are edge-based:
- Axis aligned rectangles
- Dual edge ratio
- Local ellipse
- Near-Voronoi overlap
- Opposing angles

Please refer to the project documentation for the definition of these metrics.

#### `src/datastructures`
The various datastructures we use for storing triangulations. 

This directory contains the `GraphParser`, which contains the implementation of the translation between the various datastructures.

##### `src/datastructures/dcel`
The *Doubly connected edge list* (DCEL) datastructure is central in the computation of all metrics. While overkill for most of our computations, it provides great flexibility in obtaining information about the neighborhood of a vertex in a triangulation.

The DCEL consists of `DCELVertex`, `Halfedge` and `Face` objects. The state of these objects is all automatically updated for all implemented operations **except** for the manual insertion of individual vertices, halfedges and faces in the DCEL itself. Our specific DCEL implementation has a clockwise direction for all internal halfedges from the perspective of the faces.

In our implementation a DCEL can be constructed from a graph (via the aforementioned `GraphParser`), stepwise by adding edges and vertices at specified locations, or fully manually by adding the vertex, halfedge and face objects directly.

The DCEL supports the flipping of edges. It furthermore allows for retrieving the outer face.

##### `src/datastructures/graph`
This simple graph implementation is used for storing the results of the `delaunator` library, as well as for the drawing for IPE.

##### `src/datastructures/range_tree`
The range tree is used in the computation of the axis-aligned rectangle metric.

The range tree is currently not completely tested and should be used with care. Due to some complications, we have decided to not use it for now and iterate over all points in linear time instead of the logarithmic time of the tree. Given the number of used points this is usually sufficiently fast.

#### `src/helpers`
This directory stores various helpers, which perform simple auxiliary functions. The `GeometryHelper` implements various geometric computations, the `HashHelper` defines custom hashes for pairs and pairs of pairs, and the `StringParseHelper` provides functionality for parsing strings.

#### `src/pipeline`
The pipeline defines the general behaviour of the program. It keeps track of all the elements to be run, and executes their routines in order. One can register algorithms, point generators and triangulation generators with a pipeline, and subsequently execute the pipeline with the desired output file's name as argument.  It will then run for each triangulation generator all point generators, and compute the scores of all registered algorithms. Thus, the scores for all algorithms will be computed for every pair consisting of a registered triangulation generator and a registered point generator.

#### `src/point_generators`
Point generators are used to create point sets upon which triangulations can be generated. The `SimplestPointGenerator` allows for manually defining point sets, while the `UniformPointGenerator` uses uniformly generated random coordinates in a defined domain. The most important settings are the domain, the number of points and the seed used.

#### `src/triangulations`
Our code allows for multiple methods for creating triangulations. Currently, two have been implemented. The `DelaunayCreator` uses the `delaunator` library to generate a Delaunay triangulation. The `DelaunayRepeatedFlipping` generator first computes the Delaunay triangulations and then randomly flips a specified number of edges using the DCEL datastructure. While not perfectly random, it suffices for our purposes.

### `test`
This top level directory contains the unit tests for our code. The structure of this directory mimics that of the `SRC` directory. The coverage is far from perfect, but it helps test the most critical elements. It uses the standard Google Test framework, which is automatically downloaded when running the CMake project. If you already have it downloaded, it will check whether it is the most recent version. This check requires a network connection. If no network connection is available, this check can be prevented by setting the variable `INTERNET_CONNECTION` to false in the top level `CMakeList.txt`. 