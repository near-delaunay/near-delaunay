#include <gtest/gtest.h>
#include <GeometryHelper.h>
#include <cmath>

TEST (GeometryHelper, get_inclination_test) {
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(1, 0), 0);
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(123456789, 0), 0);
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(1, 1), M_PI / 4);
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(0, 1), M_PI / 2);
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(-1, 1), 3 * M_PI / 4);
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(-1, 0), M_PI);
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(-1, -1), 5 * M_PI / 4);
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(0, -1), 3 * M_PI / 2);
    ASSERT_DOUBLE_EQ(GeometryHelper::get_inclination(1, -1), 7 * M_PI / 4);
}
