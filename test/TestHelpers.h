#ifndef NEAR_DELAUNAY_TESTHELPERS_H
#define NEAR_DELAUNAY_TESTHELPERS_H


#include "../src/datastructures/graph/Graph.h"

/**
 * Simple functions that may be used in multiple tests.
 */
class TestHelpers {
public:
    /**
     * Create a Graph object based on positions for vertices and ids for edges (similar to CPPS input format).
     *
     * @param vertices  Vector of pairs of doubles, where the ith pair indicates the position of vertex i.
     * @param edges     Vector of pairs of ints where (i, j) indicates an edge between the ith vertex and the jth vertex.
     * @return          A graph with the corresponding vertices and edges.
     */
    static std::shared_ptr<Graph> create_graph(std::vector<std::pair<double, double>> vertices, std::vector<std::pair<int, int>> edges);

};


#endif //NEAR_DELAUNAY_TESTHELPERS_H
