#include "TestHelpers.h"

std::shared_ptr<Graph> TestHelpers::create_graph(std::vector<std::pair<double, double>> vertices, std::vector<std::pair<int, int>> edges) {
    std::shared_ptr<Graph> g = std::make_shared<Graph>();

    std::vector<std::shared_ptr<GraphVertex>> graph_vertices;

    for (auto vertex : vertices) {
        std::shared_ptr<GraphVertex> graph_vertex =
                std::make_shared<GraphVertex>(GraphVertex(vertex.first, vertex.second));
        g->add_vertex(graph_vertex);
        graph_vertices.push_back(graph_vertex);
    }

    for (auto edge : edges) {
        g->add_edge(graph_vertices.at(edge.first), graph_vertices.at(edge.second));
    }

    return g;
}
