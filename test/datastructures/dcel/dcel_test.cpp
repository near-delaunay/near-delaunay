#include <gtest/gtest.h>
#include <dcel/DCEL.h>

/**
 * Test for checking whether the DCEL structure is correctly initialized in combination with the initial_vertices method
 */
TEST (DCEL, check_initialization) {
    DCEL dcel;
    std::shared_ptr<DCELVertex> v0 = std::make_shared<DCELVertex>(DCELVertex(0, 0));
    std::shared_ptr<DCELVertex> v1 = std::make_shared<DCELVertex>(DCELVertex(1, 1));

    dcel.initial_vertices(v0, v1);

    ASSERT_EQ(v0->get_edges().size(), 1);
    ASSERT_EQ(v1->get_edges().size(), 1);

    ASSERT_EQ(v0->get_edges().front()->get_target(), v1);
    ASSERT_EQ(v1->get_edges().front()->get_target(), v0);

    ASSERT_EQ(v0->get_edges().front()->get_next(), v0->get_edges().front()->get_twin());
    ASSERT_EQ(v1->get_edges().front()->get_next(), v1->get_edges().front()->get_twin());

    ASSERT_EQ(v0, dcel.get_vertices().front());
    ASSERT_EQ(v1, dcel.get_vertices().back());

    ASSERT_EQ(dcel.get_edges().size(), 2);
    ASSERT_EQ(dcel.get_vertices().size(), 2);
    ASSERT_EQ(dcel.get_faces().size(), 1);
}

/**
 * Test for checking whether the add_vertex_at method of the DCEL datastructure works correctly
 */
TEST (DCEL, check_add_vertex_at) {
    DCEL dcel;
    std::shared_ptr<DCELVertex> v0 = std::make_shared<DCELVertex>(DCELVertex(0, 0));
    std::shared_ptr<DCELVertex> v1 = std::make_shared<DCELVertex>(DCELVertex(0, 1));
    std::shared_ptr<DCELVertex> v2 = std::make_shared<DCELVertex>(DCELVertex(1, 0));

    dcel.initial_vertices(v0, v1);
    dcel.add_vertex_at(v2, v1->get_edges().front());

    ASSERT_EQ(v0->get_edges().front()->get_face(), v2->get_edges().front()->get_face());
    ASSERT_EQ(v1->get_edges().front()->get_face(), v2->get_edges().front()->get_face());

    ASSERT_EQ(v2->get_edges().front()->get_target(),  v0);
    ASSERT_EQ(v0->get_edges().back()->get_target(), v2);

    ASSERT_EQ(v2->get_edges().front()->get_next()->get_target(), v1);
    ASSERT_EQ(v2->get_edges().front()->get_twin()->get_target(), v2);

    ASSERT_EQ(dcel.get_edges().size(), 4);
    ASSERT_EQ(dcel.get_faces().size(), 1);
    ASSERT_EQ(dcel.get_vertices().size(), 3);
}

/**
 * Test for checking whether the add_edge_at method of the DCEL datastructure works correctly
 */
TEST (DCEL, check_add_edge_at) {
    DCEL dcel;
    std::shared_ptr<DCELVertex> v0 = std::make_shared<DCELVertex>(DCELVertex(0, 0));
    std::shared_ptr<DCELVertex> v1 = std::make_shared<DCELVertex>(DCELVertex(0, 1));
    std::shared_ptr<DCELVertex> v2 = std::make_shared<DCELVertex>(DCELVertex(1, 0));

    dcel.initial_vertices(v0, v1);
    dcel.add_vertex_at(v2, v1->get_edges().front());
    dcel.add_edge_at(v2, v0->get_edges().front());

    ASSERT_FALSE(v0->get_edges().front()->get_face() == v0->get_edges().front()->get_twin()->get_face());
    ASSERT_EQ(v0->get_edges().front()->get_next()->get_next()->get_next(), v0->get_edges().front());
    ASSERT_EQ(v0->get_edges().back()->get_next()->get_next()->get_next(), v0->get_edges().back());

    ASSERT_EQ(dcel.get_edges().size(), 6);
    ASSERT_EQ(dcel.get_vertices().size(), 3);
    ASSERT_EQ(dcel.get_faces().size(), 2);
}

/**
 * Test for checking whether the DCEL datastructure correctly creates a triangular structure
 */
TEST (DCEL, general_test) {
    DCEL dcel;
    std::shared_ptr<DCELVertex> v0 = std::make_shared<DCELVertex>(DCELVertex(0, 0));
    std::shared_ptr<DCELVertex> v1 = std::make_shared<DCELVertex>(DCELVertex(0, 1));
    std::shared_ptr<DCELVertex> v2 = std::make_shared<DCELVertex>(DCELVertex(1, 0));

    dcel.initial_vertices(v0, v1);
    dcel.add_vertex_at(v2, v1->get_edges().front());
    dcel.add_edge_at(v2, v0->get_edges().front());

    ASSERT_EQ(v0->get_edges().front()->get_face(), v0->get_edges().front()->get_next()->get_face());
    ASSERT_EQ(v0->get_edges().front()->get_face(), v0->get_edges().front()->get_next()->get_next()->get_face());

    ASSERT_EQ(v0->get_edges().front()->get_twin()->get_face(),
              v0->get_edges().front()->get_twin()->get_next()->get_face());
    ASSERT_EQ(v0->get_edges().front()->get_twin()->get_face(),
              v0->get_edges().front()->get_twin()->get_next()->get_next()->get_face());

    ASSERT_FALSE(v0->get_edges().front()->get_face() == v0->get_edges().front()->get_twin()->get_face());
}

/**
 * Test for checking whether retrieving the adjacent edges from a face in a DCEL datastructure works correctly
 */
TEST (DCEL, get_adjacent_edges) {
    DCEL dcel;
    std::shared_ptr<DCELVertex> v0 = std::make_shared<DCELVertex>(DCELVertex(0, 0));
    std::shared_ptr<DCELVertex> v1 = std::make_shared<DCELVertex>(DCELVertex(0, 1));
    std::shared_ptr<DCELVertex> v2 = std::make_shared<DCELVertex>(DCELVertex(1, 0));

    dcel.initial_vertices(v0, v1);
    dcel.add_vertex_at(v2, v1->get_edges().front());
    dcel.add_edge_at(v2, v0->get_edges().front());

    ASSERT_EQ(v0->get_edges().front()->get_face()->get_adjacent_edges().size(), 3);
    ASSERT_EQ(v0->get_edges().back()->get_face()->get_adjacent_edges().size(), 3);
}

/**
 * Test for checking whether retrieving the adjacent faces from a face in a DCEL datastructure works correctly
 */
TEST (DCEL, get_adjacent_faces) {
    DCEL dcel;
    std::shared_ptr<DCELVertex> v0 = std::make_shared<DCELVertex>(DCELVertex(0, 0));
    std::shared_ptr<DCELVertex> v1 = std::make_shared<DCELVertex>(DCELVertex(0, 1));
    std::shared_ptr<DCELVertex> v2 = std::make_shared<DCELVertex>(DCELVertex(1, 0));

    dcel.initial_vertices(v0, v1);
    dcel.add_vertex_at(v2, v1->get_edges().front());
    dcel.add_edge_at(v2, v0->get_edges().front());

    ASSERT_EQ(v0->get_edges().front()->get_face()->get_adjacent_faces().size(), 1);
    ASSERT_EQ(v0->get_edges().front()->get_face()->get_adjacent_faces().count(
            v0->get_edges().front()->get_twin()->get_face()), 1);
}

/**
 * Test to check whether the flip_edge method of the DCEL datastructure works correctly
 */
TEST (DCEL, flip_edge) {
    DCEL dcel;
    std::shared_ptr<DCELVertex> v0 = std::make_shared<DCELVertex>(DCELVertex(-1, 0));
    std::shared_ptr<DCELVertex> v1 = std::make_shared<DCELVertex>(DCELVertex(0, 1));
    std::shared_ptr<DCELVertex> v2 = std::make_shared<DCELVertex>(DCELVertex(1, 0));
    std::shared_ptr<DCELVertex> v3 = std::make_shared<DCELVertex>(DCELVertex(0, -1));

    // Build initial quadrilateral
    dcel.initial_vertices(v0, v1);
    dcel.add_vertex_at(v2, v0->get_edges().front());
    dcel.add_vertex_at(v3, v1->get_edges().back());
    dcel.add_edge_at(v0, v2->get_edges().back());
    dcel.add_edge_at(v1, v2->get_edges().back());

    std::shared_ptr<Halfedge> h = dcel.get_edges().back();

    std::shared_ptr<Halfedge> h_twin = h->get_twin();
    std::shared_ptr<Halfedge> h_original_next = h->get_next();
    std::shared_ptr<Halfedge> h_original_previous = h->get_previous();
    std::shared_ptr<Halfedge> h_twin_original_next = h_twin->get_next();
    std::shared_ptr<Halfedge> h_twin_original_previous = h_twin->get_previous();

    ASSERT_EQ(dcel.get_vertices().size(), 4);
    ASSERT_EQ(dcel.get_edges().size(), 10);
    ASSERT_EQ(dcel.get_faces().size(), 3);

    ASSERT_EQ(h->get_target(), v3);
    ASSERT_EQ(h->get_previous()->get_target(), v1);

    ASSERT_EQ(h->get_twin()->get_twin(), h);
    ASSERT_EQ(h->get_next()->get_next()->get_next(), h);
    ASSERT_EQ(h->get_twin()->get_next()->get_next()->get_next(), h->get_twin());
    ASSERT_EQ(h->get_previous()->get_previous()->get_previous(), h);
    ASSERT_EQ(h->get_twin()->get_previous()->get_previous()->get_previous(), h->get_twin());

    ASSERT_EQ(h_original_next->get_previous(), h);
    ASSERT_EQ(h_original_previous->get_next(), h);
    ASSERT_EQ(h_original_next->get_next(), h_original_previous);
    ASSERT_EQ(h_original_previous->get_previous(), h_original_next);

    ASSERT_EQ(h_twin_original_next->get_previous(), h_twin);
    ASSERT_EQ(h_twin_original_previous->get_next(), h_twin);
    ASSERT_EQ(h_twin_original_next->get_next(), h_twin_original_previous);
    ASSERT_EQ(h_twin_original_previous->get_previous(), h_twin_original_next);

    ASSERT_TRUE(h->get_face() != h->get_twin()->get_face());

    dcel.flip_edge(h);

    ASSERT_EQ(dcel.get_vertices().size(), 4);
    ASSERT_EQ(dcel.get_edges().size(), 10);
    ASSERT_EQ(dcel.get_faces().size(), 3);

    ASSERT_EQ(h->get_target(), v0);
    ASSERT_EQ(h->get_previous()->get_target(), v2);

    ASSERT_EQ(h->get_twin()->get_target(), v2);
    ASSERT_EQ(h->get_twin()->get_previous()->get_target(), v0);

    ASSERT_EQ(h->get_twin()->get_twin(), h);
    ASSERT_EQ(h->get_next()->get_next()->get_next(), h);
    ASSERT_EQ(h->get_twin()->get_next()->get_next()->get_next(), h->get_twin());
    ASSERT_EQ(h->get_previous()->get_previous()->get_previous(), h);
    ASSERT_EQ(h->get_twin()->get_previous()->get_previous()->get_previous(), h->get_twin());

    ASSERT_EQ(h->get_face(), h->get_next()->get_face());
    ASSERT_EQ(h->get_face(), h->get_next()->get_next()->get_face());

    ASSERT_EQ(h->get_face(), h->get_previous()->get_face());
    ASSERT_EQ(h->get_face(), h->get_previous()->get_previous()->get_face());

    ASSERT_EQ(h->get_twin()->get_face(), h->get_twin()->get_next()->get_face());
    ASSERT_EQ(h->get_twin()->get_face(), h->get_twin()->get_next()->get_next()->get_face());

    ASSERT_EQ(h->get_twin()->get_face(), h->get_twin()->get_previous()->get_face());
    ASSERT_EQ(h->get_twin()->get_face(), h->get_twin()->get_previous()->get_previous()->get_face());

    ASSERT_EQ(h->get_twin(), h_twin);
    ASSERT_EQ(h_twin->get_twin(), h);

    ASSERT_EQ(h->get_next(), h_original_previous);
    ASSERT_EQ(h->get_previous(), h_twin_original_next);

    ASSERT_EQ(h_twin->get_next(), h_twin_original_previous);
    ASSERT_EQ(h_twin->get_previous(), h_original_next);

    ASSERT_EQ(h_original_previous->get_next(), h_twin_original_next);
    ASSERT_EQ(h_original_previous->get_previous(), h);

    ASSERT_EQ(h_original_next->get_next(), h_twin);
    ASSERT_EQ(h_original_next->get_previous(), h_twin_original_previous);

    ASSERT_EQ(h_twin_original_previous->get_next(), h_original_next);
    ASSERT_EQ(h_twin_original_previous->get_previous(), h_twin);

    ASSERT_EQ(h_twin_original_next->get_next(), h);
    ASSERT_EQ(h_twin_original_next->get_previous(), h_original_previous);
}