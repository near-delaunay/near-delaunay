#include <gtest/gtest.h>
#include <dcel/Halfedge.h>
#include <dcel/DCELVertex.h>

#include <cmath>

TEST (Halfedge, get_angle_to_next) {
    std::shared_ptr<DCELVertex> v1 = std::make_shared<DCELVertex>(0, 0);
    std::shared_ptr<DCELVertex> v2 = std::make_shared<DCELVertex>(0, 1);
    std::shared_ptr<DCELVertex> v3 = std::make_shared<DCELVertex>(0, 2);

    std::shared_ptr<Halfedge> h = std::make_shared<Halfedge>();
    std::shared_ptr<Halfedge> h_twin = std::make_shared<Halfedge>();
    std::shared_ptr<Halfedge> h_previous = std::make_shared<Halfedge>();
    std::shared_ptr<Halfedge> h_next = std::make_shared<Halfedge>();

    h->set_twin(h_twin);
    h->set_previous(h_previous);
    h->set_next(h_next);

    h->set_target(v2);
    h_twin->set_target(v1);
    h_previous->set_target(v1);
    h_next->set_target(v3);

    ASSERT_DOUBLE_EQ(h->get_angle_to_next(), M_PI);

    v1 = std::make_shared<DCELVertex>(0, 0);
    v2 = std::make_shared<DCELVertex>(0, 1);
    v3 = std::make_shared<DCELVertex>(1, 1);

    h->set_target(v2);
    h_twin->set_target(v1);
    h_previous->set_target(v1);
    h_next->set_target(v3);

    ASSERT_DOUBLE_EQ(h->get_angle_to_next(), M_PI / 2.0);

    v1 = std::make_shared<DCELVertex>(0, 0);
    v2 = std::make_shared<DCELVertex>(0, 1);
    v3 = std::make_shared<DCELVertex>(1, 0);

    h->set_target(v2);
    h_twin->set_target(v1);
    h_previous->set_target(v1);
    h_next->set_target(v3);

    ASSERT_DOUBLE_EQ(h->get_angle_to_next(), M_PI / 4.0);


    v1 = std::make_shared<DCELVertex>(0, 0);
    v2 = std::make_shared<DCELVertex>(0, 1);
    v3 = std::make_shared<DCELVertex>(1, 2);

    h->set_target(v2);
    h_twin->set_target(v1);
    h_previous->set_target(v1);
    h_next->set_target(v3);

    ASSERT_DOUBLE_EQ(h->get_angle_to_next(), 3.0 * M_PI / 4.0);

    v1 = std::make_shared<DCELVertex>(0, 0);
    v2 = std::make_shared<DCELVertex>(0, 1);
    v3 = std::make_shared<DCELVertex>(-1, 2);

    h->set_target(v2);
    h_twin->set_target(v1);
    h_previous->set_target(v1);
    h_next->set_target(v3);

    ASSERT_DOUBLE_EQ(h->get_angle_to_next(), 5.0 * M_PI / 4.0);

    v1 = std::make_shared<DCELVertex>(0, 0);
    v2 = std::make_shared<DCELVertex>(0, 1);
    v3 = std::make_shared<DCELVertex>(-1, 1);

    h->set_target(v2);
    h_twin->set_target(v1);
    h_previous->set_target(v1);
    h_next->set_target(v3);

    ASSERT_DOUBLE_EQ(h->get_angle_to_next(), 3.0 * M_PI / 2.0);

    v1 = std::make_shared<DCELVertex>(0, 0);
    v2 = std::make_shared<DCELVertex>(0, 1);
    v3 = std::make_shared<DCELVertex>(-1, 0);

    h->set_target(v2);
    h_twin->set_target(v1);
    h_previous->set_target(v1);
    h_next->set_target(v3);

    ASSERT_DOUBLE_EQ(h->get_angle_to_next(), 7.0 * M_PI / 4.0);
}
