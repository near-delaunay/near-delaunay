#include <gtest/gtest.h>
#include <range_tree/RangeNode.h>
#include <range_tree/RangeBuilder.h>
#include <graph/GraphVertex.h>
#include <stack>
#include <utility>
#include "../../../src/point_generators/PointGenerator.h"
#include "../../../src/point_generators/UniformPointGenerator.h"

struct ManualTestRangeTree {
    std::vector<std::pair<double, double>> vertices;
    std::vector<std::pair<double, double>> ranges;
    std::vector<double (*)(const std::shared_ptr<GraphVertex>)> functions;

    ManualTestRangeTree(std::vector<std::pair<double, double>> vertices, std::vector<std::pair<double, double>> ranges,
                        std::vector<double (*)(const std::shared_ptr<GraphVertex>)> functions) :
                        vertices(std::move(vertices)), ranges(std::move(ranges)),
                        functions(std::move(functions)) {};
};

struct GeneratedTestRangeTree {
    std::shared_ptr<PointGenerator> generator;
    std::vector<std::string> seeds;
    std::vector<std::pair<double, double>> ranges;
    std::vector<double (*)(const std::shared_ptr<GraphVertex>)> functions;

    GeneratedTestRangeTree(std::shared_ptr<PointGenerator> generator, std::vector<std::string> seeds, std::vector<std::pair<double, double>> ranges,
                        std::vector<double (*)(const std::shared_ptr<GraphVertex>)> functions) :
            generator(std::move(generator)), seeds(std::move(seeds)), ranges(std::move(ranges)), functions(std::move(functions)) {};
};

std::vector<std::vector<std::pair<double, double>>> range_tree_test_values = {
        {{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}},
        {{0, 0}, {4, 4}, {2, 2}, {3, 3}, {1, 1}},
        {{0, 0}, {4, 4}, {2, 2}, {3, 3}},
        {{0, 3}, {4, 4}, {2, 25}, {3, 9}, {1, 0}},
        {{0, 4}, {4, 6}, {2, 4}, {3, 2}}
};

std::vector<std::pair<std::shared_ptr<PointGenerator>, std::vector<std::string>>> generators = {
        {std::make_shared<UniformPointGenerator>(UniformPointGenerator()),
                {"100#0#100#0#100#1", "100#0#100#0#100#2", "100#0#100#0#100#3", "100#-100#100#-100#100#1", "100#-100#100#-100#100#2", "100#-100#100#-100#100#3"}}
};

std::vector<ManualTestRangeTree> test_range_trees = {
//        ManualTestRangeTree(
//                 {{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}},
//                 {{2, 3}},
//                 {[] (const std::shared_ptr<GraphVertex> v) -> double
//                        {
//                            return v->get_x();
//                        }
//                 }
//        ),
        ManualTestRangeTree(
                {{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 100}, {5, 5}, {6, 6},
                    {7, 7}, {8, 8}, {9, 9}, {10, 10}, {11, 11}, {12, 12}, {13, 13}, {14, 14}},
                {{1, 7}, {0, 0}},
                {[] (const std::shared_ptr<GraphVertex> v) -> double
                 {
                     return v->get_x();
                 },
                 [] (const std::shared_ptr<GraphVertex> v) -> double
                 {
                     return v->get_y();
                 }
                }
        )
};

std::vector<GeneratedTestRangeTree> generated_test_range_trees = {
        GeneratedTestRangeTree(
                std::make_shared<UniformPointGenerator>(UniformPointGenerator()),
                {"90#0#100#0#100#1", "100#0#100#0#100#2", "100#0#100#0#100#3", "100#-100#100#-100#100#1",
                        "100#-100#100#-100#100#2", "100#-100#100#-100#100#3"},
                {{14, 73}, {0, 12}},
                {[] (const std::shared_ptr<GraphVertex> v) -> double
                 {
                     return v->get_x();
                 },
                 [] (const std::shared_ptr<GraphVertex> v) -> double
                 {
                     return v->get_y();
                 }
                }
        )
};

/**
 * Check if the tree is indeed balanced as specified with the left leaves having at most +1 height to the right leaves
 *      (only in the dimension of the root node).
 *
 * @param tree          The tree which should be checked.
 * @param no_vertices   The number of leaves the tree has.
 * @return              An error code according to the following rules:
 *                          0: everything is fine.
 *                          1: no_vertices is not equivalent to the number of leaves stored in the tree.
 *                          2: one of the left leaves doesn't have correct height.
 *                          3: one of the right leaves doesn't have correct height.
 *                          4: traversal of leaves didn't go as expected. This must be due to incorrect structure of the tree.
 */
int check_is_valid_depth(const std::shared_ptr<RangeNode<GraphVertex>>& tree, int no_vertices) {
    if (tree->get_sub_nodes().size() != no_vertices) {
        return 1;
    }

    int power = (int) std::log2(no_vertices);
    int remainder = (int)(no_vertices - std::pow(2, power));

    std::stack<std::pair<std::shared_ptr<RangeNode<GraphVertex>>, int>> stack;

    std::shared_ptr<RangeNode<GraphVertex>> current_traversal = tree;

    int counter = 0;
    int current_depth = 0;
    while (current_traversal || !stack.empty()) {
        while (current_traversal) {
            stack.push({current_traversal, current_depth});
            current_depth++;
            current_traversal = current_traversal->get_left_subtree();
        }

        current_traversal = stack.top().first;
        current_depth = stack.top().second;
        stack.pop();

        if (current_traversal->get_associated_vertex()) {
            if (counter < remainder * 2 && current_depth != power + 1) {
                return 2;
            } else if (counter >= remainder * 2 && current_depth != power) {
                return 3;
            }
            counter++;
        }


        current_traversal = current_traversal->get_right_subtree();
        current_depth++;
    }

    if (counter != no_vertices) {
        return 4;
    }

    return 0;
}

/**
 * Check if the leaves of the tree are in correct order (only in the dimension of the root node).
 *
 * @param tree          The tree which should be checked.
 * @param vertices      The leaves stored in the tree.
 * @param function      The function used to evaluate the leaves.
 * @return              An error code according to the following rules:
 *                          0: everything is fine.
 *                          1: no_vertices is not equivalent to the number of leaves stored in the tree.
 *                          2: the order of the leaves is incorrect.
 *                          3: traversal of leaves didn't go as expected. This must be due to incorrect structure of the tree.
 */
int check_is_valid_order(const std::shared_ptr<RangeNode<GraphVertex>>& tree, std::vector<std::shared_ptr<GraphVertex>> vertices,
                         double (*value)(std::shared_ptr<GraphVertex>)) {
    if (tree->get_sub_nodes().size() != vertices.size()) {
        return 1;
    }

    std::sort(vertices.begin(), vertices.end(), [value]( const auto& lhs, const auto& rhs )
    {
        return (*value)(lhs) < (*value)(rhs);
    });

    std::stack<std::shared_ptr<RangeNode<GraphVertex>>> stack;

    std::shared_ptr<RangeNode<GraphVertex>> current_traversal = tree;

    int counter = 0;
    while (current_traversal || !stack.empty()) {
        while (current_traversal) {
            stack.push(current_traversal);
            current_traversal = current_traversal->get_left_subtree();
        }

        current_traversal = stack.top();
        stack.pop();

        if (current_traversal->get_associated_vertex()) {
            if ((*value)(vertices.at(counter)) != (*value)(current_traversal->get_associated_vertex())) {
                return 2;
            }
            counter++;
        }

        current_traversal = current_traversal->get_right_subtree();
    }

    if (counter != vertices.size()) {
        // error in traversal
        return 3;
    }

    return 0;
}

/**
 * Check if the nodes of the tree have the right value for get_max_left_sub_tree and get_min_right_sub_tree
 * (only in the dimension of the root node).
 *
 * @param tree          The tree which should be checked.
 * @param function      The function used to evaluate the leaves.
 * @return              An error code according to the following rules:
 *                          0: everything is fine.
 *                          1: a leaf node doesn't have the right get_max_left_sub_tree value.
 *                          2: a leaf node doesn't have the right get_min_right_sub_tree value.
 *                          3: an internal node doesn't have the right get_max_left_sub_tree value.
 *                          4: an internal node doesn't have the right get_min_right_sub_tree value.
 */
int check_is_valid_score(const std::shared_ptr<RangeNode<GraphVertex>>& tree,
                         double (*value)(std::shared_ptr<GraphVertex>)) {
    std::stack<std::shared_ptr<RangeNode<GraphVertex>>> stack;

    std::shared_ptr<RangeNode<GraphVertex>> current_traversal = tree;

    int counter = 0;
    while (current_traversal || !stack.empty()) {
        while (current_traversal) {
            stack.push(current_traversal);
            current_traversal = current_traversal->get_left_subtree();
        }

        current_traversal = stack.top();
        stack.pop();

        if (current_traversal->get_associated_vertex()) {
            counter++;

            if (current_traversal->get_max_left_sub_tree() != (*value)(current_traversal->get_associated_vertex())) {
                return 1;
            }

            if (current_traversal->get_min_right_sub_tree() != (*value)(current_traversal->get_associated_vertex())) {
                return 2;
            }
        } else {
            std::shared_ptr<RangeNode<GraphVertex>> max_left_child = current_traversal->get_left_subtree();
            while (max_left_child->get_right_subtree()) {
                max_left_child = max_left_child->get_right_subtree();
            }

            if (current_traversal->get_max_left_sub_tree() != (*value)(max_left_child->get_associated_vertex())) {
                return 3;
            }

            std::shared_ptr<RangeNode<GraphVertex>> min_right_child = current_traversal->get_right_subtree();
            while (min_right_child->get_left_subtree()) {
                min_right_child = min_right_child->get_left_subtree();
            }

            if (current_traversal->get_min_right_sub_tree() != (*value)(min_right_child->get_associated_vertex())) {
                return 4;
            }
        }

        current_traversal = current_traversal->get_right_subtree();
    }

    return 0;
}

/**
 * Loop over the tree and assert correctness of all nodes in all dimensions.
 *
 * @param tree          The tree to be tested.
 * @param functions     The functions used to evaluate the GraphVertices.
 */
void check_tree_recursively(const std::shared_ptr<RangeNode<GraphVertex>>& tree,
                            const std::vector<double (*)(const std::shared_ptr<GraphVertex>)>& functions) {
    std::vector<std::shared_ptr<GraphVertex>> vertices = tree->get_sub_nodes();

    ASSERT_EQ(check_is_valid_depth(tree, vertices.size()), 0);
    ASSERT_EQ(check_is_valid_order(tree, vertices, functions.at(0)), 0);
    ASSERT_EQ(check_is_valid_score(tree, functions.at(0)), 0);

    if (functions.size() > 1) {
        std::vector<double (*)(const std::shared_ptr<GraphVertex>)> remaining_functions =
                std::vector<double (*)(const std::shared_ptr<GraphVertex>)>(functions.cbegin() + 1, functions.cend());

        std::stack<std::shared_ptr<RangeNode<GraphVertex>>> stack;
        std::shared_ptr<RangeNode<GraphVertex>> current_traversal = tree;

        while (current_traversal || !stack.empty()) {
            while (current_traversal) {
                stack.push(current_traversal);
                current_traversal = current_traversal->get_left_subtree();
            }

            current_traversal = stack.top();
            stack.pop();

            if (!current_traversal->get_associated_vertex()) {
                check_tree_recursively(current_traversal->get_associated_structure(), remaining_functions);
            }

            current_traversal = current_traversal->get_right_subtree();
        }
    }
}

/**
 * Loop over all vertices and find the extremum in the given range. Assert that the results of querying the tree are the same.
 *
 * @param tree          The tree to be tested.
 * @param ranges        The range which should be tested.
 * @param functions     The functions used to evaluate the GraphVertices.
 * @param find_min      Boolean whether it should be searched for a minimum or maximum.
 */
void check_range(std::shared_ptr<RangeNode<GraphVertex>> tree, std::vector<std::pair<double, double>> ranges,
                 const std::vector<double (*)(const std::shared_ptr<GraphVertex>)>& functions, bool find_min) {
    std::shared_ptr<GraphVertex> min_vertex;

    double (*final_values) (const std::shared_ptr<GraphVertex>) = functions.at(functions.size() - 1);

    for (auto leaf : tree->get_sub_nodes()) {
        bool out_of_range = false;

        for (int i = 0; i < ranges.size(); i++)  {
            if ((*functions.at(i))(leaf) <= ranges.at(i).first || (*functions.at(i))(leaf) >= ranges.at(i).second) {
                out_of_range = true;
                break;
            }
        }

        if (out_of_range) continue;

        if (!min_vertex ||
                ((((*final_values)(min_vertex) > (*final_values)(leaf)) && find_min) || ((*final_values)(min_vertex) < (*final_values)(leaf)) && !find_min)) {
            min_vertex = leaf;
        }
    }

    std::shared_ptr<RangeNode<GraphVertex>> query_result = tree->find_extremum(ranges, find_min);

    if (query_result == nullptr) {
        ASSERT_TRUE(!min_vertex);
    } else {
        ASSERT_EQ((*final_values)(query_result->get_associated_vertex()), (*final_values)(min_vertex));
    }
}



/**
 * Test for checking whether the range tree is build correctly for the manually specified values.
 */
TEST (range_tree, check_correct_build_set_values) {
    for (const auto& tree_value : range_tree_test_values) {
        std::vector<std::shared_ptr<GraphVertex>> vertices;

        vertices.reserve(tree_value.size());
        for (auto pair : tree_value) {
            vertices.push_back(std::make_shared<GraphVertex>(GraphVertex(pair.first, pair.second)));
        }

        std::vector<double (*)(const std::shared_ptr<GraphVertex>)> functions = {
                [] (const std::shared_ptr<GraphVertex> v) -> double {
                    return v->get_x();
                },
                [] (const std::shared_ptr<GraphVertex> v) -> double {
                    return v->get_y();
                },
                [] (const std::shared_ptr<GraphVertex> v) -> double {
                    return v->get_x();
                },
                [] (const std::shared_ptr<GraphVertex> v) -> double {
                    return v->get_y();
                }
        };

        std::shared_ptr<RangeNode<GraphVertex>> tree = RangeBuilder<GraphVertex>::build_tree(vertices, functions);

        check_tree_recursively(tree, functions);
    }
}

/**
 * Test for checking whether the range tree is build correctly for the autogenerated values.
 */
TEST (range_tree, check_correct_build_generated_values) {
    for (const auto& generator : generators) {
        for (const auto& seed : generator.second) {
            std::vector<std::shared_ptr<GraphVertex>> vertices;

            vertices = generator.first->get_points(seed, '#')->get_vertices();

            std::vector<double (*)(const std::shared_ptr<GraphVertex>)> functions = {
                    [] (const std::shared_ptr<GraphVertex> v) -> double {
                        return v->get_x();
                    },
                    [] (const std::shared_ptr<GraphVertex> v) -> double {
                        return v->get_y();
                    },
                    [] (const std::shared_ptr<GraphVertex> v) -> double {
                        return v->get_x();
                    },
                    [] (const std::shared_ptr<GraphVertex> v) -> double {
                        return v->get_y();
                    }
            };

            std::shared_ptr<RangeNode<GraphVertex>> tree = RangeBuilder<GraphVertex>::build_tree(vertices, functions);

            check_tree_recursively(tree, functions);
        }
    }
}

/**
 * Test for checking whether the range search is correct for the manually specified values.
 */
TEST (range_tree, check_correct_range_search_manual) {
    for (ManualTestRangeTree test_tree : test_range_trees) {
        std::vector<std::shared_ptr<GraphVertex>> vertices;

        vertices.reserve(test_tree.vertices.size());
        for (auto pair : test_tree.vertices) {
            vertices.push_back(std::make_shared<GraphVertex>(GraphVertex(pair.first, pair.second)));
        }

        std::shared_ptr<RangeNode<GraphVertex>> tree = RangeBuilder<GraphVertex>::build_tree(vertices, test_tree.functions);

        check_range(tree, test_tree.ranges, test_tree.functions, true);
        check_range(tree, test_tree.ranges, test_tree.functions, false);
    }
}

/**
 * Test for checking whether the range search is correct for the generated values.
 */
TEST (range_tree, check_correct_range_search_generated) {
    for (GeneratedTestRangeTree test_tree : generated_test_range_trees) {
        for (std::string seed : test_tree.seeds) {
            std::vector<std::shared_ptr<GraphVertex>> vertices = test_tree.generator->get_points(seed, '#')->get_vertices();

            std::shared_ptr<RangeNode<GraphVertex>> tree = RangeBuilder<GraphVertex>::build_tree(vertices, test_tree.functions);

            check_range(tree, test_tree.ranges, test_tree.functions, true);
            check_range(tree, test_tree.ranges, test_tree.functions, false);
        }
    }
}