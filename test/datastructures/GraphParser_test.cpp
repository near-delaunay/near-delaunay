#include <gtest/gtest.h>
#include <dcel/DCEL.h>
#include <graph/Graph.h>
#include <graph/GraphVertex.h>
#include <GraphParser.h>

#include <utility>
#include "../TestHelpers.h"

struct TestGraph {
    std::vector<std::pair<double, double>> vertices;
    std::vector<std::pair<int, int>> edges;
    int faces;

    TestGraph(std::vector<std::pair<double, double>> vertices, std::vector<std::pair<int, int>> edges, int faces) :
        vertices(std::move(vertices)), edges(std::move(edges)), faces(faces) {};

    std::shared_ptr<Graph> create_graph() {
        return TestHelpers::create_graph(vertices, edges);
    }
};

/**
 * Graphs for which the tests should be run. Have to be valid triangulations for check_DCEL_structure to pass.
 */
std::vector<TestGraph> test_graphs = {
        /**
                 1
              /  |
            0 -- 2
         */
        TestGraph(
                {{0, 0}, {1, 1}, {1, 0}},
                {{0, 1}, {0, 2}, {1, 2}},
                2),

        /**
               1
            /  |  \
          0 -- 2 -- 3
        */
        TestGraph(
                {{0, 0}, {1, 1}, {1, 0}, {2, 0}},
                {{0, 1}, {0, 2}, {1, 2}, {1, 3}, {2, 3}},
                3),

        /**
            1
         /  |  \
        0   |  3
         \  | /
            2
        */
        TestGraph(
                {{0, 1}, {1, 2}, {1, 0}, {2, 1}},
                {{0, 1}, {0, 2}, {1, 2}, {1, 3}, {2, 3}},
                3),

        /**
               1 ---- 4
            /  |  \  /
          0 -- 2 -- 3
        */
        TestGraph(
                {{0, 0}, {1, 1}, {1, 0}, {2, 0}, {3, 1}},
                {{0, 1}, {0, 2}, {1, 2}, {1, 3}, {2, 3}, {1, 4}, {3, 4}},
                4),
        /**
               2
            /  |  \
          0 -- 1 -- 3
        */
        TestGraph(
                {{0, 0}, {1, 0}, {1, 1}, {2, 0}},
                {{0, 1}, {0, 2}, {1, 2}, {1, 3}, {2, 3}},
                3),

        /**
         6 -- 7
         | \  |
         4 -- 5
         |\ / | \
         | 2  |  3
         |/ \ | /
         0 -- 1
        */
        TestGraph(
                {{0, 0}, {2, 0}, {1, 1}, {3, 1}, {0, 2}, {2, 2}, {0, 3}, {2, 3}},
                {{0, 1}, {0, 2}, {0, 4}, {1, 2}, {1, 3}, {1, 5}, {2, 4}, {2, 5}, {3, 5}, {4, 5},{4, 6}, {5, 6}, {5, 7}, {6, 7}},
                8)
};

/**
 * Simple function for asserting the correctness of some of the DCEL properties.
 *
 * @param dcel The DCEL to be checked.
 */
void check_DCEL_structure(std::shared_ptr<DCEL> dcel) {
    // check if properties are set.
    for (std::shared_ptr<Halfedge> edge : dcel->get_edges()) {
        ASSERT_TRUE(edge->get_face() != nullptr);
        ASSERT_TRUE(edge->get_next() != nullptr);
        ASSERT_TRUE(edge->get_twin() != nullptr);
    }
    // count the number of faces with more than 3 associated edges. This should at most be 1; the outer face.
    for (const std::shared_ptr<Face>& face : dcel->get_faces()) {
        std::shared_ptr<Halfedge> loop_edge = face->get_edge_pointer();
        for (int i = 0; i < face->get_adjacent_edges().size(); i++) {
            loop_edge = loop_edge->get_next();
        }

        // check if an inner edge (which has exactly three edges associated to its face) loops back around to itself.
        ASSERT_EQ(face->get_edge_pointer(), loop_edge);
    }
}

/**
 * Basic checks if DCEL is valid triangulation.
 *
 * @param dcel  DCEL to be checked.
 */
void check_DCEL_triangulation(std::shared_ptr<DCEL> dcel) {
    bool found = false;
    for (const std::shared_ptr<Face>& face : dcel->get_faces()) {
        if (face->get_adjacent_edges().size() > 3) {
            ASSERT_FALSE(found);
            found = true;
        }
    }
}

/**
 * Test if the parse_graph_to_DCEL has the right amount of vertices, edges and faces for the specified test graphs and if
 * basic properties about the structure hold.
 */
TEST (GraphParser, parse_graph_to_dcel_check_DCEL_correctness) {
    for (TestGraph test_graph : test_graphs) {
        std::shared_ptr<Graph> graph = test_graph.create_graph();
        std::shared_ptr<DCEL> dcel = GraphParser::parse_graph_to_DCEL(graph);

        // basic correctness check
        ASSERT_EQ(dcel->get_edges().size(), test_graph.edges.size() * 2);
        ASSERT_EQ(dcel->get_vertices().size(), test_graph.vertices.size());
        ASSERT_EQ(dcel->get_faces().size(), test_graph.faces);

        check_DCEL_structure(dcel);
        check_DCEL_triangulation(dcel);

        std::shared_ptr<Face> outer = dcel->get_outer_face();

        // check if flip still works for current graph.
        for (const std::shared_ptr<Halfedge>& edge : dcel->get_edges()) {
            if (edge->get_face() == outer || edge->get_twin()->get_face() == outer) {
                continue;
            }

            dcel->flip_edge(edge);

            check_DCEL_structure(dcel);
            check_DCEL_triangulation(dcel);
        }

        // add a center point to each inner face and connect it to the outer points.
        for (const std::shared_ptr<Face>& face : dcel->get_faces()) {
            if (face == outer) {
                continue;
            }

            std::shared_ptr<Halfedge> e1 = face->get_edge_pointer();
            std::shared_ptr<Halfedge> e2 = e1->get_next();
            std::shared_ptr<Halfedge> e3 = e2->get_next();

            double x = (e1->get_target()->get_x() + e2->get_target()->get_x() + e3->get_target()->get_x()) / 3;
            double y = (e1->get_target()->get_y() + e2->get_target()->get_y() + e3->get_target()->get_y()) / 3;

            std::shared_ptr<DCELVertex> new_vertex = std::make_shared<DCELVertex>(DCELVertex(x, y));

            dcel->add_vertex_at(new_vertex, e1);
            dcel->add_edge_at(new_vertex, e2);
            dcel->add_edge_at(new_vertex, e3);

            check_DCEL_structure(dcel);
            check_DCEL_triangulation(dcel);
        }

        // check once more if flipping still works.
        for (const std::shared_ptr<Halfedge>& edge : dcel->get_edges()) {
            if (edge->get_face() == outer || edge->get_twin()->get_face() == outer) {
                continue;
            }

            dcel->flip_edge(edge);

            check_DCEL_structure(dcel);
            check_DCEL_triangulation(dcel);
        }

        // add a point to the outer face and check if it is still the outer face.
        std::shared_ptr<Halfedge> test_edge = *(dcel->get_edges().cbegin());
        std::shared_ptr<Face> outer_face = dcel->get_outer_face();

        std::shared_ptr<DCELVertex> new_vertex = std::make_shared<DCELVertex>(DCELVertex(0, 0));
        dcel->add_vertex_at(new_vertex, outer_face->get_edge_pointer());

        ASSERT_EQ(outer_face, dcel->get_outer_face());

        check_DCEL_structure(dcel);
    }
}

/**
 * Create a DCEL from a graph and then create a graph from the DCEL. The following graphs should be isomorphic.
 * Not truly a unit test but I don't really want to create DCELs manually to test the parse_DCEL_to_graph function.
 * This also tests if parse_graph_to_DCEL preserves the structure of the graph.
 */
TEST (GraphParser, parse_graph_to_dcel_and_parse_DCEL_to_graph) {
    for (TestGraph test_graph : test_graphs) {
        std::shared_ptr<Graph> graph = test_graph.create_graph();
        std::shared_ptr<DCEL> dcel = GraphParser::parse_graph_to_DCEL(graph);
        std::shared_ptr<Graph> graph_parsed = GraphParser::parse_DCEL_to_graph(dcel);

        ASSERT_TRUE(graph->is_isomorphic(graph_parsed));
    }
}

/**
 * Create a graph, write it to a file, read the file and check if the two graphs are isomorphic.
 * This too is not really a unit test but frankly these functions are so simple (and dependent on the file format)
 * that this should suffice.
 */
TEST (GraphParser, test_save_and_load_file) {
    for (TestGraph test_graph : test_graphs) {
        std::shared_ptr<Graph> graph = test_graph.create_graph();

        // Google seems to be smart enough to not actually create these files on the actual file system.
        // Thus the test must run in some mock environment.
        GraphParser::write_to_file(graph, "temporaryTestFile.txt");
        std::shared_ptr<Graph> graph_parsed = GraphParser::read_in_file("temporaryTestFile.txt");

        ASSERT_TRUE(graph->is_isomorphic(graph_parsed));
    }
}