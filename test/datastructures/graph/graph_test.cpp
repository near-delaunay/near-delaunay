#include <gtest/gtest.h>
#include <graph/Graph.h>
#include "../../TestHelpers.h"

/**
 * Check the is equivalent functions with passing inputs.
 */
TEST (Graph, isIsomorphic_pass) {
    std::vector<std::pair<double, double>> vertices = {{0, 0}, {2, 0}, {1, 1}, {3, 1}, {0, 2},
                                                 {2, 2}, {0, 3}, {2, 3}};
    std::vector<std::pair<int, int>> edges = {{0, 1}, {0, 2}, {0, 4}, {1, 2}, {1, 3},
                                              {1, 5}, {2, 4}, {2, 5}, {3, 5}, {4, 5},
                                              {4, 6}, {5, 6}, {5, 7}, {6, 7}};

    std::shared_ptr<Graph> g1 = TestHelpers::create_graph(vertices, edges);
    std::shared_ptr<Graph> g2 = TestHelpers::create_graph(vertices, edges);

    ASSERT_TRUE(g1->is_isomorphic(g1));
    ASSERT_TRUE(g1->is_isomorphic(g2));
    ASSERT_TRUE(g2->is_isomorphic(g1));
}

/**
 * Check if isIsomorphic fails if one graph has one edge fewer than the other.
 */
TEST (Graph, isIsomorphic_fail_fewerEdges) {
    std::vector<std::pair<double, double>> vertices = {{0, 0}, {2, 0}, {1, 1}, {3, 1}, {0, 2},
                                                 {2, 2}, {0, 3}, {2, 3}};
    std::vector<std::pair<int, int>> edges = {{0, 1}, {0, 2}, {0, 4}, {1, 2}, {1, 3},
                                              {1, 5}, {2, 4}, {2, 5}, {3, 5}, {4, 5},
                                              {4, 6}, {5, 6}, {5, 7},
                                              {6, 7} // this edge is missing in other graph
                                              };

    std::vector<std::pair<int, int>> edges2 = {{0, 1}, {0, 2}, {0, 4}, {1, 2}, {1, 3},
                                              {1, 5}, {2, 4}, {2, 5}, {3, 5}, {4, 5},
                                              {4, 6}, {5, 6}, {5, 7}};

    std::shared_ptr<Graph> g1 = TestHelpers::create_graph(vertices, edges);
    std::shared_ptr<Graph> g2 = TestHelpers::create_graph(vertices, edges2);

    ASSERT_FALSE(g1->is_isomorphic(g2));
    ASSERT_FALSE(g2->is_isomorphic(g1));
}

/**
 * Check if is_isomorphic fails if one edge differs between graphs.
 */
TEST (Graph, is_isomorphic_fail_differentEdges) {
    std::vector<std::pair<double, double>> vertices = {{0, 0}, {2, 0}, {1, 1}, {3, 1}, {0, 2},
                                                 {2, 2}, {0, 3}, {2, 3}};
    std::vector<std::pair<int, int>> edges = {{0, 1}, {0, 2}, {0, 4}, {1, 2}, {1, 3},
                                              {1, 5}, // this edge is different
                                              {2, 4}, {2, 5}, {3, 5}, {4, 5},
                                              {4, 6}, {5, 6}, {5, 7}, {6, 7}};

    std::vector<std::pair<int, int>> edges2 = {{0, 1}, {0, 2}, {0, 4}, {1, 2}, {1, 3},
                                               {2, 3}, // this edge is different
                                               {2, 4}, {2, 5}, {3, 5}, {4, 5},
                                               {4, 6}, {5, 6}, {5, 7}, {6, 7}};

    std::shared_ptr<Graph> g1 = TestHelpers::create_graph(vertices, edges);
    std::shared_ptr<Graph> g2 = TestHelpers::create_graph(vertices, edges2);

    ASSERT_FALSE(g1->is_isomorphic(g2));
    ASSERT_FALSE(g2->is_isomorphic(g1));
}