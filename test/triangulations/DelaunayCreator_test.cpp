#include <gtest/gtest.h>
#include "../TestHelpers.h"
#include <DelaunayCreator.h>

#include <utility>

struct DelaunayCreatorTestGraph {
    std::vector<std::pair<double, double>> vertices;
    std::vector<std::pair<int, int>> edges;

    DelaunayCreatorTestGraph(std::vector<std::pair<double, double>> vertices, std::vector<std::pair<int, int>> edges) :
            vertices(std::move(vertices)), edges(std::move(edges)) {};

    std::shared_ptr<Graph> create_graph() const {
        return TestHelpers::create_graph(vertices, edges);
    }

    std::shared_ptr<Graph> create_edgeless_graph() const {
        return TestHelpers::create_graph(vertices, {});
    }
};

std::vector<DelaunayCreatorTestGraph> delaunay_creator_test_graphs = {
        /**
                 1
              /  |
            0 -- 2
         */
        DelaunayCreatorTestGraph(
                {{0, 0}, {1, 1}, {1, 0}},
                {{0, 1}, {0, 2}, {1, 2}}),

        /**
               1
            /  |  \
          0 -- 2 -- 3
        */
        DelaunayCreatorTestGraph(
                {{0, 0}, {1, 1}, {1, 0}, {2, 0}},
                {{0, 1}, {0, 2}, {1, 2}, {1, 3}, {2, 3}}),


        /**
               5  -----------6
             / | \         /
           /   |   \     /
         /     |     \ /
        2 ---  3 ---  4
          \   /  \  /
           0------1
        */
        DelaunayCreatorTestGraph(
        {{1, 0}, {3, 0}, {0, 1}, {2, 1}, {4, 1}, {2, 4}, {5, 4}},
        {{0, 1}, {0, 2}, {0, 3}, {1, 3}, {1, 4}, {2, 3}, {2, 5}, {3, 4}, {3, 5}, {4, 5}, {4, 6}, {5, 6}})
};

/**
 * Test if the parseGraphToDCEL has the right amount of vertices, edges and faces for the specified test graphs.
 */
TEST (GraphParser, make_Delaunay_pass) {
    for (DelaunayCreatorTestGraph test_graph : delaunay_creator_test_graphs) {

        std::shared_ptr<Graph> original = test_graph.create_graph();
        std::shared_ptr<Graph> delaunated = test_graph.create_edgeless_graph();

        DelaunayCreator delaunay_creator;

        delaunated = delaunay_creator.make_triangulation(delaunated, "", ';');
        ASSERT_TRUE(original->is_isomorphic(delaunated));

        // test if it also works with graphs that already have edges
        std::shared_ptr<Graph> delaunated_with_edges = test_graph.create_graph();
        delaunated_with_edges = delaunay_creator.make_triangulation(delaunated_with_edges, "", ';');
        ASSERT_TRUE(original->is_isomorphic(delaunated_with_edges));
    }
}